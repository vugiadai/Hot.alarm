import {AsyncStorage} from 'react-native';

export default class UserService {

    static saveUserInfo(data) {
        AsyncStorage.setItem('current_user', JSON.stringify(data));
    }

    static storeOriginalPass(password) {
        AsyncStorage.setItem('password', JSON.stringify(password));
    }

    static saveUsernameAndPass(username, password) {
        let account = {
            'username': username,
            'password': password
        };
        AsyncStorage.setItem('account', JSON.stringify(account));
    }

    static getOriginalPass() {
        AsyncStorage.getItem('password').then((password) => {
            if (password) {
                return JSON.parse(password);
            } else {
                return ""
            }
        });
    }

    static getUserInfo() {
        AsyncStorage.getItem('current_user').then((currentUser) => {
            if (currentUser) {
                return JSON.parse(currentUser);
            } else {
                return null;
            }
        });
    }
}