import React from 'react';
import LoginScreen from "./screens/LoginScreen";
import MainScreen from "./screens/MainScreen";
import {AppLoading, Font} from 'expo';
import {Ionicons} from '@expo/vector-icons';
import ToastUtils from "./constants/ToastUtils";
import UserService from "./database/UserService";
import FAApi from "./constants/Api";
import Form from "./components/Form";
import {AsyncStorage} from "react-native";


export default class App extends React.Component {

    state = {
        isLoadingComplete: false,
        hand: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            isLoading: false
        };

        this._doSignIn = this._doSignIn.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('account').then((result) => {
            try {
                let currentUser = JSON.parse(result);
                if (currentUser !== null) {
                    this.signIn(currentUser.username, currentUser.password);
                }
            } catch (e) {
                this.setState({isLogged: false});
            }
        })
    }

    static Logout() {
        AsyncStorage.clear();
    }

    componentWillUnmount() {
        this._notificationSubscription && this._notificationSubscription.remove();
    }

    render() {
        if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
            return (
                <AppLoading
                    startAsync={this._loadResourcesAsync}
                    onError={this._handleLoadingError}
                    onFinish={this._handleFinishLoading}
                />
            );
        } else {
            if (this.state.isLogged === true)
                return <MainScreen/>;
            else {
                return <LoginScreen signIn={this._doSignIn} isLoading={this.state.isLoading}/>
            }
        }
    }

    _doSignIn() {
        let login = Form.getLoginForm();

        if (this.state.isLoading) return;

        if (login.username === '' || login.password === '') {
            ToastUtils.showInfo('Hãy nhập số tài khoản và mật khẩu');
        } else {
            this.signIn(login.username, login.password);
        }
    }

    signIn(username, password) {
        this.setState({isLoading: true});
        FAApi.login(username, password)
            .then((responseJson) => {
                this.setState({isLoading: false});
                if (responseJson.response_code === 200) {
                    UserService.saveUserInfo(responseJson.data);
                    UserService.storeOriginalPass(password);
                    this.setState({
                        isLogged: true
                    });
                    UserService.saveUsernameAndPass(username, password);
                } else {
                    ToastUtils.showInfo('Tài khoản hoặc mật khẩu không chính xác');
                }
            })
            .catch((error) => {
                this.setState({isLoading: false});
                ToastUtils.showInfo('Quá trình xử lý bị lỗi!!!');
            });
    }

    _loadResourcesAsync = async () => {
        return Promise.all([
            Font.loadAsync({
                ...Ionicons.font,
                'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')
            }),
        ]);
    };

    _handleLoadingError = error => {
        console.warn(error);
    };

    _handleFinishLoading = () => {
        this.setState({isLoadingComplete: true});
    };
}