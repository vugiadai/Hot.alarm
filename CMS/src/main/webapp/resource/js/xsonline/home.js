/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("#btnSearchResult").on('click', function () {
        $("#curPage").val(1);
        $("#pageSize").val(15);
        Search();
    });
});
function Search() {
    $('#loading').show();
//    var data = $("#searchForm").serialize();
    var url = contextPath + "/getFileList";
    var agentName = $("#txtAgentName").val();
    var agentId = $("#agentId").val();
    var fromDate = $("#txtFromDateSearch").val();
    var toDate = $("#txtToDateSearch").val();
    var curPage = $("#curPage").val();
    var pageSize = $("#pageSize").val();
    var data = {};
    if (agentName != undefined && agentName != "") {
        data.agentName = agentName;
    }
    if (agentId != undefined && agentId != "") {
        data.agentId = agentId;
    }
    if (fromDate != undefined && fromDate != "") {
        data.fromDate = fromDate;
    }
    if (toDate != undefined && toDate != "") {
        data.toDate = endDayOfDate(toDate);
    }
    data.curPage = curPage;
    data.pageSize = pageSize;
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            $('#loading').hide();
//            jQuery.parseJSON(e);
            var curPage = result.curPage;
            var pageSize = result.pageSize;
            var totalRecord = result.totalRecord;
            bindingTable(result);
            updatePaginition(curPage, pageSize, totalRecord);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}
function viewDetail(packageId) {
    var url = contextPath + "/contact/getContact" + "?packageId=" + packageId;
    window.location.href = url;
}

function bindingTable(result) {
    var lotteryResult = result.listData;
    var tr;
    var index = 1;
    $("#tableBody").html("");
    for (var i = 0; i < lotteryResult.length; i++) {
        tr = $('<tr/>');
        tr.append("<td style=\"text-align: center\">" + (((result.curPage - 1) * result.pageSize) + index) + "</td>");
        tr.append('<td style="text-align: center"><button class="btn btn-sm" onclick="viewDetail(' + removeNull(lotteryResult[i].id) + ')"><span><i class="fa fa-clock-o "></i></span>Go detail<span class="glyphicon glyphicon-arrow-right" style="margin-left: 5px"></span></button></td>');
        tr.append("<td style=\"text-align: center\">" + removeNull(lotteryResult[i].id) + "</td>");
        tr.append("<td style=\"text-align: center\">" + removeNull(lotteryResult[i].branchName) + "</td>");
        var td = $('<td style="text-align: center;"/>');
        if (lotteryResult[i].totalRow === lotteryResult[i].totalCommitRow) {
            td.append('<span class="glyphicon glyphicon-ok" style="color: green"></span><div style="background-color:  greenyellow;"><span>Done</span></div>');
        } else {
            td.append('<div style="background-color:  orange;"><span>Running</span></div>');
        }
        tr.append(td);
        tr.append("<td style=\"text-align: left\">" + removeNull(lotteryResult[i].totalRow) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(lotteryResult[i].totalCommitRow) + "</td>");
        //tr.append("<td style=\"text-align: left\">" + removeNull(lotteryResult[i].executedDuration) + "</td>");
        tr.append("<td style=\"text-align: center\">" + removeNull(lotteryResult[i].createdDate) + "</td>");
        tr.append("<td style=\"text-align: center\">" + addCommas(removeNull(lotteryResult[i].createdByName)) + "</td>");
        $("#tableBody").append(tr);
        index++;
    }
}
$(document).on("click", ".open-AddBookDialog", function () {
    var curTime = new Date();
    $(".modal-body #datetime").val(formatDateCustom(curTime));//long date time
    $(".modal-body #dateTimeField").val(formatDateCustom2(curTime));//short dateTIme
});

$(document).on("click", ".open-AddContactDialog", function () {
    var curTime = new Date();
    $(".modal-body #datetime").val(formatDateCustom(curTime));//long date time
    $(".modal-body #dateTimeField").val(formatDateCustom2(curTime));//short dateTIme
});