/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var contextPath = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
var previousPage = "";
var baseUrl = '<%=request.getContextPath()%>';
var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};
$(document).ready(function () {
    previousPage = $("#previousPage").val();
    try {
        //date time picker
        $(".date-picker").datepicker();

//    $(".date-picker").on("change", function () {
//        var id = $(this).attr("id");
//        var val = $("label[for='" + id + "']").text();
//    });
    } catch (e) {
    }
    try {
        //quay tro lai trang truoc
        $('#back').on('click', function () {
            $(location).attr('href', contextPath + previousPage);
        });
    } catch (e) {
    }

    try {
        //tu dong tat thong bao sau 2 giay
        $(".alert").fadeTo(2000, 500).slideUp(500, function () {
            $(".alert").slideUp(500);
        });
    } catch (e) {
    }

    try {
        //active nav
        var url = window.location;
        $('ul.nav a[href="' + url + '"]').parent().addClass('active');
        $('ul.nav a').filter(function () {
            return this.href == url;
        }).parent().addClass('active');
    } catch (e) {
    }
    try {
        $('#logout').on('click', function () {
            $("#logoutForm").submit();
        });
    } catch (e) {
    }

    $(".searchElement").keypress(function (e) {
        if (e.which == 13) {
            $("#curPage").val(1);
            $("#pageSize").val(15);
            Search();
        }
    });
});

//function addCommas2(n) {
//    var rx = /(\d+)(\d{3})/;
//    return String(n).replace(/^\d+/, function (w) {
//        while (rx.test(w)) {
//            w = w.replace(rx, '$1.$2');
//        }
//        return w;
//    });
//}
//function addCommas3(num){
//    var n = num.toString(), p = n.indexOf(',');
//    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i){
//        return p<0 || i<p ? ($0+'.') : (','+$1);
//    });
//}
function addCommas(nStr) {
    if (nStr !== null) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    return "";
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}

function setCurPage(curPage) {
    $("#curPage").val(curPage);
    Search();
}
function setPageSize(pageSize) {
    $("#pageSize").val(pageSize);
    Search();
}

function removeNull(object) {
    if (typeof object == 'undefined' || object == null) {
        return "";
    }
    return object;
}

function LoadBootstrapValidatorScript(callback) {
    if (!$.fn.bootstrapValidator) {
//        $.getScript('plugins/bootstrapvalidator/bootstrapValidator.min.js', callback);
        $.getScript('/resource/js/bootstrap/bootstrapValidator.min.js', callback);
    } else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}

function endDayOfDate(date) {
    var end = new Date(date);
    end.setHours(23, 59, 59, 999);
    return end;
}

function formatDateCustom(date) {
//    var hours = date.getHous();
//    var minutes = date.getMinutes();
//    var seconds = date.getSeconds();
//    seconds = seconds < 10 ? '0' + seconds : seconds;
//    hours = hours < 10 ? '0' + hours : hours;
//    minutes = minutes < 10 ? '0' + minutes : minutes;
//    var strTime = hours+":"+minutes

    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}
function formatDateCustom2(date) {
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}