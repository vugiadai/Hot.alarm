/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#btnSearchCallinfo").on('click', function () {
        $("#curPage").val(1);
        $("#pageSize").val(15);
        Search();
    });


    //var playerInstance = jwplayer("myElement");
    //playerInstance.setup({
    //    file: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
    //    width: 240,
    //    height: 40
    //});

});

//$(document).on("click", ".open-AddBookDialog", function () {
//    var sourceData = $(this).data('id');
//    var id = sourceData.split(';')[1];
//    var name = sourceData.split(';')[0];
//    $(".modal-body .object-id").val(id);
//    $(".modal-body .object-name").html(name);
//
//});


function Search() {
    $('#loading').show();
//    var data = $("#searchForm").serialize();
    var url = contextPath + "/agent/getCallInfoList";
    var phoneNumber = $("#phoneNumber").val();
    var branchId = $("#branchId").val();
    var packageId = $("#packageId").val();
    var fromDate = $("#txtFromDateSearch").val();
    var toDate = $("#txtToDateSearch").val();
    var curPage = $("#curPage").val();
    var pageSize = $("#pageSize").val();
    var data = {};
    if (packageId != undefined && packageId != "") {
        data.packageId = packageId;
    }
    if (phoneNumber != undefined && phoneNumber != "") {
        data.phoneNumber = phoneNumber;
    }
    if (branchId != undefined && branchId != "") {
        data.branchId = branchId;
    }
    if (fromDate != undefined && fromDate != "") {
        data.fromDate = fromDate;
    }
    if (toDate != undefined && toDate != "") {
        data.toDate = endDayOfDate(toDate);
    }
    data.curPage = curPage;
    data.pageSize = pageSize;
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            $('#loading').hide();
//            jQuery.parseJSON(e);
            var curPage = result.curPage;
            var pageSize = result.pageSize;
            var totalRecord = result.totalRecord;
            bindingTable(result);
            updatePaginition(curPage, pageSize, totalRecord);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

//function makeCallOut(id) {
//    var url = contextPath + "/callOur/makeCallOut";
//    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
//    var csrfToken = $("meta[name='_csrf']").attr("content");
//    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
//    var data = JSON.stringify({curPage: id});
//    data[csrfParameter] = csrfToken;
//    var headers = {};
//    headers[csrfHeader] = csrfToken;
//    $.ajax({
//        url: url,
//        headers: headers,
//        data: data,
//        contentType: 'application/json; charset=utf-8',
//        dataType: 'json',
//        type: 'POST',
//        success: function (result) {
//            $('#loading').hide();
//            alert(result);
//        },
//        error: function (jqXHR, textStatus, errorThrown) {
//            $('#loading').hide();
//        }
//    });
//}

function bindingTable(result) {
    var json = result.listData;
    var tr;
    var video;
    var index = 1;
    $("#curPage").val(result.curPage);
    $("#pageSize").val(result.pageSize);
    $("#totalRecord").val(result.totalRecord);
    $("#tableBody").html("");
    for (var i = 0; i < json.length; i++) {
        tr = $('<tr/>');
        tr.append("<td  style=\"text-align: center\">" + (((result.curPage - 1) * result.pageSize) + index) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].agentName) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].callId) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].caller) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].called) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].agentId) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].startTime) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].firstConnectTime) + "</td>");
        tr.append("<td  style=\"text-align: left\">" + removeNull(json[i].acdTime) + "</td>");
        if (json[i].url != null && json[i].url.trim() != "") {
            tr.append("<td  style=\"text-align: left\">" + "<video width='100' height='40' controls name='media' style='height: 35px'>"
                    + "<source src='" + removeNull(json[i].url) + "' type='audio/wav'>"
                    + "</video>" + "</td>");
        } else {
            tr.append("<td></td>");
        }
        $("#tableBody").append(tr);
        index++;
    }
}

function upload() {
    if (validateFile()) {
        $('#branchIdUpload').val($('#currBranchId').val());
        var form = $('#uploadFileForm');
        form.submit();
        $('#loading').show();
        return false;
    }
}

function validateFile() {

    var valid = true;
    if ($("#file").val() == '') {
        alert("Select data file please!");
        // your validation error action
        valid = false;
    } else {
        $('#btnDlgUpload').prop("disabled", true);
    }
    return valid //true or false
}

function afterUpload(errorCode) {
    $('#btnDlgUpload').prop("disabled", false);
    $('#loading').hide();
    if ('ok' === errorCode) {
        $("#myModal").modal('toggle');
        ;
        Search();
    }
}

function makeCallOut(id) {
    var url = contextPath + "/callOur/makeCallOut";
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var data = JSON.stringify({curPage: id});
    data[csrfParameter] = csrfToken;
    var headers = {};
    headers[csrfHeader] = csrfToken;
    $.ajax({
        url: url,
        headers: headers,
        data: data,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            $('#loading').hide();
            alert(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
        }
    });
}

function addContact() {
    var msisdn = $('#phoneNumber').val();
    if (msisdn == null || msisdn.trim() == '') {
        alert('Không được để trống số điện thoại.');
        return;
    }


    var url = contextPath + "/agent/addContact";
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var data = JSON.stringify({
        agentId: $('#currBranchId').val(),
        msisdn: msisdn,
        contactName: $('#contactName').val(),
        description: $('#description').val()
    });
    data[csrfParameter] = csrfToken;
    var headers = {};
    headers[csrfHeader] = csrfToken;
    $.ajax({
        url: url,
        headers: headers,
        data: data,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            if (result.resultCode == '00') {
                $('#loading').hide();
                alert('Thêm mới thành công!');
//                $('#hideModalAddContact').click();
                $('#myModalAddContact').modal('hide');
                Search();
            } else {
                alert('Thêm mới thất bại. Thử lại sau!');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            alert('Đã có lỗi xảy ra. Thử lại sau');
        }
    });
}

function changeContactStatus(id, status) {
    var url = contextPath + "/agent/changeContactStatus";
    var data = {
        id: id,
        status: status};
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            if (result.resultCode == '00') {
                $('#loading').hide();
                alert('Thực hiện thành công!');
                $('#myModalActive').modal('hide');
                $('#myModalDeactive').modal('hide');
                Search();
            } else {
                alert('Thực hiện thất bại. Thử lại sau!');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            alert('Đã có lỗi xảy ra. Thử lại sau');
        }
    });

}

function makeCall() {
    var url = contextPath + "/agent/makeCall";
    var data = {
        branchId: $('#currBranchId').val()};
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            if (result.resultCode == '00') {
                $('#loading').hide();
                alert('Thực hiện thành công!');
                $('#myModalFireWarning').modal('hide');
                Search();
            } else if (result.resultCode == '01')
            {
                alert('Các yêu cầu cần cách nhau nhiều hơn 2 tiếng.');
                $('#myModalFireWarning').modal('hide');
            } else {
                alert('Thực hiện thất bại. Thử lại sau!');
                $('#myModalFireWarning').modal('hide');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            alert('Đã có lỗi xảy ra. Thử lại sau');
            $('#myModalFireWarning').modal('hide');
        }
    });
}

