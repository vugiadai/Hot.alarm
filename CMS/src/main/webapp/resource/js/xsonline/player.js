/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $("#btnSearchPlayer").on('click', function () {
        $("#curPage").val(1);
        $("#pageSize").val(15);
        Search();
    });
    $('#btnExcelExportPlayer').on('click', function () {
        var url = contextPath + '/player/playerExportExcel?';
        var phone = $("#phoneSearch").val();
        var packageId = $("#packageId").val();
        var res = $("#res").val();
        var fromDate = $("#txtFromDateSearch").val();
        var toDate = $("#txtToDateSearch").val();
        if (phone != undefined && phone != "") {
            url += '&phone=' + phone;
        }
        if (packageId != undefined && packageId != "") {
            url += '&packageId=' + packageId;
        }
        if (res != undefined && res != "") {
            url += '&res=' + phone;
        }
        if (fromDate != undefined && fromDate != "") {
            url += '&fromDate=' + fromDate;
        }
        if (toDate != undefined && toDate != "") {
            var toEndDate = endDayOfDate(toDate);
            var res = formatDateCustom(toEndDate);
            url += '&toDate=' + res;
        }
        $(location).attr('href', url);
    });
});

function Search() {

//    var data = $("#searchForm").serialize();
    var url = contextPath + "/contact/getContactList";
    var packageId = $("#packageId").val();
    var data = {};
    if (packageId != undefined && packageId != "") {
        data.packageId = packageId;
    } 
//    else {
//        alert("Select data file please!");
//        return;
//    }
    $('#loading').show();
    var phone = $("#phoneSearch").val();
    var res = $("#res").val();
    var fromDate = $("#txtFromDateSearch").val();
    var toDate = $("#txtToDateSearch").val();
    var curPage = $("#curPage").val();
    var pageSize = $("#pageSize").val();

    if (res != undefined && res != "") {
        data.res = res;
    }
    if (phone != undefined && phone != "") {
        data.phone = phone;
    }
    if (fromDate != undefined && fromDate != "") {
        data.fromDate = fromDate;
    }
    if (toDate != undefined && toDate != "") {
        data.toDate = endDayOfDate(toDate);
    }
    data.curPage = curPage;
    data.pageSize = pageSize;
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            $('#loading').hide();
//            jQuery.parseJSON(e);
            var curPage = result.curPage;
            var pageSize = result.pageSize;
            var totalRecord = result.totalRecord;
            bindingTable(result);
            updatePaginition(curPage, pageSize, totalRecord);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function bindingTable(result) {
    var player = result.listData;
    var tr;
    var index = 1;
    $("#tableBody").html("");
    for (var i = 0; i < player.length; i++) {
        tr = $('<tr/>');
        tr.append("<td style=\"text-align: center\">" + (((result.curPage - 1) * result.pageSize) + index) + "</td>");
        tr.append("<td style=\"text-align: center;\">" + removeNull(player[i].datetime) + "</td> ");
        tr.append("<td style=\"text-align: left\">" + removeNull(player[i].phoneNumber) + "</td>");
        var td = $('<td style="text-align: center;"/>');
        if (player[i].res === "180" || player[i].res === "183") {
            td.append('<span class=\"label label-success\">' + removeNull(player[i].res) + '</span>');
        } else {
            td.append('<span class=\"label label-warning\">' + removeNull(player[i].res) + '</span>');
        }
        tr.append(td);
        var td = $('<td style="text-align: center;"/>');
        if (player[i].systemStatus === "0") {
            td.append('<span class=\"label label-warning\">Running</span>');
        } else {
            if (player[i].res === "0") {
                td.append('<span class=\"label label-danger\">Not running yet</span>');
            } else {
                td.append('&nbsp;');
            }
        }
        tr.append(td);

        tr.append("<td style=\"text-align: left\">" + removeNull(player[i].callId) + "</td>");
        tr.append("<td style=\"text-align: center\">" + removeNull(player[i].makeCallTime) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(player[i].retryTotal) + "</td>");

        $("#tableBody").append(tr);
        index++;
    }
}

