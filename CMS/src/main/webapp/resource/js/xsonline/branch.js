/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#btnSearchBranch").on('click', function () {
        $("#curPage").val(1);
        $("#pageSize").val(15);
        Search();
    });
//    $('#btnAddNewBranch').on('click', function () {
//        $(location).attr('href', contextPath + '/agent/getAddNewBranch');
//    });

    $("#btnConfirmDeleteAgent").on("click", function () {
        $(location).attr('href', contextPath + '/agent/doDeleteAgent?id=' + $("#delAgentId").val());
    });

});

$(document).on("click", ".open-AddBookDialog", function () {
    var sourceData = $(this).data('id');
    var id = sourceData.split(';')[1];
    var code = sourceData.split(';')[0];
    $(".modal-body .object-id").val(id);
    $(".modal-body .object-name").html(code);
//    $(".modal-body #customerPhone").val(phone);
//    $(".modal-body #customerPhoneField").val(phone);

});

function BranchFormEditValidator() {
    $('#branchForm').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            code: {
                validators: {
                    notEmpty: {
                        message: 'The agent code is required and can\'t be empty'
                    },
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'The name is required and can\'t be empty'
                    },
                }
            },
        }
    });
}

function Search() {
    $('#loading').show();
//    var data = $("#searchForm").serialize();
    var url = contextPath + "/agent/getBranchList";
    var branchCode = $("#branchCode").val();
    var branchName = $("#branchName").val();
    var fromDate = $("#txtFromDate").val();
    var toDate = $("#txtToDate").val();
    var curPage = $("#curPage").val();
    var pageSize = $("#pageSize").val();
    var data = {};
    if (branchCode != undefined && branchCode != "") {
        data.branchCode = branchCode;
    }
    if (branchName != undefined && branchName != "") {
        data.branchName = branchName;
    }
    if (fromDate != undefined && fromDate != "") {
        data.fromDate = fromDate;
    }
    if (toDate != undefined && toDate != "") {
        data.toDate = endDayOfDate(toDate);
    }
    data.curPage = curPage;
    data.pageSize = pageSize;
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            $('#loading').hide();
//            jQuery.parseJSON(e);
            var curPage = result.curPage;
            var pageSize = result.pageSize;
            var totalRecord = result.totalRecord;
            bindingTable(result);
            updatePaginition(curPage, pageSize, totalRecord);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}


function bindingTable(result) {
    var json = result.listData;
    var tr;
    var index = 1;
    $("#curPage").val(result.curPage);
    $("#pageSize").val(result.pageSize);
    $("#totalRecord").val(result.totalRecord);
    $("#tableBody").html("");
    for (var i = 0; i < json.length; i++) {
        tr = $('<tr/>');
        tr.append("<td  style=\"text-align: center\">" + (((result.curPage - 1) * result.pageSize) + index) + "</td>");
        var detailUrl = escapeHtml(contextPath + "/agent/getBranchDetail/" + json[i].id);
        tr.append("<td  style=\"text-align: left\" onclick=\"window.location = '"+detailUrl+"'\"><a href='#'>" + json[i].code + "</a></td>");
        tr.append("<td  style=\"text-align: left\">" + json[i].name + "</td>");
        tr.append("<td  style=\"text-align: left\">" + json[i].addr + "</td>");
        var editUrl = escapeHtml(contextPath + "/agent/getEditBranch/" + json[i].id);
        var deleteUrl = escapeHtml(json[i].code + ";" + json[i].id);

        var td = $('<td style="text-align: center;"/>');
        td.append("<a style=\"margin-right: 5px\" href=\"" + editUrl + "\">Edit</a>");
        td.append("<a style=\"margin-right: 5px\" href=# onclick='makeCallOut(" + json[i].id + ")'>Call</a>");
        td.append("<a data-toggle=\"modal\" data-id=\"" + deleteUrl + "\" title=\"Confirm delete agent\" class=\"open-AddBookDialog \" href=\"#myModal\">Delete</a>");
        tr.append(td);
        $("#tableBody").append(tr);
        index++;
    }
}

function addBuilding() {
    var buildingName = $('#buildingName').val();
    if (buildingName == null || buildingName.trim() == '') {
        alert('Không được để trống tên tòa nhà.');
        return;
    }

    var url = contextPath + "/agent/addAgent";
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var data = JSON.stringify({
        name: $('#buildingName').val(),
        addr: $('#buildingAddress').val(),
    });
    data[csrfParameter] = csrfToken;
    var headers = {};
    headers[csrfHeader] = csrfToken;
    $.ajax({
        url: url,
        headers: headers,
        data: data,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            if (result.resultCode == '00') {
                $('#loading').hide();
                alert('Thêm mới thành công!');
//                $('#hideModalAddContact').click();
                $('#myModalAddBuilding').modal('hide');
                Search();
            } else {
                alert('Thêm mới thất bại. Thử lại sau!');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            alert('Đã có lỗi xảy ra. Thử lại sau');
        }
    });
}