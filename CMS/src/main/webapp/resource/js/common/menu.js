$(document).ready(function() {
	$('#logout').on('click', function() {
		$(".confirm").confirm({
		    text: "Are you sure want to logout?",		    
		    confirm: function(button) {
		    	$(location).attr('href', baseUrl + "/logout.html?logout");
		    },
		    cancel: function(button) {
		        // nothing to do
		    }		    
		});
	});
});