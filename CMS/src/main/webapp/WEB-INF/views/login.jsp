<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <link rel="shortcut icon" href='<spring:url value="/resource/img/favicon.ico"/>'>

    <style type="text/css">
        <%@include file="../../resource/css/style.css" %>
    </style>
    <c:url var="backgroundImgUrl" value="/resource/img/Jellyfish.jpg"/>
    <c:url var="iconLoginImgUrl" value="/resource/img/icon-login.png"/>
    <style type="text/css">

        .formtransparent{
            /*background: rgba(130,130,130,.3);*/
        }
        .blurBody::before {
            background: url(${backgroundImgUrl}) no-repeat center center fixed;
            content: '';
            z-index: -1;
            width: 100%;
            height: 100%;
            position:absolute; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            -webkit-filter: blur(5px);
            -moz-filter: blur(5px);
            -o-filter: blur(5px);
            -ms-filter: blur(5px);
            filter: blur(5px);
        }


    </style>
    <head>
        <title>Đăng nhập</title>
        <!--<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->

        <link href='<spring:url value="/resource/css/jQuery/jquery-ui.min.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/bootstrap/bootstrap.min.css"/>' rel="stylesheet"></link>



        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery-3.1.1.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/bootstrap.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/xsonline/login.js"/>'></script>

    </head>
    <body onload='document.loginForm.username.focus();'  class="blurBody">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall formtransparent">
                        <h1 class="text-center login-title">Sign in to your account</h1>
                        <img class="profile-img" src="${iconLoginImgUrl}"
                             alt="">
                        <form class="form-signin transparent" name='loginForm' action="<c:url value='login' />"
                              method='POST'>
                            <input name="username" type="text" class="form-control" placeholder="Username" required autofocus
                                   style="margin-bottom: 5px">
                            <input name="password" type="password" class="form-control" placeholder="Password" required>
                            <c:if test="${showCaptcha == 1}">
                                <div class="form-control" style="margin: auto;">
                                    <img id="captcha" src="<c:url value='captcha'/>"   placeholder="Enter captcha" required/>
                                    <input type="text" name="captchaValue" class="form-control" maxlength="10"/>
                                </div>
                            </c:if>
                            <div class="messages">
                                <c:set var="isError" value="true"/>
                                <c:if test="${error != null && error eq isError}">
                                    <div style="color: red; margin-bottom: 10px"><c:out value="Incorrect username or password. Please try again"/></div>
                                </c:if>
                                <c:if test="${captchaError != null && captchaError eq isError}">
                                    <div style="color: red; margin-bottom: 10px"><c:out value="Enter the captcha value"/></div>
                                </c:if>
                            </div>


                            <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">
                                Sign in</button>
                            <input type="hidden"
                                   name="${_csrf.parameterName}"
                                   value="${_csrf.token}"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
