<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/player.js"/>'></script>


<div>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li>Danh sách gọi báo cháy</li>
            </ol>
        </div>
    </div>
    <div class="form-search-player well" >
        <form id="winnerForm">
            <table width="100%">
                <tr>
                    <td width="30%">
                        <input type="text" id="packageId" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Tìm theo sự kiện" value="${packageId}"/>

                    </td>
                    <td>
                        <a href="${breadcrumbGetHomeUrl}"><span><i class="fa fa-clock-o "></i></span>Chọn<span class="glyphicon glyphicon-file" style="margin-left: 5px"></span></a>

                    </td>
                    <td>
                        &nbsp;

                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" id="phoneSearch" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Tìm theo số điện thoại"/>
                    </td>
                    <td>
                        <div class="control-group searchElement" style="max-width: 95%">
                            <div class="controls">
                                <div class="input-group">
                                    <input id="txtFromDateSearch" type="text" class="date-picker form-control" 
                                           placeholder="Từ ngày"/>
                                    <label for="txtFromDateSearch" class="input-group-addon btn" style="display: table-column">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="control-group searchElement" style="max-width: 95%">
                            <div class="controls">
                                <div class="input-group">
                                    <input id="txtToDateSearch" type="text" class="date-picker form-control" 
                                           placeholder="Tới ngày"/>
                                    <label for="txtToDateSearch" class="input-group-addon btn" style="display: table-column">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <!--                <tr>
                                    <td>
                                        <input type="text" id="res" class="form-control searchElement"
                                               style="max-width: 95%" placeholder="Kết quả"/>
                
                                    </td>
                                    <td>
                                        &nbsp;
                
                                    </td>
                                    <td>
                                        &nbsp;
                
                                    </td>
                                </tr>-->
                <tr>
                    <td align="left">
                        <button id="btnSearchPlayer" class=" btn btn-warning  btn-sm" type="button" style="margin-top: 5px">
                            <span><i class="fa fa-clock-o "></i></span> Search<span class="glyphicon glyphicon-search" style="margin-left: 5px"></span>
                        </button>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
    <!--form-list-account    -->
    <div id="form-list-player">
        <div class="box-content no-padding">
            <div style="float:right; margin: auto">
                <!--<button id="btnExcelExportPlayer" class="btn btn-success btn-sm" type="button" style="margin-bottom: 5px">
                    <span><i class="fa fa-clock-o "></i></span>Excel export<span class="glyphicon glyphicon-list-alt" style="margin-left: 5px"></span>
                </button>-->
            </div>
            <table id="playerListTable"
                   class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
                <thead>
                    <tr>
                        <th style="text-align: center; width: 5%">STT</th>
                        <th style="text-align: center; width: 10%">Thời điểm</th>

                        <th style="text-align: center; width: 12%">SĐT</th>							       
                        <th style="text-align: center; width: 10%">Kết quả</th>
                        <!--                         <th style="text-align: center; width: 9%">Package</th>
                        -->                                               
                        <th style="text-align: center; width: 9%">Trạng thái</th>

                        <th style="text-align: center; width: 8%">Mã cuộc gọi</th>
                        <th style="text-align: center; width: 15%">Thời điểm gọi</th>
                        <th style="text-align: center; width: 9%">Thử lại</th>

                    </tr>
                </thead>
                <tbody id="tableBody">
                    <c:set var="rowIndex" value="1"/>
                    <c:forEach var="player" items="${listPlayer}">
                        <tr>				
                            <td style="text-align: center"><c:out value="${((curPage-1)*pageSize)  + rowIndex}"/></td>
                            <td style="text-align: center;"><c:out value="${player.datetime}"/></td>

                            <td style="text-align: left"><c:out value="${player.phoneNumber}"/></td>
                            <td style="text-align: center;">
                                <!--180, 183-->
                                <c:if test="${player.res == '200' || player.res == '200'}">
                                    <span class="label label-success">${player.res}</span>
                                </c:if> 
                                <c:if test="${player.res != '200' && player.res != '200'}">
                                    <span class="label label-warning">${player.res}</span>
                                </c:if>
                                &nbsp;
                            </td>
                            <%--<td style="text-align: left"><c:out value="${player.packageId}"/></td>--%>
                            <td style="text-align: center;">
                                <c:if test="${player.systemStatus == '1' && player.res == '0'}">
                                    <span class="label label-danger">Not running yet</span>
                                </c:if> 
                                <c:if test="${player.systemStatus != '1'}">
                                    <span class="label label-warning">Calling</span>
                                </c:if>
                                <c:if test="${player.systemStatus == '1' && player.res == '200'}">
                                    <span class="label label-success">Done</span>
                                </c:if> 
                                &nbsp;
                            </td>

                            <td style="text-align: left"><c:out value="${player.callId}"/></td>
                            <td style="text-align: center;"><c:out value="${player.makeCallTime}"/></td>
                            <td style="text-align: left"><c:out value="${player.retryTotal}"/></td>

                            <c:set var="rowIndex" value="${rowIndex+1}"/>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <!--paging-->
            <c:set var="pagingPrefix" value="/contact/getContact"/>
            <%@include file="/WEB-INF/views/pages/layout/paginition.jsp" %>
        </div>
    </div>
</div>