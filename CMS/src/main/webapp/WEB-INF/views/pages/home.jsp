<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/home.js"/>'></script>


<div>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li>Sự kiện báo cháy</li>
            </ol>
        </div>
    </div>
    <div class="form-search-lottery well" style="max-height: 170px">
        <form:form id="resultForm" commandName="toSearchForm">
            <table width="100%">
                <tr >
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <!--<td>
                            <input type="text" id="txtAgentName" class="form-control searchElement"
                                   style="max-width: 95%" placeholder="Tìm theo tên tòa nhà"/>
                        </td>-->
                        <td width="30%">
                            <div style="max-width: 95%">
                                <form:select path="agentId" cssClass="textArea form-control" id="agentId">
                                    <option value="" label=" - Chọn tòa nhà - " ></option>
                                    <form:options items="${listBranch}"/>
                                </form:select>
                            </div>
                        </td>
                    </sec:authorize>
                    <td width="30%">
                        <div class="control-group searchElement" style="max-width: 95%">
                            <div class="controls">
                                <div class="input-group">
                                    <input id="txtFromDateSearch" type="text" class="date-picker form-control searchElement" 
                                           placeholder="Từ ngày"/>
                                    <label for="txtFromDateSearch" class="input-group-addon btn" style="display: table-column">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td width="30%">
                        <div class="control-group searchElement" style="max-width: 95%">
                            <div class="controls">
                                <div class="input-group">
                                    <input id="txtToDateSearch" type="text" class="date-picker form-control searchElement" 
                                           placeholder="Tới ngày"/>
                                    <label for="txtToDateSearch" class="input-group-addon btn" style="display: table-column">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>

                    <td align="left">
                        <button id="btnSearchResult" class=" btn btn-warning btn-sm" type="button" style="margin-top: 5px">
                            <span><i class="fa fa-clock-o "></i></span> Search<span class="glyphicon glyphicon-search" style="margin-left: 5px"></span>
                        </button>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </form:form>
    </div>
    <!--form-list-package-->
    <div id="form-list-result">
        <div class="box-content no-padding">
            <table id="resultListTable"
                   class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
                <thead>
                    <tr>
                        <th style="text-align: center; width: 5%">STT</th>
                        <th style="text-align: center; width: 10%">Hành động</th>
                        <th style="text-align: center; width: 10%">Mã sự kiện</th>
                        <th style="text-align: center; width: 10%">Tòa nhà</th>
                        <th style="text-align: center; width: 10%">Trạng thái</th>
                        <th style="text-align: center; width: 10%">Số lượng cần gọi</th>
                        <th style="text-align: center; width: 10%">Đã thực hiện</th>
                        <!--<th style="text-align: center; width: 10%">Tổng thời lượng (s)</th>-->
                        <th style="text-align: center; width: 10%">Thời gian tạo</th>							       
                        <th style="text-align: center; width: 10%">Người tạo</th>
                    </tr>
                </thead>
                <tbody id="tableBody">
                    <c:set var="rowIndex" value="1"/>
                    <c:forEach var="result" items="${listResult}">
                        <tr>				
                            <td style="text-align: center"><c:out value="${((curPage-1)*pageSize)  + rowIndex}"/></td>
                            <td style="text-align: center">
                                <button class="btn btn-sm"  onclick="viewDetail(${result.id})"><span><i class="fa fa-clock-o "></i></span>Go detail<span class="glyphicon glyphicon-arrow-right" style="margin-left: 5px"></span></button>
                            </td>
                            <td style="text-align: center"><c:out value="${result.id}"/></td>
                            <td style="text-align: center"><c:out value="${result.branchName}"/></td>
                            <!--<td style="text-align: center"><c:out value="${result.fileName}"/></td>-->
                            <td style="text-align: center">
                                <c:if test="${result.totalRow == result.totalCommitRow}">
                                    <span class="glyphicon glyphicon-ok" style="color: green"></span>
                                    <div style="background-color:  greenyellow;"><span>Done</span></div>
                                </c:if>
                                <c:if test="${result.totalRow != result.totalCommitRow}">
                                    <div style="background-color:  orange;"><span>Running</span></div>
                                </c:if>
                            </td>
                            <td style="text-align: left"><c:out value="${result.totalRow}"/></td>
                            <td style="text-align: left"><c:out value="${result.totalCommitRow}"/></td>
                            <!--<td style="text-align: left"><c:out value="${result.executedDuration}"/></td>-->
                            <td style="text-align: center"><c:out value="${result.createdDate}"/></td>
                            <td style="text-align: center"><c:out value="${result.createdByName}"/></td>
                            <c:set var="rowIndex" value="${rowIndex+1}"/>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <!--paging-->
            <c:set var="pagingPrefix" value="/getHome"/>
            <%@include file="/WEB-INF/views/pages/layout/paginition.jsp" %>
        </div>
    </div>
</div>

<iframe id="frm" name="frm" style="display: none;"></iframe>