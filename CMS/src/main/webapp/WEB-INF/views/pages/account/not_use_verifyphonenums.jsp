<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/account.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<input type="hidden" id="previousPage" value="/account/getAccount">
<spring:url value="/verifyphonenums" htmlEscape="true"
            var="verifyFormAction"></spring:url>
    <input type="hidden" id="previousPage" value="/account/getAccount">
<form:form class="form-horizontal" role="form"
           action="${verifyFormAction}" id="verifyForm" modelAttribute="account">
    <label>Nhap ma xac nhan cho tai khoan ${account.username} da duoc gui ve so dien thoai: ${account.phone}</label>
    <div class="form-group has-feedback">
        <label class="col-sm-2 control-label">Nhập mã xác nhận</label>
        <div class="col-sm-4">
            <form:input type="text" path="activeCode" id="activeCode"
                        class="form-control" placeholder="Mã xác nhận"/>
            <form:errors path="activeCode" cssClass="error" element="div" />
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-3 pull-right">
            <button class="btn btn-primary btn-label-left" type="submit">
                <span><i class="fa fa-clock-o"></i></span> Xác nhận
            </button>
        </div>
    </div>
</form:form>