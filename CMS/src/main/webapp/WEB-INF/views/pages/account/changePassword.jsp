<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
    <link rel="shortcut icon" href='<spring:url value="/resource/img/favicon.ico"/>'>
    <style type="text/css">
        <%@include file="../../../../resource/css/style.css" %>
    </style>
    <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery-3.1.1.min.js"/>'></script>
    <c:url var="backgroundImgUrl" value="/resource/img/Jellyfish.jpg"/>
    <c:url var="iconLoginImgUrl" value="/resource/img/icon-login.png"/>
    <style type="text/css">

        form.transparent{
            background-color:rgba(0,0,0,0) !important;
        }
        .blurBody::before {
            background: url(${backgroundImgUrl}) no-repeat center center fixed;
            content: '';
            z-index: -1;
            width: 100%;
            height: 100%;
            position:absolute; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            -webkit-filter: blur(5px);
            -moz-filter: blur(5px);
            -o-filter: blur(5px);
            -ms-filter: blur(5px);
            filter: blur(5px);
        }

    </style>
    <head>
        <title>Change password</title>
        <!--<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->

        <link href='<spring:url value="/resource/css/jQuery/jquery-ui.min.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/bootstrap/bootstrap.min.css"/>' rel="stylesheet"></link>

        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery-3.1.1.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/bootstrap.min.js"/>'></script>

        <script>
            $(document).ready(function () {
                var contextPath = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
                $("#back").on("click", function () {
                    $(location).attr('href', contextPath + "/getHome");
                });
                $("#submitForm").on("click", function () {
                    $("#changePassForm").submit();
                });
            });


        </script>



    </head>
    <c:url var="changePassUrl"  value="/doChangePass"/>

    <body onload='document.changePassForm.oldPass.focus();'  class="blurBody">
        <div class="container">
            <c:url value="/j_spring_security_logout" var="logoutUrl" />

            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <h1 class="text-center login-title">Đổi mật khẩu</h1>
                        <img class="profile-img" src="${iconLoginImgUrl}"
                             alt="">
                        <form class="form-signin transparent" name='changePassForm' action="${changePassUrl}" id="changePassForm"
                              method='POST'>
                            <input style ="margin-bottom: 10px" id="oldPass" name="oldPass" type="password" class="form-control" placeholder="Your current password" required autofocus>
                            <input style ="margin-bottom: 10px" id="newPass" name="newPass" type="password" class="form-control" placeholder="New password" required autofocus>
                            <input style ="margin-bottom: 10px" id="reNewPass" name="reNewPass" type="password" class="form-control" placeholder="Confirm Password" required autofocus>
                            <div class="messages">
                                <c:set var="isError" value="true"/>
                                <c:if test="${error != null && error eq isError}">
                                    <div style="color: red; margin-bottom: 10px"><c:out value="Change password failed! Please try again ..."/></div>
                                </c:if>
                            </div>


                            <input type="hidden"
                                   name="${_csrf.parameterName}"
                                   value="${_csrf.token}"/>
                        </form>
                        <!--                        <button class="btn btn-primary">
                                                    Back</button>-->
                        <div align="right" style="margin-right: 8%;margin-top:0px">
                            <button class="btn btn-small btn-primary" id="submitForm" type="submit" name="submit" style="align-self: flex-end">
                                Change</button>
                            <button id="back" class="btn btn-small btn-primary" style="align-self: flex-start">
                                Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            $('#logout').on('click', function () {
                $("#logoutForm").submit();
            });
        }
        );
    </script>    
</html>