<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/account.js"/>'></script>

<input type="hidden" id="currAccountId" value="${accountId}">
<div>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li>Quản lý người dùng</li>
            </ol>
        </div>
    </div>
    <div class="form-search-branch well" style="max-height: 170px">
        <form:form id="accountSearchForm" commandName="toSearchForm">
            <table width="100%">
                <tr>
                    <td>
                        <!--<label>Username</label>-->
                        <input type="text" id="username" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Tìm kiếm theo Username"/>
                    </td>
                    <td align: center>
                        <input type="text" id="phoneNumber" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Nhập số điện thoại đển tìm"/>
                        <!--                        <div class="control-group " style="margin: auto;max-width: 95%">
                                                    <div class="controls">
                                                        <div class="input-group">
                                                            <input id="txtFromDate" type="text" class="date-picker form-control searchElement" placeholder="Created from"/>
                                                            <label for="txtFromDate" class="input-group-addon btn">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>-->
                    </td>
                    <!--<td align:right>
                        <input type="text" id="agentCode" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Nhập mã tòa nhà để tìm"/>
                    </td>
                    <td align:right>
                        <input type="text" id="agentName" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Tìm kiếm theo tên tòa nhà"/>
                    </td>-->

                    <td align:right>
                        <form:select path="agentId" cssClass="textArea form-control" id="agentId">
                            <option value="" label=" - Chọn tòa nhà - " ></option>
                            <form:options items="${listBranch}"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <button id="btnSearchAccount" class=" btn btn-warning btn-sm" type="button" style="margin-top: 5px">
                            Search <span class="glyphicon glyphicon-search" style="margin-left: 5px"></span>
                        </button>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </form:form>
    </div>
    <!--form-list-account    -->
    <div id="form-list-account">
        <div class="box-content no-padding">
            <div style="float:right; margin: auto">
<!--                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <button data-toggle="modal" 
                            title="Update super agent discount1" class="btn-danger btn btn-sm" 
                            href="#sAgentDiscount"
                            style="margin-bottom: 5px; margin-left: 5px"
                            >S.Agent Discount
                    </button>
                </sec:authorize>-->
                <button id="btnAddNewAccount" class="btn btn-success btn-sm" type="button" style="margin-bottom: 5px">
                    <span><i class="fa fa-clock-o "></i></span> Create new account <span class="glyphicon glyphicon-plus" style="margin-left: 5px"></span>
                </button>
            </div>
            <table id="accountListTable"
                   class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
                <thead>
                    <tr>
                        <th style="text-align: center; width: 3%">STT</th>
                        <th style="text-align: center; width: 8%">Tên đăng nhập</th>
                        <th style="text-align: center; width: 8%">Quyền</th>
                        <th style="text-align: center; width: 8%">Email</th>
                        <th style="text-align: center; width: 8%">SĐT</th>
                        <th style="text-align: center; width: 12%">Địa chỉ</th>
                        <th style="text-align: center; width: 12%">Tòa nhà</th>
                        <!--<th style="text-align: center; width: 5%">Trạng thái</th>-->
                        <th style="width: 8%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody id="tableBody">
                    <c:set var="rowIndex" value="1"/>
                    <spring:url value="/account/getEditAccount/" var="editUrl" htmlEscape="true"/>
                    <c:forEach var="account" items="${listAccount}">
                        <tr>				
                            <td style="text-align: center"><c:out value="${((curPage-1)*pageSize)  + rowIndex}"/></td>
                            <td style="text-align: left"><c:out value="${account.username}"/></td>
                            <td style="text-align: left"><c:out value="${account.roleName}"/></td>
                            <td style="text-align: left"><c:out value="${account.email}"/></td>
                            <td style="text-align: left"><c:out value="${account.phone}"/></td>
                            <td style="text-align: left"><c:out value="${account.address}"/></td>
                            <td style="text-align: left"><c:out value="${account.agentName}"/></td>
<!--                            <td style="text-align: center;">
                                <c:if test="${account.enable eq true}">
                                    <span class="label label-success">OK</span>
                                </c:if> 
                                <c:if test="${account.enable eq false}">
                                    <span class="label label-danger btn open-AddBookDialog2 unlock-btn"
                                          data-toggle="modal" 
                                          data-id="${account.username.concat(';').concat(account.id)}"
                                          title="Confirm unlock account"
                                          href="#modalUnlock"
                                          >Locked</span>
                                </c:if>
                            </td>-->
                            <td style="text-align: center;">
                                <a  style="margin-right: 5px" href="${editUrl}<c:out value="${account.id}"/>">Edit</a>
                                <!--<a href="${lockUrl}<c:out value="${account.id}"/>">Lock</a>-->
                                <a data-toggle="modal" 
                                   data-id="${account.username.concat(';').concat(account.id)}"
                                   title="Confirm delete account" class="open-AddBookDialog" 
                                   href="#myModal"
                                   >Delete</a>
                            </td>

                            <c:set var="rowIndex" value="${rowIndex+1}"/>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <!--paging-->
            <c:set var="pagingPrefix" value="/account/getAccount"/>
            <%@include file="/WEB-INF/views/pages/layout/paginition.jsp" %>
        </div>
    </div>
</div>
<!-- Modal delete-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Delete account <label id="delAccountUsername" style="color: red"></label>. Are you sure?</label>
                <input type="hidden" id="delAccountId"/>
                <div style="text-align: center">
                    <button type="button" class="btn btn-default" id="btnConfirmDelete">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal unlock-->
<div id="modalUnlock" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Unlock account <label id="unlockAccountUsername" style="color: red"></label>. Are you sure?</label>
                <input type="hidden" id="unlockAccountId"/>
                <div style="text-align: center">
                    <button type="button" class="btn btn-default" id="btnConfirmUnlock">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>

    </div>
</div>
<!--modal set super agent discount-->
<div id="sAgentDiscount" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update super agent discount</h4>
            </div>
            <div class="modal-body">
                <spring:url value="/doUpdateSADiscount" htmlEscape="true"
                            var="formActionUpdate"></spring:url>
                <form:form class="form-horizontal" role="form"
                           action="${formActionUpdate}" id="spAgentDiscount" modelAttribute="spAgentDiscount">
                    <div class="form-group has-feedback control-group">
                        <label  class="col-sm-4 control-label">Super Agent Discount</label>
                        <div class="col-sm-8">
                            <input type="text" id="discount" path="discount" name="discount"
                                   class="form-control" placeholder="Super Agent Discount"/>
                        </div>
                    </div>
                    <div class="form-group control-group" style="margin-bottom: 0px">
                        <button style="float: right;margin-left: 5px ;margin-right: 15px" type="submit" class="btn btn-default " id="btnConfirmDiscount">Update</button>
                        <button style="float: right" type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>
                </form:form>

            </div>
        </div>
    </div>
</div>
