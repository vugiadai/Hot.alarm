<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/account.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<%--<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/validate.js"/>'></script>--%>
<input type="hidden" id="previousPage" value="/account/getAccount"/>
<div>
    <input type="hidden" id="pageType" value="${typeView}"/>
    <c:if test="${typeView =='edit'}">
        <spring:url value="/account/doEditAccount" htmlEscape="true"
                    var="formAction"></spring:url>
        <c:set var="disableTextOnEdit" value="true"/>
        <c:set var="breadcrumbTitle" value="Edit account"/>
    </c:if>
    <c:if test="${typeView =='new'}">
        <spring:url value="/account/doAddNewAccount" htmlEscape="true"
                    var="formAction"></spring:url>
        <c:set var="disableTextOnEdit" value="false"/>
        <c:set var="breadcrumbTitle" value="Add new account"/>
    </c:if>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <spring:url var="breadcrumbgetAccountUrl" value="/account/getAccount" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li><a href="${breadcrumbgetAccountUrl}">Account Manager</a></li>
                <li><c:out value="${breadcrumbTitle}"/></li>
            </ol>
        </div>
    </div>
    <form:form class="form-horizontal" role="form"
               action="${formAction}" id="accountInfoForm" commandName="account">
        <div class="jumbotron" style="max-width: 50%">
            <div class="form-group has-feedback control-group">
                <label class="col-sm-4 control-label">Username</label>
                <div class="col-sm-8">
                    <form:input type="text" path="username" id="username"
                                class="form-control" placeholder="Tên đăng nhập" disabled="${disableTextOnEdit}"/>
                    <c:if test="${typeView =='edit'}">
                        <form:input type="hidden" path="username"/>
                    </c:if>
                    <form:errors path="username" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">First name</label>
                <div class="col-sm-8">
                    <form:input type="text" path="firstName" id="firstName"
                                class="form-control" placeholder="Tên" />
                    <form:errors path="firstName" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">Last name</label>
                <div class="col-sm-8">
                    <form:input type="text" path="lastName" id="lastName"
                                class="form-control" placeholder="Họ" />
                    <form:errors path="lastName" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                    <form:input type="text" path="email" id="email"
                                class="form-control" placeholder="User email"/>
                    <form:errors path="email" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">Phone</label>
                <div class="col-sm-8">
                    <form:input type="text" path="phone" id="phone"
                                class="form-control" placeholder="User phone" min="0" data-bind="value:replyNumber" />
                    <form:errors path="phone" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">Địa chỉ</label>
                <div class="col-sm-8">
                    <form:input type="text" path="address" id="address"
                                class="form-control" placeholder="User address" />
                    <form:errors path="address" cssClass="error" element="div" />
                </div>
            </div>
            <%-- <c:if test="${typeView =='edit'}">
                 <div class="form-group has-feedback">
                     <label class="col-sm-4 control-label"></label>
                     <div class="col-sm-8">  
                         <form:checkbox path="isAdminChangePass" id="chkChangePass" label="Change Password"/>
                     </div>
                 </div> 
        </c:if>--%>
            <div id="changePassOnEdit">
                <div class="form-group has-feedback">
                    <label class="col-sm-4 control-label">Mật khẩu</label>
                    <div class="col-sm-8">
                        <form:input type="password" path="password" id="password"
                                    class="form-control" placeholder="User password" />
                        <form:errors path="password" cssClass="error" element="div" />
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-sm-4 control-label">Xác nhận mật khẩu</label>
                    <div class="col-sm-8">
                        <form:input type="password" path="rePassword" id="rePassword"
                                    class="form-control" placeholder="Confirm password" />
                        <form:errors path="rePassword" cssClass="error" element="div" />
                    </div>
                </div>
            </div>
            <c:if test="${typeView =='new'}">
                <div class="form-group has-feedback">
                    <label class="col-sm-4 control-label">Quyền</label>
                    <div class="col-sm-8">
                        <form:select path="role" cssClass="textArea form-control" id="roleSelect">
                            <!--<option value="Select" label=" - Select - " ></option>-->
                            <form:options items="${listRole}"/>
                        </form:select>
                        <form:errors path="role" cssClass="error" element="div" />
                    </div>
                </div>
            </c:if>
            <div id="agent_info" style="display:none">
                <div class="form-group has-feedback">
                    <label class="col-sm-4 control-label">Tòa nhà</label>
                    <div class="col-sm-8">
                        <form:select path="agentId" cssClass="textArea form-control" id="agentId">
                            <!--<option value="Select" label=" - Select - " ></option>-->
                            <form:options items="${listBranch}"/>
                        </form:select>
                        <form:errors path="agentId" cssClass="error" element="div" />
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-6 pull-right">
                    <button class="btn btn-primary btn-label-left" type="submit">
                        <span><i class="fa fa-clock-o"></i></span> Save
                    </button>
                    <button class="btn btn-primary btn-label-left" id="back"
                            type="button">
                        <span><i class="fa fa-clock-o"></i></span> Back
                    </button>
                </div>
            </div>
            <form:input type="hidden" path="id"/>
        </div>
    </form:form>
</div>

<script>
    $(document).ready(function () {
//        LoadBootstrapValidatorScript(AccountFormEditValidator);
        $("#form [name='id']").focus();
    });
</script>
