<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--<%@ taglib uri="http://beepcall.com/jsp/taglibs" prefix="flo"%>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/branch-detail.js"/>'></script>

<input type="hidden" id="currBranchId" value="${branchId}">

<div>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li>FIRE!!!</li>
            </ol>
        </div>
    </div>
    <!--<input id="totalPage" type="hidden" value="${totalPage/pageSize}"/>-->
    <!--    <div class="form-search-branch-detail well">
            <a
                onclick="makeCall()"
                id="btnMakeCall"
                type="button" 
                href="#"
                class="btn btn-danger btn-sm"> Make call <span class="glyphicon glyphicon-plus" style="margin-left: 5px"></span></a>
        </div>-->
    <c:url var="fireButtonSrc" value="/resource/img/fire-button.png"/>
    <input type="image" src="${fireButtonSrc}" style="margin-left: auto;margin-right: auto;display: block;height: 350px" 

           data-toggle="modal" 
           title="Confirm fire warning" class="open-AddBookDialog " 
           href="#myModalFireWarning"/>
    <div>
        <label class="col-sm-12" style="text-align: center;font-size: 40px;color: #ffca00;margin-top: 20px;">CẢNH BÁO CHÁY</label>
    </div>
</div>

<!-- Modal Fire Warning -->
<div id="myModalFireWarning" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <form:form class="form-horizontal" role="form"
                           action="${formActionUpdate}" id="warningForm" modelAttribute="warningForm">
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-12 control-label" style="text-align: left">Bạn chắc chắn muốn cảnh báo cháy tới tất cả SĐT?</label>
                    </div>
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">Nội dung cảnh báo</label>
                        <div class="col-sm-8">
                            <input type="text" id="notifyContent" path="notifyContent" name="notifyContent"
                                   class="form-control" placeholder="Nội dung cảnh báo" maxlength="500"/>
                        </div>
                    </div>
                    <div style="text-align:right">
                        <button type="button" class="btn btn-default" id="btnConfirmFireWarning">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form:form>
            </div>
        </div>

    </div>
</div>
