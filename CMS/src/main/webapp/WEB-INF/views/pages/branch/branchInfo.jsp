<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<input type="hidden" id="previousPage" value="/agent/getBranch">
<div>

    <div class="messages">
        <c:if test="${feedbackMessage != null}">
            <div class="alert alert-success"><c:out value="${feedbackMessage}"/></div>
        </c:if>
        <c:if test="${errorMessage != null}">
            <div class="alert alert alert-danger"><c:out value="${errorMessage}"/></div>
        </c:if>
    </div>
    <c:if test="${typeView =='edit'}">
        <spring:url value="/agent/doEditBranch" htmlEscape="true"
                    var="formAction"></spring:url>
        <c:set var="disableTextOnEdit" value="true"/>
        <c:set var="breadcrumbTitle" value="Edit agent info"/>
    </c:if>
    <c:if test="${typeView =='new'}">
        <spring:url value="/agent/doAddNewBranch" htmlEscape="true"
                    var="formAction"></spring:url>
        <c:set var="disableTextOnEdit" value="false"/>
        <c:set var="breadcrumbTitle" value="Add new agent"/>
    </c:if>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <spring:url var="breadcrumbGetBranchUrl" value="/agent/getBranch" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li><a href="${breadcrumbGetBranchUrl}">Agent manager</a></li>
                <li><c:out value="${breadcrumbTitle}"/></li>
            </ol>
        </div>
    </div>
    <form:form class="form-horizontal" role="form"
               action="${formAction}" id="branchForm" modelAttribute="branch">
        <div class="jumbotron" style="max-width: 50%">
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">ID</label>

                <div class="col-sm-8">
                    <form:input type="text" path="code" id="code"
                                class="form-control" placeholder="Mã tòa nhà" disabled="${disableTextOnEdit}"/>
                    <form:errors path="code" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">Name</label>

                <div class="col-sm-8">
                    <form:input type="text" path="name" id="name"
                                class="form-control" placeholder="Tên tòa nhà"></form:input>
                    <form:errors path="name" cssClass="error" element="div" />
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-4 control-label">Address</label>

                <div class="col-sm-8">
                    <form:input type="text" path="addr" id="addr"
                                class="form-control" placeholder="Địa chỉ" />
                    <form:errors path="addr" cssClass="error" element="div" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-6 pull-right">
                    <button class="btn btn-primary btn-label-left" type="submit">
                        <span><i class="fa fa-clock-o"></i></span> Save
                    </button>
                    <button class="btn btn-primary btn-label-left" id="back"
                            type="button">
                        <span><i class="fa fa-clock-o"></i></span> Back
                    </button>
                </div>
            </div>
            <form:input type="hidden" path="id"/>
        </div>
    </form:form>
</div>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/branch.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script>
    $(document).ready(function () {
        LoadBootstrapValidatorScript(BranchFormEditValidator);
        $("#form [name='id']").focus();
    });
</script>
