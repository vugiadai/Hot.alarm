<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--<%@ taglib uri="http://beepcall.com/jsp/taglibs" prefix="flo"%>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/branch-detail.js"/>'></script>

<input type="hidden" id="currBranchId" value="${branchId}">

<div>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <spring:url var="breadcrumbGetBranchUrl" value="/getHome" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li><a href="${breadcrumbGetBranchUrl}">Quản lý tòa nhà</a></li>
                <li>Danh sách SĐT</li>
            </ol>
        </div>
    </div>
    <!--<input id="totalPage" type="hidden" value="${totalPage/pageSize}"/>-->
    <div class="form-search-branch-detail well">
        <form:form  id ="searchForm" >
            <table width="100%">
                <tr>
                    <td class=" col-sm-4" style="padding-left: 0px">
                        <!--<label >Số điện thoại </label>-->
                        <input type="text" id="phoneNumberSearch" class="form-control searchElement"
                               style="max-width: 95%" placeholder="SĐT"/>
                    </td>

                    <td class=" col-sm-4" >
                        <input type="text" id="nameSearch" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Họ tên"/>
                    </td>

                    <td class=" col-sm-4" >
                        <input type="text" id="descriptionSearch" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Ghi chú"/>
                    </td>

                </tr>
                <tr>
                <input type="hidden"
                       name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
                <td align="left">
                    <button id="btnSearchBranchDetail" class=" btn btn-warning btn-sm" type="button" style="margin-top: 5px">
                        <span><i class="fa fa-clock-o "></i></span>Search<span class="glyphicon glyphicon-search" style="margin-left: 5px"></span>
                    </button>
                </td>
                <td></td>
                <td></td>
                </tr>
            </table>
        </form:form>
    </div>
    <!--form-list-contact    -->
    <div id="form-list-contact">
        <div class="box-content no-padding">
            <div style="float:right; margin: auto">
                <a
                    data-toggle="modal" 
                    title="Confirm fire warning" 
                    href="#myModalFireWarning"
                    type="button" 
                    href="#"
                    class="open-AddBookDialog btn btn-danger btn-sm"> Báo động cháy <span class="glyphicon glyphicon-warning-sign" style="margin-left: 5px"></span></a>
                <a data-toggle="modal" 
                   title="Upload file" class="open-AddBookDialog btn-success btn btn-sm" 
                   href="#myModalUpfile"
                   style="margin-bottom: 5px;; margin-top: 5px; margin-left: 5px"
                   >Nhập từ file<span class="glyphicon glyphicon-plus" style="margin-left: 5px"></span>
                </a>
                <a data-toggle="modal" 
                   title="add Contact"
                   href="#myModalAddContact"
                   id="btnAddNewContact" class="open-AddContactDialog btn btn-success  btn-sm" 
                   type="button" 
                   style="margin-bottom: 5px; margin-top: 5px; margin-left: 5px">
                    <span><i class="fa fa-clock-o "></i></span> Thêm mới SĐT <span class="glyphicon glyphicon-plus" style="margin-left: 5px"></span>
                </a>
            </div>
            <table id="contactListTable"
                   class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
                <thead>
                    <tr>
                        <th class="hidden-xs col-sm-1 ">STT</th>
                        <th class="hidden-xs col-sm-1 ">SĐT</th>
                        <th class="hidden-xs col-sm-1 ">Họ tên</th>
                        <th class="hidden-xs col-sm-1 ">Ghi chú</th>
                        <th class="col-xs-4 col-sm-2 ">Trạng thái</th>
                        <th class="col-xs-4 col-sm-2 ">&nbsp;</th>
                    </tr>
                </thead>
                <tbody  id="tableBody" >
                    <c:set var="rowIndex" value="1"/>
                    <c:forEach var="contact" items="${listBranchDetail}">
                        <tr>		
                            <td  style="text-align: center; "><c:out value="${((curPage-1)*pageSize)  + rowIndex}"/></td>
                            <td  style="text-align: left; "><c:out value="${contact.msisdn}"/></td>
                            <td  style="text-align: left; "><c:out value="${contact.contactName}"/></td>
                            <td  style="text-align: left; "><c:out value="${contact.description}"/></td>
                            <c:if test="${contact.isActive == 'Y'}">
                                <td  style="text-align: center; "><i><span class="glyphicon glyphicon-ok" style="color:greenyellow"></span></i></td>
                                        </c:if>
                                        <c:if test="${contact.isActive == 'N'}">
                                <td  style="text-align: center; "><i><span class="glyphicon glyphicon-minus" style="color:red"></span></i></td>
                                        </c:if>
                                        <spring:url value="/doActiveContact/" var="activeUrl" htmlEscape="true"/>
                                        <spring:url value="/doDeactiveContact/" var="deactiveUrl" htmlEscape="true"/>
                                        <spring:url value="/doDeleteContact/" var="deleteUrl" htmlEscape="true"/>
                            <td class="col-xs-4 col-sm-2" style="text-align: center; ">
                                <a data-toggle="modal" 
                                   data-id="${contact.msisdn.concat(';').concat(contact.contactId)}"
                                   title="Confirm active contact" class="open-AddBookDialog " 
                                   href="#myModalActive"
                                   >Active</a>
                                <a data-toggle="modal" 
                                   data-id="${contact.msisdn.concat(';').concat(contact.contactId)}"
                                   title="Confirm deactive contact" class="open-AddBookDialog " 
                                   href="#myModalDeactive"
                                   >Deactive</a>
                                <a data-toggle="modal" 
                                   data-id="${contact.msisdn.concat(';').concat(contact.contactId)}"
                                   title="Confirm delete contact" class="open-AddBookDialog " 
                                   href="#myModalDelete"
                                   >Delete</a>
                            </td>
                            <c:set var="rowIndex" value="${rowIndex+1}"/>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <!--paging-->
            <c:set var="pagingPrefix" value="/agent/getBranchDetail"/>
            <%@include file="/WEB-INF/views/pages/layout/paginition.jsp" %>
        </div>
    </div>

</div>

<!-- Modal Active -->
<div id="myModalActive" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Bạn chắc chắn muốn kích hoạt SĐT <label class="object-name" style="color: red"></label>?</label>
                <input type="hidden" id="contactIdAct" class="object-id"/>
                <button type="button" class="btn btn-default" id="btnConfirmActiveContact">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>

    </div>
</div>
<!-- Modal Deactive -->
<div id="myModalDeactive" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Bạn muốn bỏ SĐT <label class="object-name" style="color: red"></label>?</label>
                <input type="hidden" id="contactIdDeact" class="object-id"/>
                <button type="button" class="btn btn-default" id="btnConfirmDeactiveContact">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>

    </div>
</div>
<!-- Modal Delete -->
<div id="myModalDelete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Bạn muốn xóa SĐT <label class="object-name" style="color: red"></label>?</label>
                <input type="hidden" id="contactIdDel" class="object-id"/>
                <button type="button" class="btn btn-default" id="btnConfirmDeleteContact">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>

    </div>
</div>

<!-- Modal Add Contact -->
<div id="myModalAddContact" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thêm mới SĐT</h4>
            </div>
            <div class="modal-body">
                <spring:url value="/doAddContact" htmlEscape="true"
                            var="formActionUpdate"></spring:url>
                <form:form class="form-horizontal" role="form"
                           action="${formActionUpdate}" id="addContactForm" modelAttribute="contactInfo">
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">SĐT</label>
                        <div class="col-sm-8">
                            <input type="text" id="phoneNumber" path="phoneNumber" name="phoneNumber"
                                   class="form-control" placeholder="Phone Number"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">Họ tên</label>
                        <div class="col-sm-8">
                            <input type="text" id="contactName" path="contactName" name="contactName"
                                   class="form-control" placeholder="Contact Name"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">Chú thích</label>
                        <div class="col-sm-8">
                            <input type="text" id="description" path="description" name="description"
                                   class="form-control" placeholder="Description"/>
                        </div>
                    </div>
                    <div class="form-group control-group" style="margin-bottom: 0px">
                        <button onclick="addContact()" style="float: right;margin-left: 5px ;margin-right: 15px" type="button" class="btn btn-default " id="btnAddContact">Save</button>
                        <button id="hideModalAddContact" style="float: right" type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>
                    <!--</div>-->
                </form:form>
            </div>
        </div>

    </div>
</div>

<!-- Modal Upload file -->
<div id="myModalUpfile" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload file to call automatically</h4>
            </div>
            <div class="modal-body">
                <!--  upload file-->
                <spring:url value="/uploadfile" htmlEscape="true" var="uploadfile"></spring:url>
                <spring:url value="/download" htmlEscape="true" var="download"></spring:url>
                <form class="form-horizontal" id="uploadFileForm" onsubmit="return validateFile()" method="POST" action="${uploadfile}?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data" target="frm"> 
                    <input id="branchIdUpload" type="hidden" name="branchId" class="form-control" />
                    <div class="form-group control-group">
                        <a href="${download}?${_csrf.parameterName}=${_csrf.token}"> Download sample template</a>
                    </div>
                    <div class="col-sm-8">
                        <input id="file" type="file" name="file" class="form-control" style="padding: 0px" />
                    </div>
                    <button id="btnDlgUpload" type="button" onclick="upload()" class="btn btn-primary">Upload</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Fire Warning -->
<div id="myModalFireWarning" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <form:form class="form-horizontal" role="form"
                           action="${formActionUpdate}" id="warningForm" modelAttribute="warningForm">
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-12 control-label" style="text-align: left">Bạn chắc chắn muốn cảnh báo cháy tới tất cả SĐT?</label>
                    </div>
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">Nội dung cảnh báo</label>
                        <div class="col-sm-8">
                            <input type="text" id="notifyContent" path="notifyContent" name="notifyContent"
                                   class="form-control" placeholder="Nội dung cảnh báo" maxlength="500"/>
                        </div>
                    </div>
                    <div style="text-align:right">
                        <button type="button" class="btn btn-default" id="btnConfirmFireWarning">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form:form>
            </div>
        </div>

    </div>
</div>

<iframe id="frm" name="frm" style="display: none;"></iframe>