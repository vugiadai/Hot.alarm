<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--<%@ taglib uri="http://beepcall.com/jsp/taglibs" prefix="flo"%>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--<script src="//content.jwplatform.com/libraries/eX4mp13.js"></script>-->
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/callinfo.js"/>'></script>
<!--<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/jwplayer/jwplayer/jwplayer.js"/>'></script>-->
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/jwplayer/jwplayer.js"/>'></script>


<div>
    <!--<spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>-->

    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li>Lịch sử cuộc gọi</li>
            </ol>
        </div>
    </div>
    <!--<input id="totalPage" type="hidden" value="${totalPage/pageSize}"/>-->
    <div class="form-search-branch-detail well">
        <!--<div id="myElement">Loading the player...</div>-->
        <form:form  id ="searchForm" commandName="toSearchForm">
            <table width="100%">
                <tr>
                    <td >
                        <div style="max-width: 95%">
                            <!--                        <input type="text" id="packageId" class="form-control searchElement"
                                                           style="max-width: 95%" placeholder="Find by event id"/>-->
                            <form:select path="branchId" cssClass="textArea form-control" id="branchId">
                                <option value="" label=" - Chọn tòa nhà - " ></option>
                                <form:options items="${listBranch}"/>
                            </form:select>
                        </div>
                    </td>
                    <td >
                        <!--<label >Mã đại lý </label>-->
                        <input type="text" id="phoneNumber" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Tìm theo SĐT"/>
                    </td>
                    <td style="padding-left: 0px">
                        <div class="control-group" style="max-width: 95%">
                            <div class="controls">
                                <div class="input-group">
                                    <input id="txtFromDateSearch" type="text" class="date-picker form-control searchElement" 
                                           placeholder="Thời điểm gọi tính từ"/>
                                    <label for="txtFromDateSearch" class="input-group-addon btn" style="display: table-column">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td >
                        <div class="control-group" style="max-width: 95%">
                            <div class="controls">
                                <div class="input-group">
                                    <input id="txtToDateSearch" type="text" class="date-picker form-control searchElement" 
                                           placeholder="Thời điểm gọi tính tới"/>
                                    <label for="txtToDateSearch" class="input-group-addon btn" style="display: table-column">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                <input type="hidden"
                       name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
                <td align="left">
                    <button id="btnSearchCallinfo" class=" btn btn-warning btn-sm" type="button" style="margin-top: 5px">
                        <span><i class="fa fa-clock-o "></i></span>Search<span class="glyphicon glyphicon-search" style="margin-left: 5px"></span>
                    </button>
                </td>
                <td></td>
                <td></td>
                </tr>
            </table>
        </form:form>
    </div>
    <!--form-list-contact    -->
    <div id="form-list-contact">
        <div class="box-content no-padding">
            <table id="contactListTable"
                   class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
                <thead>
                    <tr>
                        <th class="hidden-xs col-sm-1 ">STT</th>
                        <th class="hidden-xs col-sm-1 ">Tòa nhà</th>
                        <th class="hidden-xs col-sm-1 ">Mã cuộc gọi</th>
                        <th class="hidden-xs col-sm-1 ">Số ĐT</th>
                        <th class="hidden-xs col-sm-1 ">Đầu số</th>
                        <th class="hidden-xs col-sm-1 ">SĐT tiếp nhận</th>
                        <th class="hidden-xs col-sm-1 ">Thời điểm gọi</th>
                        <th class="hidden-xs col-sm-1 ">Thời điểm nhấc máy</th>
                        <th class="hidden-xs col-sm-1 ">Thời lượng</th>
                        <th class="hidden-xs col-sm-1 ">File</th>
                    </tr>
                </thead>
                <tbody  id="tableBody" >
                    <c:set var="rowIndex" value="1"/>
                    <c:forEach var="callInfo" items="${listCallInfo}">
                        <tr>		
                            <td  style="text-align: center; "><c:out value="${((curPage-1)*pageSize)  + rowIndex}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.agentName}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.callId}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.caller}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.called}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.agentId}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.startTime}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.firstConnectTime}"/></td>
                            <td  style="text-align: left; "><c:out value="${callInfo.acdTime}"/></td>
                            <td  style="text-align: left; ">
                                <c:if test="${not empty callInfo.url}">
                        <video width="100" height="40" controls="" name="media" >
                            <source src="<c:out value="${callInfo.url}"/>" type="audio/wav"/>
                        </video>
                    </c:if>
                    </td>
                    <c:set var="rowIndex" value="${rowIndex+1}"/>
                    </tr>
                </c:forEach>
                <!--<tr>		
                                            <td  style="text-align: center; "><c:out value="345"/></td>
                                            <td  style="text-align: left; "><c:out value="345"/></td>
                                            <td  style="text-align: left; "><c:out value="3453456"/></td>
                                            <td  style="text-align: left; "><c:out value="63456"/></td>
                                            <td  style="text-align: left; "><c:out value="345re6346"/></td>
                                            <td  style="text-align: left; "><c:out value="34563456"/></td>
                                            <td  style="text-align: left; "><c:out value="34563456"/></td>
                                            <td  style="text-align: left; "><c:out value="34563456"/></td>
                                            <td  style="text-align: left; "><c:out value="2345g"/></td>
                                            <td  style="text-align: left; ">
                                                <video width="100" height="40" controls name="media" >
                                                    <source src="https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3" type="audio/mpeg">
                                                </video>
                                            </td>-->
                <c:set var="rowIndex" value="${rowIndex+1}"/>
                </tr>
                </tbody>
            </table>

            <!--paging-->
            <c:set var="pagingPrefix" value="/agent/getCallInfo"/>
            <%@include file="/WEB-INF/views/pages/layout/paginition.jsp" %>
        </div>
    </div>

</div>

<!-- Modal Active -->
<div id="myModalActive" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Active contact <label class="object-name" style="color: red"></label>. Are you sure?</label>
                <input type="hidden" id="contactIdAct" class="object-id"/>
                <button type="button" class="btn btn-default" id="btnConfirmActiveContact">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>

    </div>
</div>
