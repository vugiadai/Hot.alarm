<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url value="/getHome" var="homeUrl" htmlEscape="true" />
<spring:url value="/agent/getBranch" var="getBranchUrl" htmlEscape="true" />
<spring:url value="/agent/fireButton" var="getFireButtonUrl" htmlEscape="true" />
<spring:url value="/agent/getCallInfo" var="getCallInfoUrl" htmlEscape="true" />
<spring:url value="/transaction/getTransaction" var="getTransactionUrl" htmlEscape="true" />
<spring:url value="/winner/getWinner" var="getWinnerUrl" htmlEscape="true" />
<spring:url value="/contact/getContact" var="getPlayerUrl" htmlEscape="true" />
<spring:url value="/account/getAccount" var="accountManagerUrl" htmlEscape="true" />
<spring:url value="/package/getPackage" var="packageManagerUrl" htmlEscape="true" />
<spring:url value="/revenue/getRevenue" var="getRevenueUrl" htmlEscape="true" />
<spring:url value="/payment/getPayment" var="getPaymentUrl" htmlEscape="true" />
<spring:url value="/moReport/getMOReport" var="getMORecordUrl" htmlEscape="true" />
<spring:url value="/mtReport/getMTReport" var="getMTRecordUrl" htmlEscape="true" />
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%--<div class="sidebar-offcanvas" id="sidebar" role="navigation">
            <ul class="sidebar-nav">
              <li ><a href="${homeUrl}">Home</a></li>
              <li><a href="${getBranchUrl}">Branch Management</a></li>
              <li><a href="${getTransactionUrl}">Transaction</a></li>        
              <li><a href="${getWinnerUrl}">Winner</a></li>  
              <li><a href="${getPlayerUrl}">Player</a></li>  
              <li><a href="${accountManagerUrl}">Account manager</a></li>      
            </ul>
        </div>--%>
<style>
    .green {
        color: #999999;
    }
</style>

<nav class="navbar navbar-default">
    <!--     data-spy="affix" data-offset-top="197"-->
    <div class="container-fluid" style="width: 100%">
        <div class="nav-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
        </div>
        <div class=" collapse navbar-collapse" id="myNavbar" >
            <!--            <ul class="nav navbar-nav">-->
            <ul class="nav nav-pills nav-stacked ">
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li style="width: 100%;"><a href="${accountManagerUrl}">Quản lý người dùng <span class="glyphicon glyphicon-user green" style="float: right"></span></a></li>   
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_AGENT')">
                        <li style="width: 100%;"><a href="${getFireButtonUrl}">Báo cháy<span class="glyphicon glyphicon-folder-close green" style="float: right"></span></a></li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_AGENT')">

                    <li style="width: 100%;"><a href="${getBranchUrl}">Quản lý tòa nhà<span class="glyphicon glyphicon-folder-close green" style="float: right"></span></a></li>
                    <li style="width: 100%;"><a href="${homeUrl}">Sự kiện báo cháy<span class="glyphicon glyphicon-home green" style="float: right"></span></a></li>
                    <li style="width: 100%;"><a href="${getPlayerUrl}">Danh sách gọi báo cháy<span class="glyphicon glyphicon-book" style="float: right"></span></a></li>
                    <li style="width: 100%;"><a href="${getCallInfoUrl}">Lịch sử cuộc gọi<span class="glyphicon glyphicon-stats green" style="float: right"></span></a></li>
                        </sec:authorize>
            </ul>
        </div>
    </div>
</nav>