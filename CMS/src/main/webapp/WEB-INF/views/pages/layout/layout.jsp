<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
    "http://www.w3.org/TR/html4/loose.dtd"> 
<%
    request.setAttribute("contextPath", request.getContextPath());
%>
<html lang="en" >
    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="_csrf" content="${_csrf.token}"/>
        <!-- default header name is X-CSRF-TOKEN -->
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        <title>
            <tiles:getAsString name="title" ignore="true" />
        </title>
        <link rel="shortcut icon" href='<spring:url value="/resource/img/favicon.ico"/>'>
        <link href='<spring:url value="/resource/css/bootstrap/bootstrap.min.css"/>' rel="stylesheet"></link>
        <link href='<spring:url value="/resource/css/jQuery/jquery-ui.min.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/jquery.fancybox.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/fullcalendar.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/xcharts.min.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/select2.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/style.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resource/css/bootstrap/bootstrapValidator.min.css"/>' rel="stylesheet">

        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery-3.1.1.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery.validate.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/additional-methods.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery-ui.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/bootstrap.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/jquery.justifiedgallery.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/tinymce.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/select2.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootbox.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/bootstrap-tooltip.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/bootstrap-confirmation.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery.confirm.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/bootstrapValidator.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/jQuery/jquery.dataTables.min.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/bootstrap/dataTables.bootstrap.js"/>'></script>
        <script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
        <%--<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/waitingUtil.js"/>'></script>--%>

        <style>
            .loader {
                border: 10px solid #f3f3f3;
                border-radius: 50%;
                border-top: 10px solid #D9534F;
                width: 80px;
                height: 80px;
                -webkit-animation: spin 5s linear infinite;
                animation: spin 5s linear infinite;
                z-index: 999999;
                position: fixed; 
                top:20%; 
                left:50%;

            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }


            #loading {
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                position: fixed;
                display: block;
                opacity: 0.7;
                background-color: #fff;
                z-index: 9999999;

            }

            #loading-image {
                position: absolute;
                top: 100px;
                left: 240px;
                z-index: 100;
            }


        </style>


        <style>
            .container-fluid {
                padding-right: 0px;
                padding-left: 0px;
            }

            .navbar-collapse {
                padding-right: 0px;
                padding-left: 0px;
            }
            .page-header{ height: 50px;padding-bottom:9px;margin:0px 0 5px;border-bottom:1px solid #eee}

            .well{margin-bottom: 0px;padding: 0px}

            /*.btn{padding: 4px 12px}*/

            .form-control{height: 30px}

            .table{margin-bottom: 0px}

            .pagination{margin: 5px 0px 5px 5px }

            th{ text-align: center;
                background-color: #3C8DBC;
                color: white;
            }

            #menu{padding-right: 0px}

            * {
                border-radius: 0 !important;
                -moz-border-radius: 0 !important;
            }
            #mainRow {
                margin-right: 0px;
            }
            .form-control{
                margin-top: 5px;
            }


        </style>

    </head>

    <!--bien dung chung-->
    <sec:authentication property="principal.username" var="userNameHello"/>
    <sec:authentication property="principal.authorities" var="userRoles"/>

    <body class="main-body">
        <!--    <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">-->
        <tiles:insertAttribute name="header" />
        <!--        </div>
            </nav>-->

        <div style="background-color: inherit">
            <!--Start Container-->
            <div id="main" class="container-fluid">
                <div id="mainRow" class="row">
                    <div id="menu" class="col-xs-12 col-sm-2">
                        <tiles:insertAttribute name="menu" />
                    </div>
                    <!--Start Content-->
                    <!--thong bao loi, dua len dau trang-->
                    <div id="content" class="col-xs-12 col-sm-10">
                        <div class="messages">
                            <c:if test="${feedbackMessage != null}">
                                <div class="alert alert-success"><c:out value="${feedbackMessage}"/></div>
                            </c:if>
                            <c:if test="${errorMessage != null}">
                                <div class="alert alert alert-danger"><c:out value="${errorMessage}"/></div>
                            </c:if>
                        </div>
                        <!--Noi dung trang-->

                        <!--                        bien luu tong so trang-->
                        <input id="totalRecord" type="hidden" value="${totalRecord}"/>
                        <input id="curPage" type="hidden"  value="${curPage}"/>
                        <input id="pageSize" type="hidden" value="${pageSize}"/>

                        <tiles:insertAttribute name="body" />
                    </div>
                    <!--End Content-->
                    <%--<tiles:insertAttribute name="footer" />--%>
                </div>
            </div>
        </div>
        <div id="loading" style="display: none">
            <div class = "loader"></div>
        </div>
    </body>  

</html>  
