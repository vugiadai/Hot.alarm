<%-- 
    Document   : header
    Created on : Sep 27, 2016, 9:53:22 AM
    Author     : Duongpx2
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>

<c:url value="/j_spring_security_logout" var="logoutUrl" />
<c:url value="/getChangePass" var="changePassUrl" />




<div  class="page-header" style="background-color: #3c8dbc"  >
    <!--            #639af2">-->
    <!--             #8eb9ff-->
    <table width="99%" >
        <tr>
            <spring:url value="/getHome" var="homeUrl" htmlEscape="true"/>
            <spring:url value="/resource/img/Logo.jpg" var="imageUrl" htmlEscape="true"/>
            <c:set value="${sessionScope.username}" var="helloUserName"/>
            <td style="text-align: left">
                <a href="${homeUrl}"><img src="${imageUrl}" style="height: 49px"/></a> 
            </td>
            <td style="text-align: right">
                <div>
                    <div class="dropdown">



                        <button class="btn btn-danger btn-group dropdown-toggle" type="button" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-user" ></span>
                            <sec:authorize access="isAuthenticated()">
                                <sec:authentication property="principal.username" /> 
                            </sec:authorize>
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="${changePassUrl}">Change password</a></li>
                            <li>
                                <a id="logout" href="#">Logout</a>
                                <form id="logoutForm" action="${logoutUrl}" method="post" style="display: none">
                                    <input type="submit" value="Logout"/>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<!--<a href="${logoutUrl}">Log Out</a>-->
