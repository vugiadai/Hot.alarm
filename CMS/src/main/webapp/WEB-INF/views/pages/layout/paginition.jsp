<%-- 
    Document   : paginition
    Created on : Sep 29, 2016, 12:40:12 AM
    Author     : pxduong
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<script>
    $(document).ready(function () {
        updatePaginition();
    });

    function updatePaginition(curPage, pageSize, totalRecord) {
        if (typeof curPage == 'undefined' || curPage == undefined) {
            curPage = parseInt($("#curPage").val());
        } else {
            $("#curPage").val(curPage);
        }
        if (typeof pageSize == 'undefined' || pageSize == undefined) {
            pageSize = parseInt($("#pageSize").val());
        } else {
            $("#pageSize").val(pageSize);
        }
        if (typeof totalRecord == 'undefined' || totalRecord == undefined) {
            totalRecord = parseInt($("#totalRecord").val());
        } else {
            $("#totalRecord").val(totalRecord);
        }
        if (totalRecord > 0) {
            var totalPage = Math.floor((totalRecord - 1) / pageSize) + 1;
        } else {
            var totalPage = Math.floor(totalRecord / pageSize) + 1;
        }
        $("#noItemShow").text(pageSize);
        $("#lblTotalPage").text(totalPage);

        $("#goToPage").val(curPage);

        $("#lblTotalRecord").text(totalRecord);
        
        var uList = $("#listPageToClick");
        uList.html("");
//        previous
        if (curPage != 1) {
            uList.append("<li><a onclick=\"setCurPage(" + (curPage - 1) + ")\" href=\"#\">Previous</a></li>");
        }
//        pages
        var startPageSelect = 1;
        var stopPageSelect = totalPage;
        if (totalPage > 5) {
            if (curPage > 2) {
                if (curPage < totalPage - 1) {
                    startPageSelect = curPage - 2;
                    stopPageSelect = curPage + 2;
                } else {
                    startPageSelect = totalPage - 4;
                }
            } else {
                stopPageSelect = 5;
            }
        }
        for (i = startPageSelect; i <= stopPageSelect; i++) {
            if (i == curPage) {
                uList.append("<li class=\"active\"><a>" + i + "</a></li>");
            } else {
                uList.append("<li class=\"hidden-xs\" ><a onclick=\"setCurPage(" + i + ")\" href=\"#\">" + i + "</a></li>");
            }
        }
        //        previous
        if (curPage != totalPage) {
            uList.append("<li><a onclick=\"setCurPage(" + (curPage + 1) + ")\" href=\"#\">Next</a></li>");
        }
//        $("#paginitionTable").html("");
//        $("#paginitionTable").append(tr);
    }

</script>
<div>
    <table id="paginitionTable" style="width: 100%">
        <tr>
            <td class="col-xs-4 col-sm-4" style='text-align: left;'>

                <div class="dropdown">
                    <label>Show</label>
                    <button class="btn btn-default dropdown-toggle" id="noItemShow" type="button" data-toggle="dropdown">
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" onclick="setPageSize(10)" href="#">10 rows</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" onclick="setPageSize(20)" href="#">20 rows</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" onclick="setPageSize(30)" href="#">30 rows</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" onclick="setPageSize(50)" href="#">50 rows</a></li>
                    </ul>
                    &nbsp;<label id="lblTotalRecord">0</label>  rows
                </div>
            </td>
            <td class="col-xs-4 col-sm-4" style="text-align: center">
                <label for="goToPage" >Go to Page</label>
                <input id="goToPage" style="width: 30px"/>
                <label for="goToPage" >/</label>
                <label id="lblTotalPage" for="goToPage" >...</label>
            </td>
            <td class="col-xs-4 col-sm-4" style="text-align: right; padding: 0px">
                <div style="float:right">
                    <ul id="listPageToClick" class="pagination" >
                    </ul>
                </div>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#goToPage").keypress(function (e) {
            if (e.which == 13) {
                var page = $("#goToPage").val();
                setCurPage(page);
            }
        });
    });


</script>
