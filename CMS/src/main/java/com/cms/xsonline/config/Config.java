/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.config;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author pcduongit
 */
//@Configuration
public class Config {

    private static Config instance;
    private String countryCode;
    private String timeCheckMakeCall;

    public static Config getInstance() {
//        if (instance == null) {
            instance = new Config();
//        }
        return instance;
    }

    private Config() {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("./classes/app_config.cfg"));
            countryCode = prop.getProperty("country_code");
            timeCheckMakeCall = prop.getProperty("time_check_make_call");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTimeCheckMakeCall() {
        return timeCheckMakeCall;
    }

    public void setTimeCheckMakeCall(String timeCheckMakeCall) {
        this.timeCheckMakeCall = timeCheckMakeCall;
    }
    
    

}
