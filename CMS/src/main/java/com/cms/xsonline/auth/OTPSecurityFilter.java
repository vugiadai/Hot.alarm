/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.auth;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pxduong
 */
public class OTPSecurityFilter implements Filter {
    
    @Override
    public void init(FilterConfig fc) throws ServletException {
        System.out.println("Init Filter");
    }
    
    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) sr;
        String path = req.getRequestURI();
        if (path.contains("login")
                || path.contains("signup")
                || path.contains("logout")
                || path.contains("resource")
                || path.contains("getVerifyOTPLogin")
                || path.contains("doVerifyOTPLogin")
                || path.contains("captcha")) {
            fc.doFilter(sr, sr1);
            return;
        }
        HttpSession session = req.getSession();
        if (session.getAttribute("isOTPValidate") != null && session.getAttribute("isOTPValidate").toString().equals("true")) {
            fc.doFilter(sr, sr1);
        } else {
            // Chuy?n h??ng t?i 'image not found' image.
            HttpServletResponse resp = (HttpServletResponse) sr1;

            // ==> /ServletFilterTutorial + /images/image-not-found.png
            resp.sendRedirect(req.getContextPath() + "/getVerifyOTPLogin");
        }
    }
    
    @Override
    public void destroy() {
        System.out.println("Destroy Filter");
    }
    
}
