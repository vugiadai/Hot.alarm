/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.auth;

import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.RoleDTO;
import com.cms.xsonline.service.AccountService;
import com.cms.xsonline.util.MD5Util;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Duongpx2
 */
@Component
public class AuthenticateProv implements AuthenticationProvider {

    private AccountService accountService;

    static Logger log = Logger.getLogger(AuthenticateProv.class.getName());

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setPersonService(AccountService as) {
        this.accountService = as;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName().toLowerCase();
        String password = authentication.getCredentials().toString();
        List<AccountDTO> listAccount = this.accountService.getByUserName(name);
        if (listAccount.size() > 0) {
            AccountDTO accountInfo = listAccount.get(0);
            String MD5Pass = MD5Util.getSecurePassword(password.concat(Strings.isNullOrEmpty(accountInfo.getSalt()) ? "" : accountInfo.getSalt()));
//            System.out.println("passsss->"+MD5Pass);
//            log.fatal("passsss->"+MD5Pass);
            if (MD5Pass.equals(accountInfo.getPassword())) {//dang nhap thanh cong
                //tim cac quyen va gan vao user
                List<RoleDTO> listRoles = this.accountService.getRoleByUserId(accountInfo.getId());
                if (listRoles != null) {
                    List<GrantedAuthority> grantedAuths = new ArrayList<>();
                    List<String> listRoleName = new ArrayList<>();
                    for (RoleDTO role : listRoles) {
                        listRoleName.add(role.getRoleName().toLowerCase());
                        String roleStr = role.getRoles();
                        if (roleStr != null && roleStr != "") {
                            String[] roleList = roleStr.split("[|]");
                            for (String roleTemp : roleList) {
                                grantedAuths.add(new SimpleGrantedAuthority(roleTemp.trim()));
                            }
                        }
                    }
                    accountInfo.setListRoleName(listRoleName);
                    accountInfo.setListRoles(grantedAuths);
                    //verify qua tin nhan. Neu gui tin nhan thanh cong thi moi coi nhu qua buoc dang nhap
//                    if (this.accountService.sentOTPVerify(accountInfo) > 0) {
                    Authentication auth = new UsernamePasswordAuthenticationToken(accountInfo, accountInfo.getPassword(), accountInfo.getListRoles());
                    return auth;
//                    }
                }
            }
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
