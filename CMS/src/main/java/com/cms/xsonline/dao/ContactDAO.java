/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dao;

import com.cms.xsonline.base.BaseDAO;
import com.cms.xsonline.dto.PlayerDTO;
import com.cms.xsonline.dto.PlayerQueryDTO;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.Utilities;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author pxduong
 */
@Repository
public class ContactDAO extends BaseDAO {

    public Integer getCount(PlayerQueryDTO query) {
        Object[] strQuery = getStrQuery(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                int rowCount = dataRow.getInt("COUNT");
                return rowCount;
            }
        }
        return 0;
    }

    public List<PlayerDTO> getByPage(PlayerQueryDTO query) {
        List<PlayerDTO> result = new ArrayList<>();
        Object[] strQuery = getStrQuery(0, query);
        if (strQuery != null) {
            String sql = strQuery[0].toString();
            Object[] parameterList = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(sql, parameterList);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = extractDataRow(dataRow);
        }
        return result;
    }

    private List<PlayerDTO> extractDataRow(SqlRowSet dataRow) {
        List<PlayerDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            PlayerDTO temp = new PlayerDTO();
            temp.setDatetime(dataRow.getString("request_time"));
            // temp.setMakeCallTime(Utilities.getDateStr(dataRow.getDate("MAKE_CALL_TIME")));
            temp.setMakeCallTime(dataRow.getString("MAKE_CALL_TIME"));
            temp.setCallId(dataRow.getString("CALL_ID"));
            temp.setPhoneNumber(dataRow.getString("PHONE_NUMBER"));
            temp.setPackageId(dataRow.getLong("PACKAGE_ID"));
            temp.setRes(dataRow.getString("RES"));
            temp.setRetryTotal(dataRow.getInt("RETRY_TOTAL"));
            temp.setSystemStatus(dataRow.getString("SYSTEM_STATUS"));
            result.add(temp);
        }
        return result;
    }

    public Date getGFile(Long id) {
        String queryString = "select updated_date from D_g_file where id = " + id;
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getDate("updated_date");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object[] getStrQuery(int type, PlayerQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        switch (type) {
            case 1:
                //count
                sql.append(" SELECT COUNT(*) COUNT FROM ( ");
                break;
            case 0:
                //select *
                sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
                break;
            default:
                return null;
        }
        sql.append("SELECT V.CALL_ID, to_char(V.request_time,'dd/MM/yyyy hh24:mi:ss') request_time, "
                + "to_char(V.MAKE_CALL_TIME,'dd/MM/yyyy hh24:mi:ss') MAKE_CALL_TIME, "
                + "V.msisdn PHONE_NUMBER,V.PACKAGE_ID,V.RETRY_TOTAL,V.SYSTEM_STATUS, V.RES\n"
                + " FROM POKE_CALL V   "
                + " JOIN G_FILE COE ON V.PACKAGE_ID = COE.ID "
                + " LEFT JOIN D_CONTACT C ON C.CONTACT_ID = V.CONTACT_ID "
                + " WHERE 1=1 ");
//        if (query.getPackageId() != null) {
//            // tim theo thoi diem upload data file do row duoc chen gan tuc thoi
//            Date uploadedDate = this.getGFile(query.getPackageId());
//            if (uploadedDate == null) {
//                //id khong ton tai
//                return null;
//            }
//            query.setFromDate(uploadedDate);
//            sql.append(" where V.request_time >= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
//            params.add(Utilities.getDateStr(query.getFromDate()));
//            sql.append(" AND V.request_time <= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') + 1 ");
//            params.add(Utilities.getDateStr(query.getToDate()));
//        } else {
//            sql.append(" where 1=1");
//            if (query.getFromDate() != null) {
//                sql.append(" AND V.request_time >= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
//                params.add(Utilities.getDateStr(query.getFromDate()));
//            }
//            if (query.getToDate() != null) {
//                sql.append(" AND V.request_time <= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
//                params.add(Utilities.getDateStr(query.getToDate()));
//            }
//        }
        if (query.getAgentId() != null) {
            sql.append(" AND COE.AGENT_ID = ? ");
            params.add(query.getAgentId());
        }
        if (query.getFromDate() != null) {
            sql.append(" AND V.request_time >= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getFromDate()));
        }
        if (query.getToDate() != null) {
            sql.append(" AND V.request_time <= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getToDate()));
        }
        if (query.getPhone() != null) {
            sql.append(" AND V.msisdn like ?");
            params.add("%" + query.getPhone() + "%");
        }
        if (query.getPackageId() != null) {
            sql.append(" AND V.PACKAGE_ID = ?");
            params.add(query.getPackageId());
        }
        if (query.getRes() != null && !"".equals(query.getRes().trim())) {
            sql.append(" AND V.res in ").append(toList(query.getRes()));
        }
//        sql.append(" ORDER BY V.request_time DESC ");
        sql.append(" ORDER BY V.request_time DESC  ");
        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
        System.out.println("sql = " + sql);
        return new Object[]{sql, params.toArray()};
    }

    public static String toList(String param) {
        StringBuilder builder = new StringBuilder("('-1'");

        if (param.contains(",")) {
            String[] temps = param.split(",", -1);
            for (String temp : temps) {
                if (!"".equals(temp.trim())) {
                    builder.append(",'").append(temp.trim()).append("'");
                }
            }

        } else {

            builder.append(",'").append(param.trim()).append("'");
        }
        builder.append(")");
        return builder.toString();
    }

    public int settingNotification(int userId, boolean enableOrNot) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE D_CONTACT SET IS_ACTIVE = ? WHERE USER_ID = ?");
            return getJdbcTemplate().update(sql.toString(), enableOrNot ? "Y" : "N", userId);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
