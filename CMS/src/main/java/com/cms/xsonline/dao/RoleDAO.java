/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dao;

import com.cms.xsonline.base.BaseDAO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.RoleDTO;
import com.cms.xsonline.util.Utilities;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author duongpx2
 */
@Repository
public class RoleDAO extends BaseDAO {

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    private void initialisze() {
        setDataSource(dataSource);
    }

    public List<RoleDTO> getRoleByUserId(Long userId) {
        List<RoleDTO> result = new ArrayList<>();
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("select u.USER_ID,ur.ROLE_ID ,r.ROLE_NAME, r.ROLES from D_USERS u \n"
                    + "inner join D_USER_ROLE ur on u.user_id = ur.user_id\n"
                    + "inner join D_ROLES r on ur.ROLE_ID = r.ROLE_ID where u.USER_ID = ? "
                    + "AND u.IS_ACTIVE=1 AND ur.IS_ACTIVE = 1");

            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), new Object[]{userId});
            while (dataRow != null && dataRow.next()) {
                RoleDTO temp = new RoleDTO();
                temp.setRoleId(dataRow.getInt("ROLE_ID"));
                temp.setRoleName(dataRow.getString("ROLE_NAME"));
                temp.setRoles(dataRow.getString("ROLES"));
                result.add(temp);
            }
        } catch (Exception ex) {
            //log
        }
        return result;
    }

    public List<RoleDTO> getAllRoles() {
        List<RoleDTO> result = new ArrayList<>();
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("select * from D_roles");
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), new Object[]{});
            while (dataRow != null && dataRow.next()) {
                RoleDTO temp = new RoleDTO();
                temp.setRoleId(dataRow.getInt("ROLE_ID"));
                temp.setRoleName(dataRow.getString("ROLE_NAME"));
                result.add(temp);
            }
        } catch (Exception ex) {
            //log
        }
        return result;
    }

    public int insertUserRole(Long userId, Long roleId) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO D_USER_ROLE (USER_ID, ROLE_ID, IS_ACTIVE) VALUES (?,?,?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{userId,
                roleId,
                1
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }
}
