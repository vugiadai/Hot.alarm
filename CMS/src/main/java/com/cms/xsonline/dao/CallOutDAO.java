/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dao;

import com.cms.xsonline.base.BaseDAO;
import com.cms.xsonline.config.Config;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.CallOutEventDTO;
import com.cms.xsonline.dto.CallOutEventQueryDTO;
import com.cms.xsonline.util.Constant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author duongpx2
 */
@Repository
public class CallOutDAO extends BaseDAO {

    public Integer getCount(CallOutEventQueryDTO query) {
        Object[] strQuery = getStrQuery(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("COUNT");
            }
        }
        return 0;
    }

    public List<CallOutEventDTO> getCallOutEventById(Long id) {
        List<CallOutEventDTO> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        sql.append("SELECT * FROM G_FILE WHERE ID = ?");
        params.add(id);
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), params.toArray());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = extractCallOutEvent(dataRow);
        return result;
    }

    public List<CallOutEventDTO> getCallOutEvents(CallOutEventQueryDTO query) {
        List<CallOutEventDTO> result = new ArrayList<>();
        Object[] strQuery = getStrQuery(0, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = extractCallOutEvent(dataRow);
        }
        return result;
    }

    public int makeCallOutEvent(CallOutEventDTO event) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO G_FILE"
                    + "(ID, AGENT_ID, CREATED_BY, CREATED_DATE, STATUS,"
                    + " TOTAL_ROW, TOTAL_COMMIT_ROW, UPDATED_BY, UPDATE_DATE, DONE_DATE) "
                    + "VALUES (G_FILE_SEQ.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                event.getAgentId(),
                event.getCreatedBy(),
                event.getCreatedDate(),
                event.getStatus(),
                event.getTotalRow(),
                event.getTotalCommitRow(),
                event.getUpdateBy(),
                event.getUpdateDate(),
                event.getDoneDate()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int makeCallOutEvent(CallOutEventDTO event, Long id) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO G_FILE"
                    + "(ID, AGENT_ID, CREATED_BY, CREATED_DATE, STATUS,"
                    + " TOTAL_ROW, TOTAL_COMMIT_ROW, UPDATED_BY, UPDATED_DATE, DONE_DATE, NOTIFY_CONTENT) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                id,
                event.getAgentId(),
                event.getCreatedBy(),
                event.getCreatedDate(),
                event.getStatus(),
                event.getTotalRow(),
                event.getTotalCommitRow(),
                event.getUpdateBy(),
                event.getUpdateDate(),
                event.getDoneDate(),
                event.getNotifyContent()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

//    public int genCallOutDetail(Long callOutEventId, CallOutEventDTO event) {
//        try {
//            StringBuilder queryString = new StringBuilder();
//            queryString.append("INSERT INTO CALL_OUT_DETAIL "
//                    + "(CALL_OUT_DETAIL_ID, CALL_OUT_EVENT_ID, AGENT_ID, MSISDN, CREATED_DATE, STATUS, EXPIRED_DATE) "
//                    + " SELECT CALL_OUT_DETAIL_SEQ.nextval, " + callOutEventId + ", " + event.getAgentId() + ", MSISDN, "
//                    + " ?");
//
//            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
//                event.getAgentId(),
//                event.getCreatedBy(),
//                new Date(),
//                Constant.CallOutEventStatus.INIT
//            });
//            return result;
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }
//        return 0;
//    }
    public Integer deleteCallOutEvent(Long id) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();

        sql.append("DELETE FROM G_FILE COE WHERE COE.ID = ?");
        params.add(id);
        int result = getJdbcTemplate().update(sql.toString(), params.toArray());
        return result;
    }

    private Object[] getStrQuery(int type, CallOutEventQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        switch (type) {
            case 1:
                //count
                sql.append(" SELECT COUNT(*) COUNT FROM (");
                break;
            case 0:
                //select *
                sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
                break;
            default:
                return null;
        }

        sql.append("SELECT "
                + " COE.ID, "
                + " COE.AGENT_ID, "
                + " A.AGENT_NAME, "
                + " COE.USER_CREATED_ID, "
                + " U.USER_NAME, "
                + " COE.CREATED_DATE, "
                + " COE.STATUS "
                + " FROM G_FILE COE"
                + " JOIN D_AGENTS A ON COE.AGENT_ID = A.AGENT_ID"
                + " JOIN D_USERS U ON COE.USER_CREATED_ID = U.USER_ID ");
        sql.append(" WHERE 1=1 ");
        if (query.getAgentId() != null) {
            sql.append(" AND COE.AGENT_ID = ?");
            params.add(query.getAgentId());
        }
        if (query.getStatus() != null && query.getStatus() != "") {
            sql.append(" AND COE.STATUS = ?");
            params.add(query.getStatus());
        }
        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
        return new Object[]{sql, params.toArray()};
    }

    private List<CallOutEventDTO> extractCallOutEvent(SqlRowSet dataRow) {
        List<CallOutEventDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            CallOutEventDTO temp = new CallOutEventDTO();
            temp.setCallOutEventId(dataRow.getLong("ID"));
            temp.setAgentId(dataRow.getLong("AGENT_ID"));
            temp.setAgentName(dataRow.getString("AGENT_NAME"));
            temp.setCreatedDate(dataRow.getDate("CREATED_DATE"));
            temp.setStatus(dataRow.getString("STATUS"));
            temp.setCreatedBy(dataRow.getLong("USER_CREATED_ID"));
            temp.setUserCreatedName(dataRow.getString("USER_NAME"));
            result.add(temp);
        }
        return result;
    }

    public Integer CheckAgentCodeExists(BranchDTO agentInfo) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM D_AGENTS WHERE LOWER(AGENT_CODE) = ?");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{agentInfo.getCode().toLowerCase()});
        } catch (Exception ex) {
        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    public Long getCallOutEventSeqNextval() {
        String queryString = "select g_file_seq.nextval id from dual";
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getLong("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public Long checkEventLastTwoHours(Long branchId) {
        StringBuilder sql = new StringBuilder();
        //sql.append("SELECT COUNT(*) COUNT FROM G_FILE WHERE AGENT_ID = ? AND CREATED_DATE > SYSDATE - (2/24)");
        sql.append("SELECT COUNT(*) COUNT FROM G_FILE WHERE AGENT_ID = ? AND CREATED_DATE > SYSDATE - (1/24/60)");
//        sql.append("SELECT COUNT(*) COUNT FROM G_FILE WHERE AGENT_ID = ? AND CREATED_DATE > SYSDATE - ").append(Config.getInstance().getTimeCheckMakeCall());
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{branchId});
        } catch (Exception ex) {
        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getLong("COUNT");
        }
        return 1l;
    }
}
