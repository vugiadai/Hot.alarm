/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dao;

import com.cms.xsonline.base.BaseDAO;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.AccountQueryDTO;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FAUpdateRequest;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.Utilities;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author pxduong
 */
@Repository
public class AccountDAO extends BaseDAO {

    public Integer getCount(AccountQueryDTO query) {
        Object[] strQuery = getStrQuery(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("COUNT");
            }
        }
        return 0;
    }

    public List<AccountDTO> getByUserName(String userName) {
        List<AccountDTO> result = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT ")
                .append(" * ")
                .append(" FROM D_USERS U"
                        + " LEFT JOIN D_USER_ROLE UR ON U.USER_ID = UR.USER_ID"
                        + " LEFT JOIN D_ROLES R ON UR.ROLE_ID = R.ROLE_ID "
                        + " LEFT JOIN D_AGENTS A ON A.AGENT_ID = U.AGENT_ID "
                        + " WHERE U.IS_ACTIVE = 1"
                        + " AND UR.IS_ACTIVE = 1 "
                        + " AND LOWER(U.USER_NAME) = ?");
        Object[] parameterList = {userName.toLowerCase()};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), parameterList);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = getAccountFromRow(dataRow);
        return result;
    }

    public List<AccountDTO> getManagersByAgent(Long agentId) {
        List<AccountDTO> result = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();
        queryString.append(" SELECT ")
                .append(" * ")
                .append(" FROM D_USERS U"
                        + " LEFT JOIN D_USER_ROLE UR ON U.USER_ID = UR.USER_ID"
                        + " LEFT JOIN D_ROLES R ON UR.ROLE_ID = R.ROLE_ID "
                        + " LEFT JOIN D_AGENTS A ON A.AGENT_ID = U.AGENT_ID "
                        + " WHERE U.IS_ACTIVE = 1"
                        + " AND UR.IS_ACTIVE = 1 "
                        + " AND R.ROLE_ID = 2 "
                        + " AND U.AGENT_ID = ?");
        Object[] parameterList = {agentId};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), parameterList);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = getAccountFromRow(dataRow);
        return result;
    }

    public List<AccountDTO> getByAgentId(Long agentId) {
        List<AccountDTO> result = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();

        queryString.append("select u.* from D_users u "
                + "join d_contact c on u.USER_ID = c.USER_ID "
                + "where c.AGENT_ID = ?");
        Object[] parameterList = {agentId};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), parameterList);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = extractAccountOrg(dataRow);
        return result;
    }

    public List<AccountDTO> getByContactId(Long contactId) {
        List<AccountDTO> result = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();

        queryString.append("select u.* from D_users u "
                + "join d_contact c on u.USER_ID = c.USER_ID "
                + "where c.contact_id = ?");
        Object[] parameterList = {contactId};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), parameterList);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = extractAccountOrg(dataRow);
        return result;
    }

    public AccountDTO getById(Long userId) {
        List<AccountDTO> result = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT ")
                .append(" * ")
                .append(" FROM D_USERS U"
                        + " LEFT JOIN D_USER_ROLE UR ON U.USER_ID = UR.USER_ID"
                        + " LEFT JOIN D_ROLES R ON UR.ROLE_ID = R.ROLE_ID "
                        + " LEFT JOIN D_AGENTS A ON  U.AGENT_ID = A.AGENT_ID"
                        + " WHERE  "
                        + " UR.IS_ACTIVE = 1 "
                        + " AND U.USER_ID = ?");
        Object[] parameterList = {userId};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), parameterList);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = getAccountFromRow(dataRow);
        return result.get(0);
    }

    public List<AccountDTO> getByPage(AccountQueryDTO query) {
        List<AccountDTO> result = new ArrayList<>();
        Object[] strQuery = getStrQuery(0, query);
        if (strQuery != null) {
            String sql = strQuery[0].toString();
            Object[] parameterList = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(sql, parameterList);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = getAccountFromRow(dataRow);
        }
        return result;
    }

    public Long createAccount(AccountDTO account) {///return userId
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("BEGIN INSERT INTO D_USERS"
                    + "(USER_ID, USER_NAME, FIRST_NAME, LAST_NAME, PASSWORD, IS_ACTIVE, "
                    + "EMAIL, UPDATED_DATE, PHONE_NUMBER, CREATED_DATE, ADDRESS, AGENT_ID, SALT) "
                    + "VALUES (D_USERS_SEQ.nextval, ?, ?, ?, ?, ?, ?, "
                    + "TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), "
                    + "?, TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'),?,?, ?) returning  USER_ID into ? ;END;");
            Long result = insertWithIdReturn(queryString.toString(), new Object[]{
                account.getUsername().toLowerCase(),
                account.getFirstName(),
                account.getLastName(),
                account.getPassword(),
                1,
                account.getEmail(),
                Utilities.getDateStr(new Date()),
                account.getPhone(),
                Utilities.getDateStr(new Date()),
                account.getAddress(),
                account.getAgentId(),
                account.getSalt()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0L;
    }

    public Long getUserSeqNextval() {
        String queryString = "select D_USERS_SEQ.nextval id from dual";
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getLong("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public Long createAccount(AccountDTO account, Long id) {///return userId
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("BEGIN INSERT INTO D_USERS"
                    + "(USER_ID, USER_NAME, FIRST_NAME, LAST_NAME, PASSWORD, IS_ACTIVE, "
                    + "EMAIL, UPDATED_DATE, PHONE_NUMBER, CREATED_DATE, ADDRESS, AGENT_ID, SALT) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, "
                    + "TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), "
                    + "?, TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'),?,?, ?) returning  USER_ID into ? ;END;");
            Long result = insertWithIdReturn(queryString.toString(), new Object[]{
                id,
                account.getUsername().toLowerCase(),
                account.getFirstName(),
                account.getLastName(),
                account.getPassword(),
                1,
                account.getEmail(),
                Utilities.getDateStr(new Date()),
                account.getPhone(),
                Utilities.getDateStr(new Date()),
                account.getAddress(),
                account.getAgentId(),
                account.getSalt()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0L;
    }

    public int updateAccount(AccountDTO account) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE D_USERS SET UPDATED_DATE = TO_DATE(?,'" + Constant.sqlDateTimeFormat + "') ");
            params.add(Utilities.getDateStr(new Date()));
            queryString.append(", FIRST_NAME = ?");
            params.add(account.getFirstName() == null ? "" : account.getFirstName());
            queryString.append(", LAST_NAME = ?");
            params.add(account.getLastName() == null ? "" : account.getLastName());
            queryString.append(", EMAIL = ?");
            params.add(account.getEmail() == null ? "" : account.getEmail());
            queryString.append(", PHONE_NUMBER = ?");
            params.add(account.getPhone() == null ? "" : account.getPhone());
            queryString.append(", ADDRESS = ?");
            params.add(account.getAddress() == null ? "" : account.getAddress());
            if (account.getPassword() != null && !"".equals(account.getPassword())
                    && account.getSalt() != null && !"".equals(account.getSalt())) {
                queryString.append(", PASSWORD = ?");
                params.add(account.getPassword());
                queryString.append(", SALT = ?");
                params.add(account.getSalt());
            }
            queryString.append(" WHERE USER_ID = ?");
            params.add(account.getId());
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int deleteAccount(Long id) {
        try {

            int result = 0;
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            //set user is_active = 0
            queryString.append("UPDATE D_USERS SET IS_ACTIVE = 0, UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') WHERE USER_ID = ?");
            params.add(Utilities.getDateStr(new Date()));
            params.add(id);
            result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            if (result > 0) {
                //reset value
                result = 0;
                queryString = new StringBuilder();
                params = new ArrayList<>();
                //set user_role is_active = 0
                queryString.append("DELETE D_USER_ROLE WHERE USER_ID = ?");
                params.add(id);
                result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            }
            //check if result <= 0 thi rollback.
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    //ham nay phuc vu active/deactive user cua cu dan
    public int changeActiveStatus(Long id, int status) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            //set user is_active = 0
            queryString.append("UPDATE D_USERS SET IS_ACTIVE = ?, UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') WHERE USER_ID = ?");
            params.add(status);
            params.add(Utilities.getDateStr(new Date()));
            params.add(id);
            return getJdbcTemplate().update(queryString.toString(), params.toArray());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int lockAccount(Long id) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE D_USERS SET IS_LOCK = 1, UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') WHERE USER_ID = ?");
            params.add(Utilities.getDateStr(new Date()));
            params.add(id);
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int unlockAccount(Long id) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE D_USERS SET IS_LOCK = 0, UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') WHERE USER_ID = ?");
            params.add(Utilities.getDateStr(new Date()));
            params.add(id);
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    private List<AccountDTO> getAccountFromRow(SqlRowSet dataRow) {
        List<AccountDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            AccountDTO temp = new AccountDTO();
            temp.setId(dataRow.getLong("USER_ID"));
            temp.setUsername(dataRow.getString("USER_NAME"));
            temp.setPhone(dataRow.getString("PHONE_NUMBER"));
            temp.setEmail(dataRow.getString("EMAIL"));
            temp.setEnable(dataRow.getInt("IS_LOCK") != 1);
            temp.setCreationTime(Utilities.getDateStr(dataRow.getDate("CREATED_DATE")));
            temp.setModificationTime(Utilities.getDateStr(dataRow.getDate("UPDATED_DATE")));
            temp.setFirstName(dataRow.getString("FIRST_NAME"));
            temp.setLastName(dataRow.getString("LAST_NAME"));
            temp.setPassword(dataRow.getString("PASSWORD"));
            temp.setAgentId(dataRow.getLong("AGENT_ID"));
            temp.setAgentCode(dataRow.getString("AGENT_CODE"));
            temp.setAgentName(dataRow.getString("AGENT_NAME"));
            temp.setAddress(dataRow.getString("ADDRESS"));
            temp.setIsLocked(dataRow.getLong("IS_LOCK") == 1);
            temp.setRoleName(dataRow.getString("ROLE_NAME"));
            temp.setRole(dataRow.getString("ROLE_ID"));
            temp.setSalt(dataRow.getString("SALT") == null ? "" : dataRow.getString("SALT"));
            result.add(temp);
        }
        return result;
    }

    private List<AccountDTO> extractAccountOrg(SqlRowSet dataRow) {
        List<AccountDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            AccountDTO temp = new AccountDTO();
            temp.setId(dataRow.getLong("USER_ID"));
            temp.setUsername(dataRow.getString("USER_NAME"));
            temp.setFirstName(dataRow.getString("FIRST_NAME"));
            temp.setLastName(dataRow.getString("LAST_NAME"));
            temp.setPhone(dataRow.getString("PHONE_NUMBER"));
            temp.setEmail(dataRow.getString("EMAIL"));
            temp.setCreationTime(Utilities.getDateStr(dataRow.getDate("CREATED_DATE")));
            temp.setModificationTime(Utilities.getDateStr(dataRow.getDate("UPDATED_DATE")));
            temp.setAgentId(dataRow.getLong("AGENT_ID"));
            temp.setAgentCode(dataRow.getString("AGENT_CODE"));
            temp.setAddress(dataRow.getString("ADDRESS"));
            result.add(temp);
        }
        return result;
    }

    private Object[] getStrQuery(int type, AccountQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (type == 1) {//count
            sql.append(" SELECT COUNT(*) COUNT FROM ( ");
        } else if (type == 0) {//select *
            sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
        } else {
            return null;
        }
        sql.append("SELECT * FROM D_USERS U"
                + " LEFT JOIN D_USER_ROLE UR ON U.USER_ID = UR.USER_ID"
                + " LEFT JOIN D_ROLES R ON UR.ROLE_ID = R.ROLE_ID "
                + " LEFT JOIN D_AGENTS A ON U.AGENT_ID = A.AGENT_ID"
                + " WHERE  1=1");
        sql.append(" AND U.IS_ACTIVE = 1 ");
        sql.append(" AND UR.IS_ACTIVE = 1 ");
        if (query.getUsername() != null && !"".equals(query.getUsername())) {
            sql.append(" AND LOWER(U.USER_NAME) like ?");
            params.add("%" + query.getUsername().toLowerCase() + "%");
        }
        if (query.getPhoneNumber() != null && !"".equals(query.getPhoneNumber())) {
            sql.append(" AND U.PHONE_NUMBER like ?");
            params.add("%" + query.getPhoneNumber() + "%");
        }
        if (query.getAgentId() != null) {
            sql.append(" AND A.AGENT_ID = ?");
            params.add(query.getAgentId());
        }
        if (query.getAgentCode() != null && !"".equals(query.getAgentCode())) {
            sql.append(" AND LOWER(A.AGENT_CODE) like ?");
            params.add("%" + query.getAgentCode().toLowerCase() + "%");
        }
        if (query.getAgentName() != null && !"".equals(query.getAgentName())) {
            sql.append(" AND LOWER(A.AGENT_NAME) like ?");
            params.add("%" + query.getAgentName().toLowerCase() + "%");
        }
        if (query.getFromDate() != null) {
            sql.append(" AND U.CREATED_DATE >= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getFromDate()));
        }
        if (query.getToDate() != null) {
            sql.append(" AND U.CREATED_DATE <= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getToDate()));
        }
        sql.append(" ORDER BY U.CREATED_DATE DESC ");
        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
        return new Object[]{sql, params.toArray()};
    }

    public int changePass(AccountDTO accountInfo, String newPass, String salt) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE D_USERS SET PASSWORD = ?, SALT = ? WHERE USER_ID = ?");
        return getJdbcTemplate().update(sql.toString(), new Object[]{newPass, salt, accountInfo.getId()});
    }

    public Integer CheckAccountExists(AccountDTO accountInfo) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM D_USERS WHERE IS_ACTIVE = 1 AND "
                + "(LOWER(USER_NAME) = ? "
                + "OR LOWER(AGENT_CODE) = ?"
                + "OR LOWER(PHONE_NUMBER) = ?)");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{
                accountInfo.getUsername().toLowerCase(),
                accountInfo.getAgentCode().toLowerCase(),
                accountInfo.getPhone().toLowerCase()});

        } catch (Exception ex) {

        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    public Integer CheckAgentPhoneNumber(String phoneNumber) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM AGENTS WHERE AGENT_ID = ? ");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{
                phoneNumber});

        } catch (Exception ex) {
//do nothing
        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    public Integer CheckAgentUserNumber(String userName) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM AGENTS WHERE vsa_user_login = ? ");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{
                userName});

        } catch (Exception ex) {
//do nothing
        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    //agentId tuong tuong so dien thoai. queueId tuong duong ma toa nha
    public Integer checkQueueAgentExists(String agentId, String queueId) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM QUEUE_AGENT WHERE AGENT_ID = ? AND QUEUE_ID = ?");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{
                agentId,
                queueId});

        } catch (Exception ex) {
//do nothing
        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    //agentId tuong tuong so dien thoai. queueId tuong duong ma toa nha. phone tuong duong agentId moi
    public int updateQueueAgentPhone(String agentId, String queueId, String phone) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE QUEUE_AGENT SET AGENT_ID = ? WHERE AGENT_ID = ? AND QUEUE_ID = ?");
            params.add(phone);
            params.add(agentId);
            params.add(queueId);
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int updateAgentByUsername(AccountDTO accountInfo) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE AGENTS SET AGENT_ID = ? WHERE vsa_user_login = ?");
            params.add(accountInfo.getAgentId());
            params.add(accountInfo.getUsername());
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int updateAgentByPhone(AccountDTO accountInfo) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE AGENTS SET vsa_user_login = ? WHERE agent_id = ?");
            params.add(accountInfo.getUsername());
            params.add(accountInfo.getPhone());
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int changeAgentUserStatus(String phone, String status) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE AGENTS SET user_status = ? WHERE agent_id = ?");
            params.add(status);
            params.add(phone);
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int createAgent(AccountDTO accountInfo) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("insert into agents (agent_id,system_status,user_status,"
                    + "last_start_work,last_finish_work,vsa_user_login,num_rejectcall,"
                    + "callout_id,email_user_status,chat_user_status,happycall_user_status,"
                    + "misscall_user_status,voicemail_user_status,campaign_user_status,"
                    + "multi_channel_user_status,max_transaction_email,max_transaction_chat,"
                    + "max_current_transaction,total_transaction,chat_system_status,"
                    + "email_system_status,TOTAL_ANSWER_CHAT,TOTAL_ANSWER_TIME_CHAT,"
                    + "TOTAL_ANSWER_EMAIL,TOTAL_ANSWER_TIME_EMAIL)"
                    + "values"
                    + "(?,'AVAILABLE','LOGOUT',sysdate,sysdate,?,0,102,"
                    + "'NOT AVAILABLE','NOT AVAILABLE','NOT AVAILABLE','NOT AVAILABLE',"
                    + "'NOT AVAILABLE','NOT AVAILABLE','LOGOUT',1,1,3,3,0,0,0,0,0,0)");
            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                accountInfo.getPhone(),
                accountInfo.getUsername()
            });
            return result;
        } catch (Exception ex) {
            return 0;
        }
    }

    public int createQueueAgent(AccountDTO accountInfo) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO ");
            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                accountInfo.getPhone(),
                accountInfo.getUsername()
            });
            return result;
        } catch (Exception ex) {
            return 0;
        }
    }

    public Integer CheckAccountSuperAgentExists() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM D_USERS U INNER JOIN D_USER_ROLE UR ON U.USER_ID = UR.USER_ID WHERE "
                + "UR.ROLE_ID = ? AND U.IS_ACTIVE = 1 AND UR.IS_ACTIVE = 1 ");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{Constant.ROLE_SUPER_AGENT_ID});

        } catch (Exception ex) {

        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    public int UpdateSuperAgentDiscount(Long accountId, Float discount) {///return userId
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("UPDATE D_AGENTS SET DISCOUNT = ?, UPDATE_BY = ?, UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("')");
            int rs = getJdbcTemplate().update(queryString.toString(), new Object[]{
                discount,
                accountId,
                Utilities.getDateStr(new Date())
            });
            return rs;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return 0;
    }

    public int settingNotification(int userId, boolean enableOrNot) throws FAException {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE D_USERS SET IS_ACTIVE = ? WHERE USER_ID = ?");
            return getJdbcTemplate().update(sql.toString(), new Object[]{enableOrNot ? "1" : "0", userId});
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    public int updateAccountDTO(int userId, FAUpdateRequest request) throws FAException {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE D_USERS SET FIRST_NAME = ?, LAST_NAME = ?, PHONE_NUMBER = ?, EMAIL = ?  WHERE USER_ID = ?");
            return getJdbcTemplate().update(sql.toString(),
                    new Object[]{request.firstName, request.lastName, request.phone, request.email, userId});
        } catch (Exception ex) {
            throw ex;
        }
    }

}
