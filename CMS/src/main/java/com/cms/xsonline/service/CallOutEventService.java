/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.base.BaseService;
import com.cms.xsonline.dto.CallOutEventDTO;
import com.cms.xsonline.dto.CallOutEventQueryDTO;
import java.util.List;

/**
 *
 * @author duongpx2
 */
public interface CallOutEventService extends BaseService<CallOutEventDTO, CallOutEventQueryDTO> {

    public Integer deleteCallOutEvent(Long id);

    public List<CallOutEventDTO> getById(Long id);

    public Integer addNewCallOutEvent(CallOutEventDTO event);
}
