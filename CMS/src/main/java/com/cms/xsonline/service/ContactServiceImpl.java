/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.dao.ContactDAO;
import com.cms.xsonline.dto.PlayerDTO;
import com.cms.xsonline.dto.PlayerQueryDTO;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author Duongpx2
 */
@Service("playerService")
public class ContactServiceImpl implements ContactService {

    @Resource
    private ContactDAO playerDAO;

    @Override
    public Integer getCount(PlayerQueryDTO query) {
        return this.playerDAO.getCount(query);
    }

    @Override
    public List<PlayerDTO> getByPage(PlayerQueryDTO query) {
        List<PlayerDTO> result = this.playerDAO.getByPage(query);
        return result;
    }

}
