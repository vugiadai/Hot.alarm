/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.dao.AccountDAO;
import com.cms.xsonline.dao.BranchDAO;
import com.cms.xsonline.dao.ContactDAO;
import com.cms.xsonline.dao.RoleDAO;
import com.cms.xsonline.dto.*;
import com.cms.xsonline.obj.BaseResult;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FAUpdateRequest;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.MD5Util;
import com.cms.xsonline.util.Utilities;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author pxduong
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountDAO accountDAO;
    @Resource
    private RoleDAO roleDAO;
    @Resource
    private BranchDAO branchDAO;

    @Autowired
    private ContactDAO contactDAO;

    private BranchService branchService;

    @Autowired(required = true)
    @Qualifier(value = "branchService")
    public void setBranchService(BranchService bs) {
        this.branchService = bs;
    }

    @Override
    public Integer getCount(AccountQueryDTO query) {
        return this.accountDAO.getCount(query);
    }

    @Override
    public List<AccountDTO> getByPage(AccountQueryDTO query) {
        return this.accountDAO.getByPage(query);
    }

    @Override
    public BaseResult createAccount(AccountDTO account) {
        BaseResult result = new BaseResult();
        result.setIsFailed(true);
        result.setErrorCode(Constant.ErrorCode.UNKNOWN);
        result.setErrorMessage(Constant.ErrorMessage.UNKNOWN);
        if (validateAccount(account)) {
            account.setPhone(Utilities.preparePhoneNums(account.getPhone()));
            if (checkAccountExist(account)) {
                result.setIsFailed(true);
                result.setErrorCode(Constant.ErrorCode.DUPLICATE_DATA);
                result.setErrorMessage("Account exists. Try another...");
            } else {
                String newPassword = account.getPassword();
                String userSalt = Utilities.randomString(16);
                String MD5Pass = MD5Util.getSecurePassword(newPassword.concat(userSalt));
                account.setPassword(MD5Pass);
                account.setSalt(userSalt);
                Long userIdCreated = 0l;
                if (account.getId() != null) {
                    userIdCreated = this.accountDAO.createAccount(account, account.getId());
                } else {
                    userIdCreated = this.accountDAO.createAccount(account);
                }
                if (userIdCreated != null && userIdCreated > 0) {
                    //insert to user_role table
                    int insertUserRoleResult = this.roleDAO.insertUserRole(userIdCreated, Long.valueOf(account.getRole()));
                    //insert agent
                    int insertAgentResult = 0;
                    int insertQueueAgentResult = 0;
                    //check khi user la quan ly toa nha moi them
                    if ("2".equals(account.getRole())) {

                        if (this.accountDAO.CheckAgentPhoneNumber(account.getPhone()) <= 0) {
                            //check neu chua co agent nao co agent_id trung so dien thoai thi them
                            insertAgentResult = this.accountDAO.createAgent(account);
                        } else {
                            //neu da ton tai sdt --> cap nhat username cua agent = username
                            insertAgentResult = this.accountDAO.updateAgentByPhone(account);
                        }
                        ///queue_agent
                        //neu chua co thi moi them, co san roi thi khong can
                        if (this.accountDAO.checkQueueAgentExists(account.getPhone(), account.getAgentId().toString()) <= 0) {
                            //insert bang queue_agent
                            QueueAgentDTO queueAgent = new QueueAgentDTO();
                            queueAgent.setAgentId(Long.valueOf(account.getPhone()));
                            queueAgent.setQueueId(account.getAgentId());
                            queueAgent.setLevelPriority(1l);
                            queueAgent.setPriority(1l);
                            queueAgent.setSkillLevel(1l);
                            insertQueueAgentResult = this.branchDAO.createQueuesAgent(queueAgent);
                        } else {
                            insertQueueAgentResult = 1;
                        }
                    } else {
                        insertAgentResult = 1;
                        insertQueueAgentResult = 1;
                    }
                    //end insert agent

                    if (insertUserRoleResult > 0 && insertAgentResult > 0 && insertQueueAgentResult > 0) {
                        result.setIsFailed(false);
                        result.setErrorCode(Constant.ErrorCode.SUCCESS);
                        result.setErrorMessage("Create account success!");
                    } else {
                        //rollback, set account isactive = 0
                        this.accountDAO.deleteAccount(userIdCreated);
                    }
                }
            }
        } else {
            result.setIsFailed(true);
            result.setErrorCode(Constant.ErrorCode.INVALIDATE_DATA);
            result.setErrorMessage(Constant.ErrorMessage.INVALIDATE_DATA);
        }
        return result;//that bai
    }

    private boolean validateAccount(AccountDTO account) {
        if (account.getUsername() == null || "".equals(account.getUsername())) {
            return false;
        }
        if (account.getPhone() == null || "".equals(account.getPhone())) {
            return false;
        }
        if (account.getRole() == null || "".equals(account.getRole())) {
            return false;
        }
        if (account.getRole().equals(Constant.ROLE_AGENT_ID)) {
            if (account.getAgentCode() == null || "".equals(account.getAgentCode())) {
                return false;
            }
        }
        return !(account.getPassword() == null || account.getRePassword() == null
                || "".equals(account.getPassword()) || "".equals(account.getRePassword()));
    }

    @Override
    public List<AccountDTO> getByUserName(String userName) {
        return this.accountDAO.getByUserName(userName);
    }

    @Override
    public List<RoleDTO> getRoleByUserId(Long userId) {
        return this.roleDAO.getRoleByUserId(userId);
    }

    @Override
    public List<BranchDTO> getAllBranch() {
        return this.branchDAO.getAllBranch();
    }

    @Override
    public AccountDTO getById(Long id) {
        return this.accountDAO.getById(id);
    }

    @Override
    public List<RoleDTO> getAllRole() {
        return this.roleDAO.getAllRoles();
    }

    @Override
    public Integer deleteAccount(Long id) {
        AccountDTO accountInfo = this.accountDAO.getById(id);
        if ("3".equals(accountInfo.getRole())) {
            //neu user là cư dan thi deactive sdt
            //lay thong tin contact
            ContactDTO contact = this.branchService.getContactByUserId(id);
//            return this.branchService.changeContactStatus(id, "N");
            if (contact != null) {
                //xoa luon contact
                if (this.branchDAO.deleteContact(contact.getContactId()) > 0) {
                    return this.accountDAO.changeActiveStatus(id, 0);
                }
            } else {
                return this.accountDAO.changeActiveStatus(id, 0);
            }
        } else if ("2".equals(accountInfo.getRole())) {
            //thay doi trang thai agent
            this.accountDAO.changeAgentUserStatus(accountInfo.getPhone(), "NOT AVAILABLE");
            //xoa trong queue agent
            this.branchDAO.deleteQueueAgent(accountInfo.getPhone(), accountInfo.getAgentId());
        }
        return this.accountDAO.deleteAccount(id);
    }

    @Override
    public Integer changeActiveStatus(AccountDTO account, int status
    ) {
        int changeAccountStatus = this.accountDAO.changeActiveStatus(account.getId(), status);
        if (changeAccountStatus > 1) {
//            thay doi trang thai trong bang agents
            if ("2".equals(account.getRole())) {
                this.accountDAO.changeAgentUserStatus(account.getPhone(), status == 0 ? "LOGOUT" : "NOT AVAILABLE");
            }
        }
        return changeAccountStatus;
    }

    @Override
    public BaseResult editAccount(AccountDTO account
    ) {
        BaseResult result = new BaseResult();

        if (account.isIsAdminChangePass()) {
            if (account.getPassword() == null || account.getRePassword() == null
                    || "".equals(account.getPassword()) || "".equals(account.getRePassword())) {
                result.setIsFailed(true);
                result.setErrorCode(Constant.ErrorCode.INVALIDATE_DATA);
                result.setErrorMessage("Password must not be empty. Please try again ...");
                return result;
            }
            String newPassword = account.getPassword();
            String userSalt = Utilities.randomString(Constant.USER_SALT_LENGTH);
            String MD5Pass = MD5Util.getSecurePassword(newPassword.concat(userSalt));
            account.setPassword(MD5Pass);
            account.setSalt(userSalt);
        }
        //edit phone number
        account.setPhone(Utilities.preparePhoneNums(account.getPhone()));
        int insertAgentResult = 0;
        int insertQueueAgentResult = 0;
        AccountDTO oldAccount = this.accountDAO.getById(account.getId());
        if (this.accountDAO.updateAccount(account) > 0) {
            //check khi user la quan ly toa nha moi them
            if ("2".equals(oldAccount.getRole())) {
                //insert agent
                if (this.accountDAO.CheckAgentUserNumber(account.getPhone()) <= 0) {
                    //check neu chua co agent nao co agent_id trung so dien thoai thi them
                    insertAgentResult = this.accountDAO.createAgent(account);
                } else {
                    //neu da ton tai sdt --> cap nhat sdt cua agent = sdt moi theo username
                    insertAgentResult = this.accountDAO.updateAgentByUsername(account);
                }
                ///queue_agent
                if (this.accountDAO.checkQueueAgentExists(oldAccount.getPhone(), oldAccount.getAgentId().toString()) > 0) {
                    insertQueueAgentResult = this.accountDAO.updateQueueAgentPhone(oldAccount.getPhone(),
                            oldAccount.getAgentId().toString(), account.getPhone());
                } else {
                    //insert bang queue_agent
                    QueueAgentDTO queueAgent = new QueueAgentDTO();
                    queueAgent.setAgentId(Long.valueOf(account.getPhone()));
                    queueAgent.setQueueId(account.getAgentId());
                    queueAgent.setLevelPriority(1l);
                    queueAgent.setPriority(1l);
                    queueAgent.setSkillLevel(1l);
                    insertQueueAgentResult = this.branchDAO.createQueuesAgent(queueAgent);
                }
            } else if ("3".equals(oldAccount.getRole())) {
                ContactDTO contact = this.branchDAO.getContactByUserId(oldAccount.getId());
                insertQueueAgentResult = 1;
                insertAgentResult = this.branchDAO.updateContactPhone(contact.getContactId(), account.getPhone());
            } else {
                insertAgentResult = 1;
                insertQueueAgentResult = 1;
            }
            if (insertAgentResult > 0 && insertQueueAgentResult > 0) {
                result.setIsFailed(false);
                result.setErrorCode(Constant.ErrorCode.SUCCESS);
                result.setErrorMessage("Edit account info success!");
            } else {
                result.setIsFailed(false);
                result.setErrorCode(Constant.ErrorCode.SUCCESS);
                result.setErrorMessage("Edit account info fail when update agent vsa. Please try again!");
            }
        }
        return result;

    }

    @Override
    public int changePass(AccountDTO accountInfo, ChangePasswordDTO changePass
    ) {
        //check curPass

        if (!Strings.isNullOrEmpty(changePass.getNewPass()) && changePass.getNewPass().equals(changePass.getReNewPass())) {
            String MD5OldPass = MD5Util.getSecurePassword(changePass.getOldPass().concat(Strings.isNullOrEmpty(accountInfo.getSalt()) ? "" : accountInfo.getSalt()));
            if (MD5OldPass.equals(accountInfo.getPassword())) {
                String userSalt = Utilities.randomString(Constant.USER_SALT_LENGTH);
                String MD5NewPass = MD5Util.getSecurePassword(changePass.getNewPass().concat(userSalt));
                return this.accountDAO.changePass(accountInfo, MD5NewPass, userSalt);
            }
        }
        return 0;//failed
    }

    @Override
    public boolean checkAccountExist(AccountDTO accountInfo
    ) {
        if (this.accountDAO.CheckAccountExists(accountInfo) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Integer unlockAccount(Long id
    ) {
        return this.accountDAO.unlockAccount(id);
    }

    @Override
    public BaseResult updateSuperAgentDiscount(AccountDTO accountInfo, Float discount
    ) {
        BaseResult result = new BaseResult();
        //check role
        if (!accountInfo.getRoleName().toLowerCase().contains("admin")) {
            result.setIsFailed(true);
            result.setErrorCode(Constant.ErrorCode.HAS_NO_ACCESS);
            result.setErrorMessage("You do not have permission to perform this action");
            return result;
        }
        //check discount
        if (discount >= 100 || discount <= 0) {
            result.setIsFailed(true);
            result.setErrorCode(Constant.ErrorCode.WRONG_DATA);
            result.setErrorMessage("Discount must less than 100% and greater than 0%");
            return result;
        }

        //update
        if (this.accountDAO.UpdateSuperAgentDiscount(accountInfo.getId(), discount) > 0) {
            result.setIsFailed(false);
            result.setErrorCode(Constant.ErrorCode.SUCCESS);
            result.setErrorMessage("Update super agent discount success");
        } else {
            result.setIsFailed(true);
            result.setErrorCode(Constant.ErrorCode.UNKNOWN);
            result.setErrorMessage("Update super agent discount failed");
        }
        return result;
    }

    @Override
    public int activeNotification(int userId, boolean enableOrNot) throws FAException {
        try {
            return contactDAO.settingNotification(userId, enableOrNot);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int updateProfile(int userId, FAUpdateRequest request) throws FAException {
        try {
            return accountDAO.updateAccountDTO(userId, request);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<AccountDTO> getManagerByAgent(Long agentId) {
        return this.accountDAO.getManagersByAgent(agentId);
    }
}
