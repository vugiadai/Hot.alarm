/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.base.BaseService;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.LotteryQueryDTO;
import com.cms.xsonline.dto.LotteryResultDTO;
import com.cms.xsonline.dto.PlayerDTO;
import java.util.List;

/**
 *
 * @author pxduong
 */
public interface HomeService extends BaseService<LotteryResultDTO, LotteryQueryDTO> {

    public Integer insertPokeCall(PlayerDTO result);

    public void saveBatch(final List<PlayerDTO> playerDTOs, Long insertedId);

    public Integer updateResult(AccountDTO accountInfo, LotteryResultDTO result);

    public Integer commitResult(AccountDTO accountInfo);

    public Integer updateTotalRow(int totalRow, long id);

    public boolean checkEnableUpdate(LotteryResultDTO result);

    public boolean checkEnableCommit(LotteryResultDTO result);
}
