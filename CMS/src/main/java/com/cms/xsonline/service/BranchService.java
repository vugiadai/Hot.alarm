/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.base.BaseService;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.BranchDetailQueryDTO;
import com.cms.xsonline.dto.BranchQueryDTO;
import com.cms.xsonline.dto.CallInfoDTO;
import com.cms.xsonline.dto.CallInfoQueryDTO;
import com.cms.xsonline.dto.ContactDTO;
import java.util.List;

/**
 *
 * @author pxduong
 */
public interface BranchService extends BaseService<BranchDTO, BranchQueryDTO> {

    public Integer deleteBranch(Long id);
    
    public Integer deleteContact(Long id);

    public List<BranchDTO> getById(Long id);

    public Integer addNewBranch(BranchDTO branch);

    public Integer editBranch(BranchDTO branch);

    public boolean checkAgentExist(BranchDTO agentInfo);

    public List<ContactDTO> getBranchDetail(BranchDetailQueryDTO query);

    public Integer getCountBranchDetail(BranchDetailQueryDTO query);

    public Integer addContact(ContactDTO query);

    public Integer changeContactStatus(Long id, String status);

    public void saveContactBatch(Long branchId, List<ContactDTO> listContact);

    public String makeCall(Long branchId, String notifyContent, AccountDTO userinfo);

    public List<CallInfoDTO> getCallInfo(CallInfoQueryDTO query);

    public Integer getCountCallInfo(CallInfoQueryDTO query);
    
    public Long getUserSeqNextVal();
    
    public ContactDTO getContactByUserId(Long userId);
}
