/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.base.BaseService;
import com.cms.xsonline.dto.PlayerDTO;
import com.cms.xsonline.dto.PlayerQueryDTO;

/**
 *
 * @author Duongpx2
 */
public interface ContactService extends BaseService<PlayerDTO, PlayerQueryDTO> {

}
