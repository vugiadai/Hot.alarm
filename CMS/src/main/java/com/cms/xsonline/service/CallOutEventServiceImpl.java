/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.dao.CallOutDAO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.CallOutEventDTO;
import com.cms.xsonline.dto.CallOutEventQueryDTO;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author duongpx2
 */
@Service("callOutEventService")
public class CallOutEventServiceImpl implements CallOutEventService {

    @Resource
    private CallOutDAO callOutDAO;

    @Override
    public List<CallOutEventDTO> getByPage(CallOutEventQueryDTO query) {
        return this.callOutDAO.getCallOutEvents(query);
    }

    @Override
    public Integer getCount(CallOutEventQueryDTO query) {
        return this.callOutDAO.getCount(query);
    }

    @Override
    public Integer deleteCallOutEvent(Long id) {
        return this.callOutDAO.deleteCallOutEvent(id);
    }

    @Override
    public List<CallOutEventDTO> getById(Long id) {
        return this.callOutDAO.getCallOutEventById(id);
    }

    @Override
    public Integer addNewCallOutEvent(CallOutEventDTO event) {
        Integer callPutEventId = this.callOutDAO.makeCallOutEvent(event);
        return callPutEventId;
    }
}
