package com.cms.xsonline.obj.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

public class LoginResponse {

    public String username;

    @JsonProperty(value = "first_name")
    public String firstName;

    @JsonProperty(value = "last_name")
    public String lastName;

    public String email;

    public String phone;

    @JsonProperty(value = "manager_phone")
    public List<ManagerPhoneResponse> managerPhone;

    public String building;

    @JsonProperty(value = "building_address")
    public String buildingAddress;

    @JsonProperty(value = "building_code")
    public String buildingCode;

    @JsonProperty(value = "building_id")
    public long buildingId;

    @JsonProperty(value = "role")
    public Map<String, String> role;

    @JsonProperty(value = "role_admin")
    public int roleAdmin;

    @JsonProperty(value = "agency_phone")
    public String agencyPhone;

    //Can ho
    public String apartment = "";

    @JsonProperty(value = "apartment_id")
    public int apartmentId = -1;

    @JsonProperty(value = "access_token")
    public String accessToken;

    public List<Map<Integer, String>> floors = Lists.newArrayList();
}
