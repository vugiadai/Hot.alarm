/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.obj;

import com.cms.xsonline.util.Constant;

/**
 *
 * @author pcduongit
 */
public class BaseResult {

    private boolean isFailed;
    private String errorCode;
    private String errorMessage;

    public BaseResult() {
        this.isFailed = true;
        this.errorCode = Constant.ErrorCode.UNKNOWN;
        this.errorMessage = Constant.ErrorMessage.UNKNOWN;
    }

    public boolean isIsFailed() {
        return isFailed;
    }

    public void setIsFailed(boolean isFailed) {
        this.isFailed = isFailed;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
