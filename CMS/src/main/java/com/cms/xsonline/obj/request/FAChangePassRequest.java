package com.cms.xsonline.obj.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FAChangePassRequest {

    public String password;

    @JsonProperty(value = "new_password")
    public String newPassword;

    @JsonProperty(value = "re_password")
    public String rePassword;
}
