package com.cms.xsonline.util;

import com.cms.xsonline.obj.exception.FAException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TokenUtils {
    private static String SIGNING_KEY = "WnhjdmJubQ=="; // base64 of `Zxcvbnm`
    private static String SEPARATOR_STRING = "::";
    public static int EXPIRE_TIME_IN_TOKEN_POS = 2; // position of expiry time in splitting of decoded token.
    public static int EXPIRE_TIME = 24*60*60*360; // position of expiry time in splitting of decoded token.

    public static String md5(String in) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return "";
        }

        md.update(in.getBytes());
        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public static boolean isTokenExpired(String token) {
        if (token == null || "".equals(token)) {
            return true;
        }

        String[] splits;
        try {
            splits = parseArray(token);
            if (splits.length < 3) {
                return true;
            }
        } catch (FAException e) {
            return true;
        }

        long expires = Long.valueOf(splits[EXPIRE_TIME_IN_TOKEN_POS]) / 10000; // from ms -> hour
        long now = Calendar.getInstance().getTimeInMillis() / 10000; // from ms -> hour

        return (now > expires);
    }

    public static String createTokenForUser(long userId, String username, int expiresInHour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(cal.getTime());
        cal.add(Calendar.HOUR, expiresInHour);
        SimpleDateFormat frm = new SimpleDateFormat(Constant.DATE_TIME_FORMAT_4_WEB_DISPLAY);
        System.out.println("Date =  " + frm.format(cal.getTime()));
        return secure(userId, username, String.valueOf(cal.getTimeInMillis()));
    }

    public static String secure(long userId, String username, String time) {
        String subject = String.format("%d::%s::%s", userId, userId, time);
        return Jwts.builder().setSubject(subject).signWith(SignatureAlgorithm.HS512, SIGNING_KEY).compact();
    }

    public static int getUserIdFromToken(String token) throws FAException {
        String[] splits = parseArray(token);

        if (splits.length == 0) {
            throw new FAException(Constant.ERROR_CODE_TOKEN_INVALID, "Could not get user identifier from token.");
        }

        try {
            return Integer.parseInt(splits[0]);
        } catch (NumberFormatException e) {
            throw new FAException(Constant.ERROR_CODE_TOKEN_INVALID, "Could not get user identifier from token.");
        }
    }

    public static String[] parseArray(String token) throws FAException {
        String subject = Jwts.parser().setSigningKey(SIGNING_KEY).parseClaimsJws(token).getBody().getSubject();
        if (subject == null && "".equals(subject)) {
            throw new FAException(Constant.ERROR_CODE_TOKEN_INVALID, "Token invalid.");
        }

        return subject.split(SEPARATOR_STRING);
    }
}
