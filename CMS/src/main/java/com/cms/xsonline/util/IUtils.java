/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author duongtb2
 */
public class IUtils {

    private static long jobIndex = 0;

    private static ApplicationContext ctx;
    private static String keySalt = null;
    private static long keyIndex = 0l;
    private static long callIndex = 0L;
    private static final Logger logger = Logger.getLogger(IUtils.class);
    private static final SimpleDateFormat datePattern = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private static final SimpleDateFormat datePattern2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public synchronized static ApplicationContext getApplicationContex() {
        if (ctx == null) {
            ctx = new FileSystemXmlApplicationContext(
                    new String[]{"../etc/applicationContext.xml"});
        }
        return ctx;
    }

    public static synchronized String keyGen() {
        if (keySalt == null) {
            keySalt = "";
            Random ran = new Random();
            for (int i = 0; i < 10; ++i) {
                keySalt += "" + (char) ('a' + ran.nextInt(26));
            }
        }

        return keySalt + "-" + (keyIndex++);
    }

    public static String getSecurePassword(String passwordToHash) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.update(salt.getBytes());
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xFF) + 256, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public static String getSystemDate() {
        try {
            return datePattern.format(new Date());
        } catch (Exception ex) {
            logger.error("error format system date", ex);
            return datePattern.toPattern();
        }
    }

    public static String formatDate2(Date dt) {
        try {
            return datePattern2.format(dt);
        } catch (Exception ex) {
            logger.error("error format system date", ex);
            return null;
        }
    }

    public synchronized static String genCallId(String phoneNumber) {
        String callId = genVoteCode("") + "-" + phoneNumber + "-" + callIndex++;
        return callId;
    }
    
   
    public static synchronized String genVoteCode(String lotoPackage) {
//        String rs = "";
//        Random ran = new Random();
//        for (int i = 0; i < 6; i++) {
//            rs += ran.nextInt(9);
//        }
        String rs = Math.random() + "";
        rs = rs.substring(2, 2 + 10);
        return lotoPackage + rs;
    }

    public static synchronized String genOtp() {
//        String rs = "";
//        Random ran = new Random();
//        for (int i = 0; i < 6; i++) {
//            rs += ran.nextInt(9);
//        }
        String rs = Math.random() + "";
        rs = rs.substring(2, 2 + 6);
        return rs;
    }

    public synchronized static Long genJobID() {

        return jobIndex++;
    }

    public static String evalString(Object val) {
        String rs = "";
        if (val != null) {
            rs = String.valueOf(val).trim();
        }
        return rs;
    }

    public static int evalInt(Object val, int defaultVal) {
        try {
            return Integer.valueOf(evalString(val));
        } catch (Exception ex) {

            //ex.printStackTrace();
        }

        return defaultVal;
    }

    public static Long evalLong(Object val, Long defaultVal) {
        try {
            defaultVal = Long.valueOf(evalString(val));
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return defaultVal;
    }

    public static Double evalDouble(Object val, Double defaultVal) {
        try {
            defaultVal = Double.valueOf(evalString(val));
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return defaultVal;
    }

    public static long convertDateStringToLong(String dateStr, long defaultVal) {

        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFormat.parse(dateStr);
            return date.getTime();
        } catch (ParseException ex) {
            ex.printStackTrace();
            return defaultVal;
        }
    }

    public static Date toDate(String dateString, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.parse(dateString);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String formatDate(Date dt, String pattern) {
        String rs = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(dt);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return rs;
    }

    public static String formatPhoneNumber(String phoneNumber) {
        if (phoneNumber != null) {
            String phone = phoneNumber;
            phone = phone.startsWith("84") ? "0" + phone.substring(2) : phone;
            phone = phone.startsWith("+84") ? "0" + phone.substring(3) : phone;
            phone = phone.startsWith("0") ? phone : "0" + phone;
            return phone;
        }
        return "";
    }

    public static int getDay() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static int getSysHour() {
//        Calendar calendar = Calendar.getInstance();
//        int hh = calendar.get(Calendar.HOUR_OF_DAY);
//        int mm = calendar.get(Calendar.MINUTE);
        Date dt = new Date();
        int hh = dt.getHours();
        int mm = dt.getMinutes();
        String mmstr = mm + "";
        if (mm >= 0 && mm < 10) {
            mmstr = "0" + mm;
        }
        int rs = Integer.valueOf(hh + mmstr);
        return rs;
    }

    public static boolean isNumber(String val) {

        if (val == null) {
            return false;
        }
        val = val.trim();
        int len = val.length();
        if (len == 0) {
            return false;
        }

        if (val.startsWith("+")) {
            val = val.substring(1, val.length());
        }
        for (int i = 0; i < len; ++i) {
            if (!Character.isDigit(val.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("pass -> " + getSecurePassword("123456" + 123456));
//        for (int i = 0; i < 100; i++) {
//            HashSet set = new HashSet();
//            for (int j = 0; j < 1000000; j++) {
//                set.add(genVoteCode());
//            }
//            System.out.println(set.size());
//        }
        System.out.println(genVoteCode("LT2"));
        System.out.println(getSysHour());
        System.out.println(getSecurePassword("123456"));
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        System.out.println("day of week" + dayOfWeek);
    }
}
