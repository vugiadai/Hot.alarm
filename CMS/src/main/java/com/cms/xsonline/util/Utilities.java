/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.util;

import com.cms.xsonline.config.Config;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author pxduong
 */
public class Utilities {

    private static Long request = 1L;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat shortDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat shortDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    public static String getDateStr(Date date) {
        String result = "";
        try {
            result = dateFormat.format(date);
        } catch (Exception ex) {
        }
        return result;
    }

    public static Date getDateFromStr(String dateStr) {
        Date result = null;
        try {
            result = dateFormat.parse(dateStr);
        } catch (Exception ex) {
        }
        return result;
    }

    public static Date getDateFromStrShort(String dateStr) {
        Date result = null;
        try {
            result = shortDateFormat.parse(dateStr);
        } catch (Exception ex) {
        }
        return result;
    }

    public static String getShortDateStrJS(Date date) {
        String result = "";
        try {
            result = shortDateFormat2.format(date);
        } catch (Exception ex) {
        }
        return result;
    }

    public static String getShortDateStr(Date date) {
        String result = "";
        try {
            result = shortDateFormat.format(date);
        } catch (Exception ex) {
        }
        return result;
    }

    public static String getTimeStr(Date date) {
        String result = "";
        try {
            result = timeFormat.format(date);
        } catch (Exception ex) {
        }
        return result;
    }

    public static Date getEndOfDay(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            return cal.getTime();
        }
        return null;

    }

    public static Long getNextReq() {
        if (request > Long.MAX_VALUE - 1) {
            request = 1L;
        }
        return request++;
    }

    public static Integer genRandom6() {
        Random rand = new Random();
        return rand.nextInt(899999) + 100000;
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    public static String preparePhoneNums(String oldPhone) {

        if (oldPhone != null && !"".equals(oldPhone)) {
            if (oldPhone.startsWith(Constant.AREA_CODE)) {
                return oldPhone.replaceFirst(Constant.AREA_CODE, "");
            }
            if (oldPhone.startsWith("0")) {
                return oldPhone.replaceFirst("0", "");
            }
            return oldPhone;
        }
        return "";
    }
}
