/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

/**
 *
 * @author pxduongit
 */
public class QueueAgentDTO {
    private Long queueId;
    private Long agentId;
    private Long priority;
    private Long skillLevel;
    private Long levelPriority;

    public Long getQueueId() {
        return queueId;
    }

    public void setQueueId(Long queueId) {
        this.queueId = queueId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Long getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(Long skillLevel) {
        this.skillLevel = skillLevel;
    }

    public Long getLevelPriority() {
        return levelPriority;
    }

    public void setLevelPriority(Long levelPriority) {
        this.levelPriority = levelPriority;
    }
    
    
}
