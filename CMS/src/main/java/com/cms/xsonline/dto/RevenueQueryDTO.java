/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseQueryDTO;

/**
 *
 * @author pxduong
 */
public class RevenueQueryDTO extends BaseQueryDTO {

    private String packageCode;

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

}
