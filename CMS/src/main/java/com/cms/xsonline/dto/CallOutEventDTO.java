/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;
import java.util.Date;

/**
 *
 * @author duongpx2
 */
public class CallOutEventDTO extends BaseDTO {

    private Long callOutEventId;
    private Long agentId;
    private String agentName;
    private Long createdBy;
    private String userCreatedName;
    private Date createdDate;
    private String status;
    private Long totalRow;
    private Long totalCommitRow;
    private Long updateBy;
    private Date updateDate;
    private Date doneDate;
    private String notifyContent;

    public Long getCallOutEventId() {
        return callOutEventId;
    }

    public void setCallOutEventId(Long callOutEventId) {
        this.callOutEventId = callOutEventId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getUserCreatedName() {
        return userCreatedName;
    }

    public void setUserCreatedName(String userCreatedName) {
        this.userCreatedName = userCreatedName;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(Long totalRow) {
        this.totalRow = totalRow;
    }

    public Long getTotalCommitRow() {
        return totalCommitRow;
    }

    public void setTotalCommitRow(Long totalCommitRow) {
        this.totalCommitRow = totalCommitRow;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getDoneDate() {
        return doneDate;
    }

    public void setDoneDate(Date doneDate) {
        this.doneDate = doneDate;
    }

    public String getNotifyContent() {
        return notifyContent;
    }

    public void setNotifyContent(String notifyContent) {
        this.notifyContent = notifyContent;
    }

}
