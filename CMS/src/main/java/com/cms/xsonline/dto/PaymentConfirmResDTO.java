/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

/**
 *
 * @author pcduongit
 */
public class PaymentConfirmResDTO {

    private String errorCode;
    private String message;
    private Integer failCount;
//    private String voteCode;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

//    public String getVoteCode() {
//        return voteCode;
//    }
//
//    public void setVoteCode(String voteCode) {
//        this.voteCode = voteCode;
//    }

    
}
