/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseQueryDTO;

/**
 *
 * @author Duongpx2
 */
public class WinnerQueryDTO extends BaseQueryDTO {

    private Long branchId;
    private String branchCode;
    private String vodeCode;
    private String phone;
    private Integer status;

    public String getVodeCode() {
        return vodeCode;
    }

    public void setVodeCode(String vodeCode) {
        this.vodeCode = vodeCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    
}
