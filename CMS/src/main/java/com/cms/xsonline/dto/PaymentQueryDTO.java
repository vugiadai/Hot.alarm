/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseQueryDTO;
import java.util.Date;

/**
 *
 * @author pxduong
 */
public class PaymentQueryDTO extends BaseQueryDTO {

    private Long branchId;
    private String branchCode;
    private String vodeCode;
    private String phone;
    private Date date;

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getVodeCode() {
        return vodeCode;
    }

    public void setVodeCode(String vodeCode) {
        this.vodeCode = vodeCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    
}
