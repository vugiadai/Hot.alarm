/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import java.util.List;

/**
 *
 * @author pxduong
 */
public class DataTableDTO {

    private Integer curPage;
    private Integer pageSize;
    private Integer totalRecord;
    private List listData;
    private Float[] totals;

    public Integer getCurPage() {
        return curPage;
    }

    public void setCurPage(Integer curPage) {
        this.curPage = curPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Integer totalRecord) {
        this.totalRecord = totalRecord;
    }

  

    public List getListData() {
        return listData;
    }

    public void setListData(List listData) {
        this.listData = listData;
    }

    public Float[] getTotals() {
        return totals;
    }

    public void setTotals(Float[] totals) {
        this.totals = totals;
    }

}
