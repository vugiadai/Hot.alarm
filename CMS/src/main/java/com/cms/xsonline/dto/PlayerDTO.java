/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;
import java.util.Date;

/**
 *
 * @author Duongpx2
 */
public class PlayerDTO extends BaseDTO {

    private String datetime;
    private String makeCallTime;
    private String callId;
    private String caller; // so goi den nhung o day chinh la so hien thi goi ra
    private String phoneNumber; // = msisdn
    private long packageId;
    private String res = "0";
    private String systemStatus = "1";
    private Integer retryTotal = 0;
    private Date requestTime;
    private String transId;
    private long rowId = 0;
    private Long branchId;
    private String noitifyContent;

    public PlayerDTO() {
    }

    public PlayerDTO(String transId, String caller, String msisdn, Date requestTime, String notifyContent) {
        this.transId = transId;
        this.caller = caller;
        this.phoneNumber = msisdn;
        this.requestTime = requestTime;
        this.noitifyContent = notifyContent;
    }
    
    public PlayerDTO(String transId, String caller, String msisdn, Date requestTime) {
        this.transId = transId;
        this.caller = caller;
        this.phoneNumber = msisdn;
        this.requestTime = requestTime;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getMakeCallTime() {
        return makeCallTime;
    }

    public void setMakeCallTime(String makeCallTime) {
        this.makeCallTime = makeCallTime;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getPackageId() {
        return packageId;
    }

    public void setPackageId(long packageId) {
        this.packageId = packageId;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public String getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(String systemStatus) {
        this.systemStatus = systemStatus;
    }

    public Integer getRetryTotal() {
        return retryTotal;
    }

    public void setRetryTotal(Integer retryTotal) {
        this.retryTotal = retryTotal;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getNoitifyContent() {
        return noitifyContent;
    }

    public void setNoitifyContent(String noitifyContent) {
        this.noitifyContent = noitifyContent;
    }

}
