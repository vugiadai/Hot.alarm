/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;
import java.util.Date;

/**
 *
 * @author pxduong
 */
public class RevenueDTO extends BaseDTO {

    private String datetime;
    private String packageCode;
    private Long winCount;
    private Float revenue;
    private Float discount;
    private Float actualRevenue;
    private Float totalWinAmount;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public Long getWinCount() {
        return winCount;
    }

    public void setWinCount(Long winCount) {
        this.winCount = winCount;
    }

    public Float getRevenue() {
        return revenue;
    }

    public void setRevenue(Float revenue) {
        this.revenue = revenue;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Float getActualRevenue() {
        return actualRevenue;
    }

    public void setActualRevenue(Float actualRevenue) {
        this.actualRevenue = actualRevenue;
    }

    public Float getTotalWinAmount() {
        return totalWinAmount;
    }

    public void setTotalWinAmount(Float totalWinAmount) {
        this.totalWinAmount = totalWinAmount;
    }

    
}
