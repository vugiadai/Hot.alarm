/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;
import java.util.Date;

/**
 *
 * @author duongpx2
 */
public class ContactDTO extends BaseDTO {

    private Long ContactId;
    private String msisdn;
    private String contactName;
    private String description;
    private Long agentId;
    private String isActive;
    private Date createdDate;
    private Long createdBy;
    private Long userId;

    public Long getContactId() {
        return ContactId;
    }

    public ContactDTO() {
    }

    public ContactDTO(String msisdn, Long agentId) {
        this.msisdn = msisdn;
        this.agentId = agentId;
    }

    public ContactDTO(String msisdn, String contactName, String description, Long agentId) {
        this.msisdn = msisdn;
        this.contactName = contactName;
        this.description = description;
        this.agentId = agentId;
    }

    public void setContactId(Long ContactId) {
        this.ContactId = ContactId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
