/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

/**
 *
 * @author pxduongit
 */
public class QueuesDTO {

    private Long queueId;
    private String description;
    private String queueManagerId;
    private Long serviceId;
    private String tenanIdl;

    public Long getQueueId() {
        return queueId;
    }

    public void setQueueId(Long queueId) {
        this.queueId = queueId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQueueManagerId() {
        return queueManagerId;
    }

    public void setQueueManagerId(String queueManagerId) {
        this.queueManagerId = queueManagerId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getTenanIdl() {
        return tenanIdl;
    }

    public void setTenanIdl(String tenanIdl) {
        this.tenanIdl = tenanIdl;
    }

}
