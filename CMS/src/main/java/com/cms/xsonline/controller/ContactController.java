/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.controller;

import com.cms.xsonline.base.BaseController;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.DataTableDTO;
import com.cms.xsonline.dto.PlayerDTO;
import com.cms.xsonline.dto.PlayerQueryDTO;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cms.xsonline.service.ContactService;

/**
 *
 * @author Duongpx2
 */
@Controller
public class ContactController extends BaseController {

    @Resource
    private MessageSource messageSource;

    private ContactService playerService;

    @Autowired(required = true)
    @Qualifier(value = "playerService")
    public void setPersonService(ContactService ps) {
        this.playerService = ps;
    }

    @RequestMapping(value = {"/contact/getContact"}, method = {RequestMethod.GET})
    public String getPlayer(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Long packageId,
            @RequestParam(required = false) String packageName,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {

        List<PlayerDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
//            if (pageSize != null) {
            // neu la action cua search khong phai la redirect toi
            PlayerQueryDTO toSearch = new PlayerQueryDTO();
            toSearch.setPackageId(packageId);
            toSearch.setPhone(phone);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                toSearch.setAgentId(accountInfo.getAgentId());
            }
            totalRecord = this.playerService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.playerService.getByPage(toSearch);
//            } else {
//                pageSize = 15;
//                curPage = 1;
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        model.addAttribute("packageId", packageId);
        model.addAttribute("packageName", packageName);
        // paging
        model.addAttribute("curPage", curPage);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalRecord", totalRecord);
        model.addAttribute("listPlayer", result);
        return "listContact";
    }

    @RequestMapping(value = {"/contact/getContactList"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    @ResponseBody
    public DataTableDTO getAccountList(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Long packageId,
            @RequestParam(required = false) String res,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        DataTableDTO response = new DataTableDTO();
        List<PlayerDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            PlayerQueryDTO toSearch = new PlayerQueryDTO();
            toSearch.setPackageId(packageId);
            toSearch.setRes(res);
            toSearch.setPhone(phone);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                toSearch.setAgentId(accountInfo.getAgentId());
            }
            totalRecord = this.playerService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.playerService.getByPage(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        response.setCurPage(curPage);
        response.setPageSize(pageSize);
        response.setTotalRecord((totalRecord));
        response.setListData(result);
        return response;
    }

    @RequestMapping(value = {"/player/playerExportExcel"}, method = {RequestMethod.GET})
    public String exportExcel(
            HttpSession session,
            HttpServletRequest request,
            @RequestParam(required = false) Long packageId,
            @RequestParam(required = false) String res,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        List<PlayerDTO> result = new ArrayList<>();
        PlayerQueryDTO toSearch = new PlayerQueryDTO();
        try {
            //get transactions
            toSearch.setPackageId(packageId);
            toSearch.setRes(res);
            toSearch.setPhone(phone);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            result = this.playerService.getByPage(toSearch);
        } catch (Exception ex) {
        }
        model.addAttribute("data", result);
        model.addAttribute("type", "player");
        return "excelView";
    }
}
