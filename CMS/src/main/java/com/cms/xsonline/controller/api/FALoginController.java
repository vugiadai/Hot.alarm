package com.cms.xsonline.controller.api;


import com.cms.xsonline.dto.ChangePasswordDTO;
import com.cms.xsonline.facade.LoginFacade;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FALoginRequest;
import com.cms.xsonline.obj.request.FAResetPassRequest;
import com.cms.xsonline.obj.request.FAUpdateRequest;
import com.cms.xsonline.obj.response.FAGenericResponse;
import com.cms.xsonline.obj.response.LoginResponse;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class FALoginController {

    private final Logger logger = LoggerFactory.getLogger(FALoginController.class);

    @Autowired
    private LoginFacade loginFacade;

    @RequestMapping(value = "/v1/login", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<LoginResponse> loginApi(@RequestBody() FALoginRequest loginRequest) {
        logger.info("begin login {}", loginRequest);
        FAGenericResponse<LoginResponse> response = new FAGenericResponse<>();
        try {
            response.data = loginFacade.login(loginRequest.getPhone(), loginRequest.getPassword());
            response.code = "Login successful";

            return response;
        } catch (FAException e) {
            e.printStackTrace();
            response.responseCode = e.getErrorCode();
            response.code = e.getMessage();
        }

        return response;
    }


    @RequestMapping(value = "/v1/update/profile", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<Integer> updateProfile(@RequestHeader("X-Token") String token,
                                             @RequestBody() FAUpdateRequest updateRequest) {
        logger.info("begin updateProfile ");
        FAGenericResponse<Integer> response = new FAGenericResponse<>();
        try {
            if (TokenUtils.isTokenExpired(token)) {
                response.responseCode = Constant.ERROR_CODE_TOKEN_EXPIRED;
                response.code = "Token is expired or invalid";
                return response;
            }

            int currentUser = TokenUtils.getUserIdFromToken(token);
            response.data = loginFacade.updateProfile(currentUser, updateRequest);
            response.responseCode = Constant.RESPONSE_CODE_OK;
            response.code = "Updated profile successfully";
            return response;
        } catch (FAException e) {
            response.responseCode = e.getErrorCode();
            response.code = e.getMessage();
        }
        return response;
    }


    @RequestMapping(value = "/v1/pass/change", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<Integer> changePassword(@RequestHeader("X-Token") String token,
                                              @RequestBody() ChangePasswordDTO changeRequest) {
        FAGenericResponse<Integer> response = new FAGenericResponse<>();
        try {
            if (TokenUtils.isTokenExpired(token)) {
                response.responseCode = Constant.ERROR_CODE_TOKEN_EXPIRED;
                response.code = "Token is expired or invalid";
                return response;
            }
            int userId = TokenUtils.getUserIdFromToken(token);
            response.data = loginFacade.changePassword(userId, changeRequest);
            response.responseCode = Constant.RESPONSE_CODE_OK;
            return response;
        } catch (FAException e) {
            e.printStackTrace();
            response.responseCode = e.getErrorCode();
            response.code = e.getMessage();
        }

        return response;
    }

    @RequestMapping(value = "/v1/pass/reset", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<LoginResponse> resetPassword(@RequestBody() FAResetPassRequest request) {
        logger.info("begin resetPassword ");
        FAGenericResponse<LoginResponse> response = new FAGenericResponse<>();
//        try {
//            return response;
//        } catch (FAException e) {
//            e.printStackTrace();
//            response.responseCode = e.getErrorCode();
//            response.code = e.getMessage();
//        }

        return response;
    }


}
