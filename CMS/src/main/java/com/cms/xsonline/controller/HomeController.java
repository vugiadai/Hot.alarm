package com.cms.xsonline.controller;

import com.cms.xsonline.base.BaseController;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.ContactDTO;
import com.cms.xsonline.dto.DataTableDTO;
import com.cms.xsonline.dto.LotteryQueryDTO;
import com.cms.xsonline.dto.LotteryResultDTO;
import com.cms.xsonline.dto.PlayerDTO;
import com.cms.xsonline.service.AccountService;
import com.cms.xsonline.service.BranchService;
import com.cms.xsonline.service.HomeService;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.IUtils;
import com.cms.xsonline.util.MD5Util;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController extends BaseController {

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "repos";
    private HomeService homeService;
    private BranchService branchService;
    private AccountService accountService;

    @Autowired(required = true)
    @Qualifier(value = "homeService")
    public void setHomeService(HomeService hs) {
        this.homeService = hs;
    }

    @Autowired(required = true)
    @Qualifier(value = "branchService")
    public void setBranchService(BranchService bs) {
        this.branchService = bs;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setAccountService(AccountService as) {
        this.accountService = as;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        return "login";
    }

//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    public String loginPost(ModelMap model) {
//        return "login";
//    }
    @RequestMapping(value = "/loginError", method = RequestMethod.GET)
    public String loginError(
            @RequestParam(required = false) String captchaError,
            ModelMap model
    ) {
        Object fail = request.getSession().getAttribute(Constant.SessionKey.LOGIN_FAIL_COUNTER);
        Long failCount = fail == null ? 0 : Long.parseLong(fail.toString());
        failCount++;
        request.getSession().setAttribute(Constant.SessionKey.LOGIN_FAIL_COUNTER, failCount);
        if (failCount >= Constant.MAX_LOGIN_FAIL) {
            model.addAttribute("showCaptcha", 1);
        }
        if ("true".equals(captchaError)) {
            model.addAttribute("captchaError", "true");
        } else {
            model.addAttribute("error", "true");
        }
        return "login";
    }

    @RequestMapping(value = {"/getHome"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String getLotteryResult(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            @RequestParam(required = false) String agentName,
            @RequestParam(required = false) Long agentId,
            ModelMap model, Principal principal) {

        //reset captcha status
        request.getSession().setAttribute(Constant.SessionKey.USED_CAPTCHA, false);
        request.getSession().setAttribute(Constant.SessionKey.CAPTCHA_TOKEN, null);

        List<LotteryResultDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        Map<String, String> listBranchResponse = new HashMap<>();
        try {

            LotteryQueryDTO toSearch = new LotteryQueryDTO();
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            toSearch.setAgentName(agentName);
            toSearch.setAgentId(agentId);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                toSearch.setAgentId(accountInfo.getAgentId());
            }
            totalRecord = this.homeService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.homeService.getByPage(toSearch);

            List<BranchDTO> listBranch = this.accountService.getAllBranch();
            for (BranchDTO temp : listBranch) {
                listBranchResponse.put(String.valueOf(temp.getId()), temp.getName());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        //check enable update or commit
//        boolean updateEnable = true;
//        boolean commitEnable = false;
//        for (LotteryResultDTO temp : result) {
//            if (temp.getDatetime() != null && temp.getDatetime().equals(Utilities.getShortDateStr(new Date()))) {
//                updateEnable = this.homeService.checkEnableUpdate(temp);
//                commitEnable = this.homeService.checkEnableCommit(temp);
//                break;
//            }
//        }
//        //
        model.addAttribute("curPage", curPage);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalRecord", totalRecord);
//        model.addAttribute("updateEnable", updateEnable);
//        model.addAttribute("commitEnable", commitEnable);
        model.addAttribute("listBranch", listBranchResponse);
        model.addAttribute("listResult", result);
        model.addAttribute("toSearchForm", new LotteryQueryDTO());
        return "home";
    }

    @RequestMapping(value = {"/getFileList"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    @ResponseBody
    public DataTableDTO getFileList(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String agentName,
            @RequestParam(required = false) Long agentId,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        DataTableDTO response = new DataTableDTO();
        List<LotteryResultDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            LotteryQueryDTO toSearch = new LotteryQueryDTO();
            toSearch.setAgentName(agentName);
            toSearch.setAgentId(agentId);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                toSearch.setAgentId(accountInfo.getAgentId());
            }
            totalRecord = this.homeService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.homeService.getByPage(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        response.setCurPage(curPage);
        response.setPageSize(pageSize);
        response.setTotalRecord((totalRecord));
        response.setListData(result);
        return response;
    }

    @RequestMapping(value = "/getVerifyOTPLogin", method = RequestMethod.GET)
    public String getVerifyOTPLogin(ModelMap model,
            @RequestParam(required = false) String error,
            Principal principal) {
        if (error != null && "true".equals(error)) {
            model.addAttribute("error", error);
        }
        return "verifyOTPLogin";
    }

    @RequestMapping(value = "/doVerifyOTPLogin", method = RequestMethod.POST)
    public String verifyOTPLogin(
            HttpSession session,
            ModelMap model,
            @RequestParam(required = true) String verifyCode,
            Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();

        if (CheckOTPValid(accountInfo, verifyCode)) {
            session.setAttribute("isOTPValidate", "true");
            return "redirect:/getHome";
        } else {
            return "redirect:/getVerifyOTPLogin?error=true";
        }
    }

    private boolean CheckOTPValid(AccountDTO accountInfo, String OTPPass) {
        try {
            if (Objects.equals(Integer.valueOf(OTPPass), accountInfo.getOTPPass())) {
                if (Calendar.getInstance().getTime().getTime() < accountInfo.getOTPPassValidTime().getTime()) {
                    return true;
                }
            }
        } catch (Exception ex) {
        }
        return false;
    }

    @RequestMapping(value = {"/doUpdateLotteryResult"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String updateLotteryResult(@Valid
            @ModelAttribute("LotteryResult") LotteryResultDTO lotteryResult,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
        long result = this.homeService.updateResult(accountInfo, lotteryResult);
        if (result > 0) {
            attributes.addFlashAttribute("feedbackMessage", "Update lottery result success!");
        } else {
            attributes.addFlashAttribute("errorMessage", "Update lottery result failed. Please try again later...");
        }
        return "redirect:/getHome";
    }

    @RequestMapping(value = {"/doCommitLotteryResult"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String commitLotteryResult(@Valid
            @ModelAttribute("commitPassword") String commitPassword,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        if (commitPassword != null && !"".equals(commitPassword)) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            String MD5Pass = MD5Util.getSecurePassword(commitPassword + accountInfo.getSalt());
            int result = 0;
            if (MD5Pass.equals(accountInfo.getPassword())) {
                //thuc hien commit
                result = this.homeService.commitResult(accountInfo);
            } else {
                result = 202;
            }
            switch (result) {
                case 1:
                    attributes.addFlashAttribute("feedbackMessage", "Update lottery result success!");
                    break;
                case 201:
                    attributes.addFlashAttribute("errorMessage", "Lottery have been updated by another!");
                    break;
                case 202:
                    attributes.addFlashAttribute("errorMessage", "Wrong password!");
                    break;
                default:
                    attributes.addFlashAttribute("errorMessage", "Update lottery result failed. Please try again later...");
                    break;
            }

        } else {
            attributes.addFlashAttribute("errorMessage", "Password cannot null. Please try again...");
        }
        return "redirect:/getHome";
    }

    // download template
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response) {
        try {
            File file = new File(getHttpSession().getServletContext().getRealPath("resource") + File.separator + "sample.xlsx");
            if (!file.exists()) {
                String errorMessage = "Sorry. The file you are looking for does not exist";
                System.out.println(errorMessage);
                try (OutputStream outputStream = response.getOutputStream()) {
                    outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
                }
                return;
            }
            /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser 
            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
//        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
            //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
            response.setHeader("Content-Disposition", "attachment; filename=sample.xlsx");
            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            //Copy bytes from source to destination(outputstream in this example), closes both streams.
            FileCopyUtils.copy(inputStream, response.getOutputStream());
//        IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // upload file 
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
            @RequestParam("branchId") Long branchId,
            RedirectAttributes redirectAttributes, ModelMap modelMap) {

        if (file.isEmpty()) {
            modelMap.addAttribute("errorMessage", "Please select a file to upload");
            redirectAttributes.addFlashAttribute("errorMessage", "Please select a file to upload");
            return "uploadsuccess";
        }
        if (!file.getOriginalFilename().toLowerCase().endsWith(".xlsx")) {
            modelMap.addAttribute("errorMessage", "Please select excel file .xlsx");
            redirectAttributes.addFlashAttribute("errorMessage", "Please select excel file .xlsx");
            return "uploadsuccess";
        }

        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            String rootPath = getHttpSession().getServletContext().getRealPath(UPLOADED_FOLDER);
            // Creating the directory to store file
            File dir = new File(rootPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            // Create the file on server
//                File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
//                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
//                stream.write(bytes);
//                stream.close();
//                logger.info("Server File Location=" + serverFile.getAbsolutePath());
            //Now do something with file...
            String realFileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
            String absoluteRealFileName = rootPath + File.separator + realFileName;
            FileCopyUtils.copy(bytes, new File(absoluteRealFileName));
            redirectAttributes.addFlashAttribute("fileName", file.getOriginalFilename());
            redirectAttributes.addFlashAttribute("errorMessage", "You successfully uploaded '" + file.getOriginalFilename() + "'");
            modelMap.addAttribute("errorMessage", "You successfully uploaded '" + file.getOriginalFilename() + "'");

            // doc file
//            List<PlayerDTO> playerDTOs = this.readFile(absoluteRealFileName, modelMap);
            List<ContactDTO> listContact = this.readContactFile2(absoluteRealFileName, modelMap, branchId);
            if (!listContact.isEmpty()) {
                if (branchId > 0) {
                    int count = 0;
                    List<ContactDTO> batchList = new ArrayList<>();
                    List<AccountDTO> cusAccounts = new ArrayList<>();
                    //lay thong tin toa nha
                    List<BranchDTO> branchInfo = this.branchService.getById(branchId);
                    for (ContactDTO bo : listContact) {
                        //them vao list account
                        Long userId = this.branchService.getUserSeqNextVal();
                        AccountDTO cusAccount = new AccountDTO();
                        cusAccount.setUsername(branchInfo.get(0).getCode() + "_" + bo.getMsisdn());
                        //fix mat khau mac dinh la 123
                        cusAccount.setPassword("123");
                        cusAccount.setRePassword("123");
                        cusAccount.setAddress(bo.getDescription());
                        cusAccount.setAgentId(branchId);
                        cusAccount.setFirstName(bo.getContactName());
                        cusAccount.setPhone(bo.getMsisdn());
                        cusAccount.setRole("3");//nguoi dan
                        cusAccount.setId(userId);
                        cusAccounts.add(cusAccount);
                        //.
                        bo.setUserId(userId);
                        batchList.add(bo);
                        if (++count % 1000 == 0) {
//                            this.homeService.saveBatch(batchList, insertedId);
                            this.branchService.saveContactBatch(branchId, listContact);
                            batchList.clear();
                        }
                    }
                    if (!batchList.isEmpty()) {
//                        this.homeService.saveBatch(batchList, insertedId);
                        this.branchService.saveContactBatch(branchId, batchList);
                    }
                    //tao account cho cu dan. tach sang vong lap khac khong de anh huong den toa nha
                    //them tung account mot
                    for (AccountDTO user : cusAccounts) {
                        this.accountService.createAccount(user);
                    }
                    // cap nhat total row
//                    int totalRow = listContact.size();
//                    int rs = this.homeService.updateTotalRow(totalRow, insertedId);
//                    System.out.println("update g_file id = " + insertedId + " total_row = " + totalRow + " res = " + rs);
                    modelMap.addAttribute("res", "ok");
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            modelMap.addAttribute("errorMessage", "There are some error when parsing file. Check file again please!");
        }
        return "uploadsuccess";
        //return "redirect:/getHome";
    }

    public Integer saveFileBO(LotteryResultDTO lotteryResult) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
        Integer result = this.homeService.updateResult(accountInfo, lotteryResult);
        System.out.println("g_file id = " + result);
        return result;
    }

    public List<PlayerDTO> readFile(String absolutefileName, ModelMap modelMap) throws Exception {
        List<PlayerDTO> playerDTOs = new ArrayList<>();
        // new XSSFWorkbook -> too slow
//        First up, don't load a XSSFWorkbook from an InputStream when you have a file! Using an InputStream requires buffering of everything into memory, which eats up space and takes time. Since you don't need to do that buffering, don't!
        FileInputStream excelFile = null;
        XSSFWorkbook workbook = null;

        try {
            File file = new File(absolutefileName);
            excelFile = new FileInputStream(file);
            //Workbook workbook = new XSSFWorkbook(excelFile);
            // Finds the workbook instance for XLSX file
            workbook = new XSSFWorkbook(excelFile);

            // Return first sheet from the XLSX workbook 
            Sheet mySheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = mySheet.iterator();
            Date requestTime = new Date(System.currentTimeMillis());
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                int rowNum = currentRow.getRowNum();
                try {
                    Cell currentCell = currentRow.getCell(0); // PHONE_NUMBER
                    String phoneNumber = "";
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        phoneNumber = IUtils.evalString(currentCell.getStringCellValue());
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        phoneNumber = IUtils.evalString(NumberToTextConverter.toText(currentCell.getNumericCellValue()));
                    }

                    if (!"PHONE_NUMBER".equals(phoneNumber)) {
                        if (!"".equals(phoneNumber)) {
                            // se set luc goi ra tai bcapp
                            String transId = IUtils.genCallId(phoneNumber);
                            PlayerDTO playerDTO = new PlayerDTO(transId, "", phoneNumber, requestTime);
                            // set res luon cho nhung ban ghi khong hop le
                            playerDTO.setRowId(rowNum++);
                            //playerDTO.setPackageId(packageId);
                            if (!IUtils.isNumber(phoneNumber)) {
                                // playerDTO.setRes("400");
                                modelMap.addAttribute("errorMessage", "Invalid phone number at row index = " + rowNum + ". Check all row one again please!");
                                return new ArrayList<>();
                            }
                            // luu poke call vao db de execute
                            //this.homeService.insertPokeCall(playerDTO);
                            playerDTOs.add(playerDTO);
                        }
                    }

//                Iterator<Cell> cellIterator = currentRow.iterator();
//                while (cellIterator.hasNext()) {
//                    Cell currentCell = cellIterator.next();
//                    //getCellTypeEnum shown as deprecated for version 3.15
//                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
//                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
//                        System.out.print(currentCell.getStringCellValue() + "--");
//                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
//                        System.out.print(currentCell.getNumericCellValue() + "--");
//                    }
//                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (excelFile != null) {
                    excelFile.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return playerDTOs;
    }

    public List<ContactDTO> readContactFile(String absolutefileName, ModelMap modelMap, Long branchId) throws Exception {
        List<ContactDTO> result = new ArrayList<>();
        // new XSSFWorkbook -> too slow
//        First up, don't load a XSSFWorkbook from an InputStream when you have a file! Using an InputStream requires buffering of everything into memory, which eats up space and takes time. Since you don't need to do that buffering, don't!
        FileInputStream excelFile = null;
        XSSFWorkbook workbook = null;

        try {
            File file = new File(absolutefileName);
            excelFile = new FileInputStream(file);
            //Workbook workbook = new XSSFWorkbook(excelFile);
            // Finds the workbook instance for XLSX file
            workbook = new XSSFWorkbook(excelFile);

            // Return first sheet from the XLSX workbook 
            Sheet mySheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = mySheet.iterator();
//            Date requestTime = new Date(System.currentTimeMillis());
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                int rowNum = currentRow.getRowNum();
                try {
                    Cell currentCell = currentRow.getCell(0); // PHONE_NUMBER
                    String phoneNumber = "";
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        phoneNumber = IUtils.evalString(currentCell.getStringCellValue());
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        phoneNumber = IUtils.evalString(NumberToTextConverter.toText(currentCell.getNumericCellValue()));
                    }

                    if (!"PHONE_NUMBER".equals(phoneNumber)) {
                        if (!"".equals(phoneNumber)) {
                            if (!IUtils.isNumber(phoneNumber)) {
                                rowNum++;
                                modelMap.addAttribute("errorMessage", "Invalid phone number at row index = " + rowNum + ". Check all row one again please!");
                                return new ArrayList<>();
                            }
                            ContactDTO contact = new ContactDTO(phoneNumber, branchId);
                            contact.setIsActive("Y");
                            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
                            contact.setCreatedBy(accountInfo.getId());
                            //Kiem tra neu phone da ton tai --> bo qua, khong them nua
                            result.add(contact);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (excelFile != null) {
                    excelFile.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<ContactDTO> readContactFile2(String absolutefileName, ModelMap modelMap, Long branchId) throws Exception {
        List<ContactDTO> result = new ArrayList<>();
        // new XSSFWorkbook -> too slow
//        First up, don't load a XSSFWorkbook from an InputStream when you have a file! Using an InputStream requires buffering of everything into memory, which eats up space and takes time. Since you don't need to do that buffering, don't!
        FileInputStream excelFile = null;
        XSSFWorkbook workbook = null;

        try {
            File file = new File(absolutefileName);
            excelFile = new FileInputStream(file);
            //Workbook workbook = new XSSFWorkbook(excelFile);
            // Finds the workbook instance for XLSX file
            workbook = new XSSFWorkbook(excelFile);

            // Return first sheet from the XLSX workbook 
            Sheet mySheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = mySheet.iterator();
//            Date requestTime = new Date(System.currentTimeMillis());
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                int rowNum = currentRow.getRowNum();
                try {
                    Cell phoneNumberCell = currentRow.getCell(0); // PHONE_NUMBER
                    String phoneNumber = "";
                    if (phoneNumberCell.getCellTypeEnum() == CellType.STRING) {
                        phoneNumber = IUtils.evalString(phoneNumberCell.getStringCellValue());
                    } else if (phoneNumberCell.getCellTypeEnum() == CellType.NUMERIC) {
                        phoneNumber = IUtils.evalString(NumberToTextConverter.toText(phoneNumberCell.getNumericCellValue()));
                    }

                    Cell contactNameCell = currentRow.getCell(1); //CONTACT NAME
                    String contactName = "";
                    if (contactNameCell.getCellTypeEnum() == CellType.STRING) {
                        contactName = IUtils.evalString(contactNameCell.getStringCellValue());
                    } else if (contactNameCell.getCellTypeEnum() == CellType.NUMERIC) {
                        contactName = IUtils.evalString(NumberToTextConverter.toText(contactNameCell.getNumericCellValue()));
                    }

                    Cell descriptionCell = currentRow.getCell(2); //DESCRIPTION
                    String description = "";
                    if (descriptionCell.getCellTypeEnum() == CellType.STRING) {
                        description = IUtils.evalString(descriptionCell.getStringCellValue());
                    } else if (descriptionCell.getCellTypeEnum() == CellType.NUMERIC) {
                        description = IUtils.evalString(NumberToTextConverter.toText(descriptionCell.getNumericCellValue()));
                    }

                    if (!"PHONE_NUMBER".equals(phoneNumber)) {
                        if (!"".equals(phoneNumber)) {
                            if (!IUtils.isNumber(phoneNumber)) {
                                rowNum++;
                                modelMap.addAttribute("errorMessage", "Invalid phone number at row index = " + rowNum + ". Check all row one again please!");
                                return new ArrayList<>();
                            }
                            ContactDTO contact = new ContactDTO(phoneNumber, contactName, description, branchId);
                            contact.setIsActive("Y");
                            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
                            contact.setCreatedBy(accountInfo.getId());
                            //Kiem tra neu phone da ton tai --> bo qua, khong them nua
                            result.add(contact);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (excelFile != null) {
                    excelFile.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    // doc toi dau insert toi do
    // upload file 
    @RequestMapping(value = "/uploadfile2", method = RequestMethod.POST)
    public String singleFileUpload2(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes, ModelMap modelMap) {

        if (file.isEmpty()) {
            modelMap.addAttribute("errorMessage", "Please select a file to upload");
            redirectAttributes.addFlashAttribute("errorMessage", "Please select a file to upload");
            return "uploadsuccess";
        }
        if (!file.getOriginalFilename().toLowerCase().endsWith(".xlsx")) {
            modelMap.addAttribute("errorMessage", "Please select excel file .xlsx");
            redirectAttributes.addFlashAttribute("errorMessage", "Please select excel file .xlsx");
            return "uploadsuccess";
        }

        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            String rootPath = getHttpSession().getServletContext().getRealPath(UPLOADED_FOLDER);
            // Creating the directory to store file
            File dir = new File(rootPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            // Create the file on server
//                File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
//                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
//                stream.write(bytes);
//                stream.close();
//                logger.info("Server File Location=" + serverFile.getAbsolutePath());
            //Now do something with file...
            String realFileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
            String absoluteRealFileName = rootPath + File.separator + realFileName;
            FileCopyUtils.copy(bytes, new File(absoluteRealFileName));
            redirectAttributes.addFlashAttribute("fileName", file.getOriginalFilename());
            redirectAttributes.addFlashAttribute("errorMessage", "You successfully uploaded '" + file.getOriginalFilename() + "'");
            modelMap.addAttribute("errorMessage", "You successfully uploaded '" + file.getOriginalFilename() + "'");

            // luu file
            LotteryResultDTO lotteryResult = new LotteryResultDTO();
            lotteryResult.setFileName(file.getOriginalFilename());
            lotteryResult.setRealFileName(realFileName);
            lotteryResult.setTotalRow(0l);
            lotteryResult.setStatus("0");
            Integer insertedId = this.saveFileBO(lotteryResult);
            // doc file
            int totalRow = this.readFile2(absoluteRealFileName, insertedId);
            // cap nhat total row
            int rs = this.homeService.updateTotalRow(totalRow, insertedId);
            System.out.println("update g_file id = " + insertedId + " total_row = " + totalRow + " res = " + rs);
        } catch (Exception ex) {
            ex.printStackTrace();
            modelMap.addAttribute("errorMessage", "There are some error when parsing file. Check file again please!");
        }
        return "uploadsuccess";
        //return "redirect:/getHome";
    }

    public int readFile2(String absolutefileName, long packageId) throws Exception {
        // new XSSFWorkbook -> too slow
//        First up, don't load a XSSFWorkbook from an InputStream when you have a file! Using an InputStream requires buffering of everything into memory, which eats up space and takes time. Since you don't need to do that buffering, don't!
        int totalRow = 0;
        FileInputStream excelFile = null;
        XSSFWorkbook workbook = null;

        try {
            File file = new File(absolutefileName);
            excelFile = new FileInputStream(file);
            //Workbook workbook = new XSSFWorkbook(excelFile);
            // Finds the workbook instance for XLSX file
            workbook = new XSSFWorkbook(excelFile);

            // Return first sheet from the XLSX workbook 
            Sheet mySheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = mySheet.iterator();
            Date requestTime = new Date(System.currentTimeMillis());
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                int rowNum = currentRow.getRowNum();
                try {
                    Cell currentCell = currentRow.getCell(0); // PHONE_NUMBER
                    String phoneNumber = "";
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        phoneNumber = IUtils.evalString(currentCell.getStringCellValue());
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        phoneNumber = IUtils.evalString(NumberToTextConverter.toText(currentCell.getNumericCellValue()));
                    }

                    if (!"PHONE_NUMBER".equals(phoneNumber)) {
                        if (!"".equals(phoneNumber)) {
                            totalRow++;
                            // se set luc goi ra tai bcapp
                            String transId = IUtils.genCallId(phoneNumber);
                            PlayerDTO playerDTO = new PlayerDTO(transId, "", phoneNumber, requestTime);
                            // set res luon cho nhung ban ghi khong hop le
                            playerDTO.setRowId(rowNum);
                            playerDTO.setPackageId(packageId);
                            if (!IUtils.isNumber(phoneNumber)) {
                                playerDTO.setRes("400");
                            }
                            // luu poke call vao db de execute
                            this.homeService.insertPokeCall(playerDTO);
                        }
                    }

//                Iterator<Cell> cellIterator = currentRow.iterator();
//                while (cellIterator.hasNext()) {
//                    Cell currentCell = cellIterator.next();
//                    //getCellTypeEnum shown as deprecated for version 3.15
//                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
//                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
//                        System.out.print(currentCell.getStringCellValue() + "--");
//                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
//                        System.out.print(currentCell.getNumericCellValue() + "--");
//                    }
//                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (excelFile != null) {
                    excelFile.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return totalRow;
    }
}
