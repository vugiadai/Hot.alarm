/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.controller;

import com.cms.xsonline.base.BaseController;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.BranchDetailQueryDTO;
import com.cms.xsonline.dto.BranchQueryDTO;
import com.cms.xsonline.dto.CallInfoDTO;
import com.cms.xsonline.dto.CallInfoQueryDTO;
import com.cms.xsonline.dto.ContactDTO;
import com.cms.xsonline.dto.DataTableDTO;
import com.cms.xsonline.dto.ResponseDTO;
import com.cms.xsonline.service.AccountService;
import com.cms.xsonline.service.BranchService;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author pxduong
 */
@Controller
public class BranchController extends BaseController {

    @Resource
    private MessageSource messageSource;
    private AccountService accountService;
    private BranchService branchService;

    @Autowired(required = true)
    @Qualifier(value = "branchService")
    public void setPersonService(BranchService bs) {
        this.branchService = bs;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setAccountService(AccountService as) {
        this.accountService = as;
    }

//    private static Logger logger = Logger.getLogger(BranchController.class.getName());
    @RequestMapping("/agent/getBranch")
    public String getBranch(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String branchId,
            @RequestParam(required = false) String branchCode,
            @RequestParam(required = false) String branchName,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        List<BranchDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            //neu khong phai admin thi redirect ve trang branch detail
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                return "redirect:/agent/getBranchDetail/" + accountInfo.getAgentId();
            }
            BranchQueryDTO toSearch = new BranchQueryDTO();
            toSearch.setBranchId(branchId != null ? Long.valueOf(branchId) : null);
            toSearch.setBranchCode(branchCode);
            toSearch.setBranchName(branchName);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            totalRecord = this.branchService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.branchService.getByPage(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        BranchQueryDTO toSearchForm = new BranchQueryDTO();
        model.addAttribute("curPage", curPage);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalRecord", totalRecord);
        model.addAttribute("listBranch", result);
        model.addAttribute("toSearchForm", toSearchForm);
        return "branchManager";
    }

    @RequestMapping(value = "/agent/getBranchList", method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public @ResponseBody
    DataTableDTO getBranchList(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String branchId,
            @RequestParam(required = false) String branchCode,
            @RequestParam(required = false) String branchName,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        DataTableDTO response = new DataTableDTO();
        List<BranchDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            BranchQueryDTO toSearch = new BranchQueryDTO();
            if (pageSize == null) {
                pageSize = 15;
            }
            toSearch.setBranchCode(branchCode);
            toSearch.setBranchName(branchName);
            toSearch.setBranchId(branchId != null ? Long.valueOf(branchId) : null);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            totalRecord = this.branchService.getCount(toSearch);
            if (curPage == null) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.branchService.getByPage(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        response.setCurPage(curPage);
        response.setPageSize(pageSize);
        response.setTotalRecord((totalRecord));
        response.setListData(result);
        return response;
    }

    @RequestMapping("/agent/fireButton")
    public String getFireButton(
            ModelMap model, Principal principal) {
        //mac dinh tra ve id cua agent
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
        model.addAttribute("branchId", accountInfo.getAgentId());
        return "fireButton";
    }

    @RequestMapping(value = {"/agent/getBranchDetail/{branchId}"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String getBranchDetail(@PathVariable("branchId") Long branchId,
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description,
            ModelMap model, Principal principal) {
        List<ContactDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        Long id = null;
        try {
            //check quyen
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            if (accountInfo.getListRoleName().contains("admin") && branchId != null) {
                id = branchId;
            } else {
                id = accountInfo.getAgentId();
            }
            BranchDetailQueryDTO toSearch = new BranchDetailQueryDTO();
            toSearch.setObjId(id);
            toSearch.setPhoneNumber(phoneNumber);
            toSearch.setName(name);
            toSearch.setDescription(description);
            totalRecord = this.branchService.getCountBranchDetail(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.branchService.getBranchDetail(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        BranchQueryDTO toSearchForm = new BranchQueryDTO();
        model.addAttribute("curPage", curPage);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalRecord", totalRecord);
        model.addAttribute("listBranchDetail", result);
        model.addAttribute("toSearchForm", toSearchForm);
        model.addAttribute("branchId", id);
        return "branchDetail";
    }

    @RequestMapping(value = "/agent/getBranchDetailList", method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public @ResponseBody
    DataTableDTO getBranchDetailList(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Long branchId,
            @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description,
            ModelMap model, Principal principal) {
        DataTableDTO response = new DataTableDTO();
        List<ContactDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            //check quyen
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
            Long id = null;
            if (accountInfo.getListRoleName().contains("admin") && branchId != null) {
                id = branchId;
            } else {
                id = accountInfo.getAgentId();
            }
            BranchDetailQueryDTO toSearch = new BranchDetailQueryDTO();
            if (pageSize == null) {
                pageSize = 15;
            }
            toSearch.setObjId(id);
            toSearch.setPhoneNumber(phoneNumber);
            toSearch.setName(name);
            toSearch.setDescription(description);
            totalRecord = this.branchService.getCountBranchDetail(toSearch);
            if (curPage == null) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.branchService.getBranchDetail(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        response.setCurPage(curPage);
        response.setPageSize(pageSize);
        response.setTotalRecord((totalRecord));
        response.setListData(result);
        return response;
    }

    @RequestMapping(value = {"/agent/getEditBranch/{id}"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String editBranch(@PathVariable("id") Long id, ModelMap model
    ) {
        List<BranchDTO> listBranch = this.branchService.getById(id);
        if (listBranch.size() > 0) {
            model.addAttribute("typeView", "edit");
            model.addAttribute("branch", listBranch.get(0));
        }
        return "branchEdit";
    }

    @RequestMapping(value = {"/agent/doDeleteAgent"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String deleteBranch(
            @Valid
            @RequestParam(required = true) Long id,
            ModelMap model
    ) {
        try {
            this.branchService.deleteBranch(id);
        } catch (Exception ex) {
            //log
        }
        return "redirect:/agent/getBranch";
    }

    @RequestMapping(value = {"/agent/doDeleteContact"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String deleteContact(
            @Valid
            @RequestParam(required = true) Long id,
            @RequestParam(required = true) Long branchId,
            ModelMap model
    ) {
        try {
            this.branchService.deleteContact(id);
        } catch (Exception ex) {
            //log
        }
        return "redirect:/agent/getBranchDetail/" + branchId;
    }

    @RequestMapping(value = {"/agent/getAddNewBranch"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String addNewBranch(ModelMap model
    ) {
        model.addAttribute("typeView", "new");
        BranchDTO temp = new BranchDTO();
        model.addAttribute("branch", temp);
        return "branchEdit";
    }

    @RequestMapping(value = {"/agent/doEditBranch"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String submitEditBranchForm(@Valid
            @ModelAttribute("branch") BranchDTO updated,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        try {
            if (this.branchService.editBranch(updated) > 0) {
                attributes.addFlashAttribute("feedbackMessage", "Edit agent info success!");
            } else {
                attributes.addFlashAttribute("errorMessage", "Edit agent info failed. Please try again later...");
            }
        } catch (Exception ex) {
        }
        return "redirect:/agent/getBranch";
    }

    @RequestMapping(value = {"/agent/doAddNewBranch"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String submitCreateBranchForm(
            @Valid @ModelAttribute("branch") BranchDTO updated,
            BindingResult bindingResult,
            RedirectAttributes attributes) {
        try {
            if (!this.branchService.checkAgentExist(updated)) {
                if (this.branchService.addNewBranch(updated) > 0) {
                    attributes.addFlashAttribute("feedbackMessage", "Add new agent success!");
                } else {
                    attributes.addFlashAttribute("errorMessage", "Add new agent failed. Please try again later...");
                }
            } else {
                attributes.addFlashAttribute("errorMessage", "Agent code exists. Please try another...");
            }
        } catch (Exception ex) {
        }
        return "redirect:/agent/getBranch";
    }

    @RequestMapping(value = "/agent/addContact", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseDTO addContact(
            @RequestBody ContactDTO contact,
            ModelMap model, Principal principal) {
        ResponseDTO result = new ResponseDTO();
        result.setResultCode("99");
        result.setMessage("UNKNOWN");
        AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!accountInfo.getListRoleName().contains("admin")) {
            if (contact.getAgentId().longValue() != accountInfo.getAgentId().longValue()) {
                result.setResultCode("403");
                result.setMessage("NO PERMISSION");
                return result;
            }
        }
        contact.setIsActive("Y");
        contact.setCreatedBy(accountInfo.getId());
        Integer rows = this.branchService.addContact(contact);
        if (rows > 0) {
            result.setResultCode("00");
            result.setMessage("SUCCESS");
        } else {
            result.setResultCode("01");
            result.setMessage("NOT HAVE ANY ROW AFFECTED");
        }
        return result;
    }

    @RequestMapping(value = "/agent/addAgent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseDTO addAgent(
            @RequestBody BranchDTO branch,
            ModelMap model, Principal principal) {
        ResponseDTO result = new ResponseDTO();
        result.setResultCode("99");
        result.setMessage("UNKNOWN");
        try {
//            if (!this.branchService.checkAgentExist(branch)) {
//them moi tu gen code nen khong can check trung

            if (this.branchService.addNewBranch(branch) > 0) {
                result.setResultCode("00");
                result.setMessage("Thêm mới thành công");
//                    .addFlashAttribute("feedbackMessage", "Add new agent success!");
            } else {
                result.setResultCode("01");
                result.setMessage("Thêm mới thất bại. Thử lại sau!");
//                    attributes.addFlashAttribute("errorMessage", "Add new agent failed. Please try again later...");
            }
//            } else {
//                result.setResultCode("00");
//                result.setMessage("Thêm mới thất bại. Thử lại sau!");
//                attributes.addFlashAttribute("errorMessage", "Agent code exists. Please try another...");
//            }
        } catch (Exception ex) {
            result.setResultCode("99");
            result.setMessage("UNKNOWN");
        }
        return result;
    }

    @RequestMapping(value = {"/agent/changeContactStatus"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public @ResponseBody
    ResponseDTO changeContactStatus(
            @RequestParam(required = true) Long id,
            @RequestParam(required = true) String status
    ) {
        ResponseDTO result = new ResponseDTO();
        result.setResultCode("99");
        result.setMessage("UNKNOWN");
        try {

//            AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            if (!accountInfo.getListRoleName().contains("admin") && contact.getAgentId() != accountInfo.getAgentId()) {
//                result.setResultCode("403");
//                result.setMessage("NO PERMISSION");
//                return result;
//            }
            Integer rows = this.branchService.changeContactStatus(id, status);
            if (rows > 0) {
                result.setResultCode("00");
                result.setMessage("SUCCESS");
            } else {
                result.setResultCode("01");
                result.setMessage("NOT HAVE ANY ROW AFFECTED");
            }
        } catch (Exception ex) {
            System.out.println("lo");
        }
        return result;
    }

    @RequestMapping(value = {"/agent/makeCall"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public @ResponseBody
    ResponseDTO makeCall(
            @RequestParam(required = true) Long branchId,
            @RequestParam(required = false) String notifyContent
    ) {
        ResponseDTO result = new ResponseDTO();
        result.setResultCode("99");
        result.setMessage("UNKNOWN");
        try {
            Long id = null;
            AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (accountInfo.getListRoleName().contains("admin") && branchId != null) {
                id = branchId;
            } else {
                id = accountInfo.getAgentId();
            }
            String makeCallResult = this.branchService.makeCall(id, notifyContent, accountInfo);
            if ("SUCCESS".equals(makeCallResult)) {
                result.setResultCode("00");
                result.setMessage("SUCCESS");
            } else if ("ERR_LAST_2_HOUR".equals(makeCallResult)) {
                result.setResultCode("01");
                result.setMessage("ERR_LAST_2_HOUR");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return result;
    }

    @RequestMapping("/agent/getCallInfo")
    public String getCallInfo(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) Long branchId,
            @RequestParam(required = false) String packageId,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        List<CallInfoDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        Map<String, String> listBranchResponse = new HashMap<>();
        try {
            CallInfoQueryDTO toSearch = new CallInfoQueryDTO();
            toSearch.setBranchId(branchId);
            toSearch.setCaller(phoneNumber);
            toSearch.setPackageId(packageId);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                toSearch.setBranchId(accountInfo.getAgentId());
            }
            totalRecord = this.branchService.getCountCallInfo(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.branchService.getCallInfo(toSearch);

            //init agent to search
            List<BranchDTO> listBranch = this.accountService.getAllBranch();
            for (BranchDTO temp : listBranch) {
                listBranchResponse.put(String.valueOf(temp.getId()), temp.getName());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        BranchQueryDTO toSearchForm = new BranchQueryDTO();
        model.addAttribute("curPage", curPage);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalRecord", totalRecord);
        model.addAttribute("listCallInfo", result);
        model.addAttribute("toSearchForm", toSearchForm);
        model.addAttribute("listBranch", listBranchResponse);
        return "callInfo";
    }

    @RequestMapping(value = "/agent/getCallInfoList", method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public @ResponseBody
    DataTableDTO getCallInfoList(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String packageId,
            @RequestParam(required = false) Long branchId,
            @RequestParam(required = false) Date fromDate,
            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        DataTableDTO response = new DataTableDTO();
        List<CallInfoDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            CallInfoQueryDTO toSearch = new CallInfoQueryDTO();
            if (pageSize == null) {
                pageSize = 15;
            }
            toSearch.setCaller(phoneNumber);
            toSearch.setBranchId(branchId);
            toSearch.setPackageId(packageId);
            toSearch.setFromDate(fromDate);
            toSearch.setToDate(toDate);
            AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (!accountInfo.getListRoleName().contains("admin")) {
                toSearch.setBranchId(accountInfo.getAgentId());
            }
            totalRecord = this.branchService.getCountCallInfo(toSearch);
            if (curPage == null) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.branchService.getCallInfo(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        response.setCurPage(curPage);
        response.setPageSize(pageSize);
        response.setTotalRecord((totalRecord));
        response.setListData(result);
        return response;
    }

    // download template
    @RequestMapping(value = "/agent/getfile", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public void downloadFile(
            @RequestParam(required = true) String f,
            HttpServletResponse response) {
        try {
            String FILE_PATH = "C:\\Users\\asus\\Music\\SampleMusic";
            File file = new File(FILE_PATH + File.separator + "audio.wav");
            if (!file.exists()) {
                String errorMessage = "Sorry. The file you are looking for does not exist";
                System.out.println(errorMessage);
                try (OutputStream outputStream = response.getOutputStream()) {
                    outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
                }
                return;
            }
            /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser 
            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
//        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
            //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
//            response.setHeader("Content-Disposition", "attachment; filename=sample.wav");
            
            response.setContentType("audio/wav");
            response.setContentLength((int) file.length());
            
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            //Copy bytes from source to destination(outputstream in this example), closes both streams.
            FileCopyUtils.copy(inputStream, response.getOutputStream());
//        IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
