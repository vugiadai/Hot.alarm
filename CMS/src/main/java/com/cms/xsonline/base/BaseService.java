/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.base;

import java.util.List;

/**
 *
 * @author pxduong
 */
public interface BaseService<T extends BaseDTO, K extends BaseQueryDTO> {

//    public List<T> getAll(BaseQueryDTO query);
    public Integer getCount(K query);

    public List<T> getByPage(K query);

}
