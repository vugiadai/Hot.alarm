/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.base;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author pxduong
 */
@Transactional
public class BaseDAO extends JdbcDaoSupport {

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    private void initialisze() {
        setDataSource(dataSource);
    }
//    protected Logger logger;

    protected Long insertWithIdReturn(String query, Object[] params) {
        try (Connection conn = dataSource.getConnection();) {
            try (CallableStatement ps = conn.prepareCall(query)) {
                for (int i = 0; i < params.length; i++) {
                    ps.setObject(i + 1, params[i]);
                }
                ps.registerOutParameter(params.length + 1, OracleTypes.NUMBER);
                ps.executeQuery();
                return ps.getLong(params.length + 1);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
