/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.filter;

import com.cms.xsonline.util.Constant;
import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author Duongpx2
 */
public class CaptchaCaptureFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest hsr, HttpServletResponse hsr1, FilterChain fc) throws ServletException, IOException {
        String path = hsr.getRequestURI();
        if (!path.contains("login")) {
            fc.doFilter(hsr, hsr1);
            return;
        }
        if (path.contains("loginError")
                || path.contains("login.js")
                || path.contains("icon-login.png")) {
            fc.doFilter(hsr, hsr1);
            return;
        }
        Object temp = hsr.getSession().getAttribute(Constant.SessionKey.USED_CAPTCHA);
        Boolean usedCaptcha = temp == null ? false : (Boolean) temp;
        if (usedCaptcha) {//neu dung captcha thi thuc hien check
            Object captchaInSession = hsr.getSession().getAttribute(Constant.SessionKey.CAPTCHA_TOKEN);
            String[] captchaInRequest = hsr.getParameterValues("captchaValue");
            if (captchaInSession != null) {
                if (captchaInRequest != null && captchaInRequest.length > 0) {
                    if (captchaInRequest[0].equals(captchaInSession.toString())) {
                        fc.doFilter(hsr, hsr1);
                        return;
                    } else {
                        hsr1.sendRedirect(hsr.getContextPath() + "/loginError?captchaError=true");
                        return;
                    }
                } else {
                    hsr1.sendRedirect(hsr.getContextPath() + "/loginError?captchaError=true");
                    return;
                }
            } else {
                fc.doFilter(hsr, hsr1);
                return;
            }
        } else {
            fc.doFilter(hsr, hsr1);
            return;
        }
    }

}
